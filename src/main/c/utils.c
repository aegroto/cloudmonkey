#include "include/utils.h"

char* ulongToBinary(unsigned long a, int length) {
    char* out = malloc(sizeof(char) * (length + 1));
    for(int i = 0; i < length; ++i)
        out[i] = '0';
    out[length] = 0;

    int i = length-1;

    do {
        out[i--] = '0' + a % 2;
        a /= 2;
    } while(a > 0);

    return out;
}

int min(int a, int b) {
  return a <= b ? a : b;
}

int isPowerOfTwo(unsigned int x) {
  return !(x & (x - 1));
}