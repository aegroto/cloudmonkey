#include "units/approximation/approximation.h"

#define VALUES_PER_LEVEL 2

#define DEBUG(str, ...) \
	do{ if(0) printf(str, ##__VA_ARGS__); } while(0)

int approximation_encode(unsigned char* input, unsigned char* output, int inputOffset, int outputOffset, int inputLength) {
    static unsigned int frequencies[256];
    static unsigned char map[256];

    // calculating frequencies
    for(int i = 0; i < inputLength; ++i) {
        ++frequencies[input[i]];
    }

    // creating map
    for(unsigned int i = 0; i < 256; i += VALUES_PER_LEVEL) {
        unsigned char mappedValue = (char) i;

        for(unsigned int k = 1; k < VALUES_PER_LEVEL; ++k) {
            if(frequencies[i + k] > frequencies[mappedValue]) {
                mappedValue = i + k;
            }
        }

        for(unsigned int k = 0; k < VALUES_PER_LEVEL; ++k) {
            map[i + k] = mappedValue;
        }
    }

    // mapping input to output
    for(int i = 0; i < inputLength; ++i) {
        output[i] = map[input[i]];
    }

    return inputLength;
}

#undef DEBUG
#define DEBUG(str, ...) \
	do{ if(0) printf(str, ##__VA_ARGS__); } while(0)

void approximation_decode(unsigned char* input, unsigned char* output, int inputOffset, int outputOffset, int encodedInputLength) {
    // bulk method, should be never called out of testing
    for(int i = 0; i < encodedInputLength; ++i) {
        output[i] = input[i];
    }
}