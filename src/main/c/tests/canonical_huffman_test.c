#include "../include/utils.h"
#include "../units/canonical_huffman/canonical_huffman.h"

int main(int argc) {
    char input[] = {
        47, 41, 31, -1, 53, 48, 36, -1, 63, 61, 48, -1, 65, 62, 49, -1, 63, 59, 46, -1, 63, 60, 47, -1, 68, 64, 49, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 63, 50, -1, 64, 63, 50, -1, 64, 63, 50, -1
        // 47, 41, 31, -1, 53, 48, 36, -1, 63
        // 0, 0, 0, 0, 1, 1, 1, 1
    };

    size_t inputLength = sizeof(input);

    printf("Input: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%d ", input[i]);
    }
    printf("\n");

    size_t encodedInputMaxLength = 8 + inputLength + inputLength * 8 + 4;

    char* encoded_input = (char*) malloc(encodedInputMaxLength);
    char* output = (char*) malloc(inputLength);

    int encodedInputLength = canonical_huffman_encode(input, encoded_input, 0, 0, sizeof(input));

    printf("Encoded input: ");
    for(int i = 0; i < encodedInputLength; ++i) {
        printf("%s ", ulongToBinary(encoded_input[i], 8));
    }
    printf("\n");

    canonical_huffman_decode(encoded_input, output, 0, 0, encodedInputLength);

    printf("Output: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%d ", output[i]);
    }
    printf("\n");

    free(encoded_input);
    free(output);

    return 0;
}
