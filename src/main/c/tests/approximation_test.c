#include "../units/approximation/approximation.h"

int main(int argc) {
    char input[] = {
        0, 0, 0, 0, 5, 4, 4, 6
    };

    size_t inputLength = sizeof(input);

    printf("Input: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%d ", input[i]);
    }
    printf("\n");

    size_t encodedInputMaxLength = 8 + inputLength + inputLength * 8 + 4;

    char* encoded_input = (char*) malloc(encodedInputMaxLength);
    char* output = (char*) malloc(inputLength);

    int encodedInputLength = approximation_encode(input, encoded_input, 0, 0, sizeof(input));;

    printf("Encoded input: ");
    for(int i = 0; i < encodedInputLength; ++i) {
        printf("%s ", ulongToBinary(encoded_input[i], 8));
    }
    printf("\n");

    approximation_decode(encoded_input, output, 0, 0, encodedInputLength);

    printf("Output: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%d ", output[i]);
    }
    printf("\n");

    free(encoded_input);
    free(output);

    return 0;
}
