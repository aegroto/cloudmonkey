CFLAGS="-fPIC -O3 -march=native -mtune=native"
LIBS_FOLDER="../../../.."

gcc -g $CFLAGS runlength_test.c ../runlength.c ../utils.c -o runlength_test -lm
gcc -g $CFLAGS approximation_test.c ../approximation.c ../utils.c -o approximation_test -lm
gcc -g $CFLAGS zstd_test.c ../zstd.c ../utils.c -o zstd_test -pthread -lm -Wl,--whole-archive $LIBS_FOLDER/libzstd.a -Wl,--no-whole-archive
gcc -g $CFLAGS lz4_test.c ../lz4.c ../utils.c -o lz4_test -pthread -lm -Wl,--whole-archive $LIBS_FOLDER/liblz4.a -Wl,--no-whole-archive
gcc -g $CFLAGS huffman_test.c ../huffman.c ../HuffmanPriorityQueue.c ../utils.c -o huffman_test -lm
gcc -g $CFLAGS canonical_huffman_test.c ../canonical_huffman.c ../utils.c -o canonical_huffman_test -lm
gcc -g $CFLAGS differential_compare_test.c ../differential_compare.c ../utils.c -o differential_compare_test -lm