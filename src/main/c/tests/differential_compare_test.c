#include <time.h>

#include "../units/differential_compare/differential_compare.h"

#define DEBUG 0 

int main(int argc) {
    size_t inputLength = 1280*720*4;

    unsigned char* firstInput = (unsigned char*) malloc(inputLength);
    unsigned char* secondInput = (unsigned char*) malloc(inputLength);

    for(int i = 0; i < inputLength; ++i) { 
        firstInput[i] = (unsigned char) (rand() % 256);
        secondInput[i] = (unsigned char) (rand() % 256);
    }

    // size_t inputLength = sizeof(input);

#if DEBUG
    printf("First input: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%d ", firstInput[i]);
    }
    printf("\n");

    printf("Second input: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%u ", secondInput[i]);
    }
    printf("\n");
#endif

    size_t encodedInputMaxLength = inputLength;

    unsigned char* encodedInput = (unsigned char*) malloc(encodedInputMaxLength);
    unsigned char* output = (unsigned char*) malloc(inputLength);

    int encodedInputLength = differential_compare_encode(firstInput, secondInput, encodedInput, 0, 0, 0, inputLength);

    // printf("Encoding time: %f (Compression ratio: %f)\n", time_spent, (double)(encodedInputLength) / (inputLength));

#if DEBUG
    printf("Encoded input: ");
    for(int i = 0; i < encodedInputLength; ++i) {
        printf("%u ", encodedInput[i]);
    }
    printf("\n");
#endif

    differential_compare_decode(encodedInput, firstInput, output, 0, 0, 0, encodedInputLength);

#if DEBUG
    printf("Output: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%d ", output[i]);
    }
    printf("\n");
#endif

    unsigned int diff = 0;
    for(int i = 0; i < inputLength; ++i) {
        if(secondInput[i] != output[i]) {
            ++diff;
        }
    }

    printf("Diff: %d\n", diff);

    free(firstInput);
    free(secondInput);
    free(encodedInput);
    free(output);

    return 0;
}
