#include <time.h>

#include "../units/zstd/zstd.h"

int main(int argc) {
    size_t inputLength = 1920*1080*4*120;
    // char input[] = { 0, 0, 0, 0, 5, 4, 4, 6 };
    char* input = (char*) malloc(inputLength);

    for(int i = 0; i < inputLength; ++i) { 
        input[i] = (char) (rand() % 256);
    }

    // size_t inputLength = sizeof(input);

    /*printf("Input: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%d ", input[i]);
    }
    printf("\n");*/

    size_t encodedInputMaxLength = ZSTD_compressBound(inputLength);

    char* encoded_input = (char*) malloc(encodedInputMaxLength);
    char* output = (char*) malloc(inputLength);

    ZSTD_CCtx *context = ZSTD_createCCtx();
    ZSTD_CCtx_reset(context, ZSTD_reset_session_and_parameters);

    ZSTD_CCtx_setParameter(context, ZSTD_c_nbWorkers, 8);
    ZSTD_CCtx_setParameter(context, ZSTD_c_jobSize, 0);

    /*
    int encodedInputLength = zstd_encode(context, input, encoded_input, 0, 0, inputLength, encodedInputMaxLength);

    // printf("Encoding time: %f (Compression ratio: %f)\n", time_spent, (double)(encodedInputLength) / (inputLength));

    /*printf("Encoded input: ");
    for(int i = 0; i < encodedInputLength; ++i) {
        printf("%s ", ulongToBinary(encoded_input[i], 8));
    }
    printf("\n");

    zstd_decode(encoded_input, output, 0, 0, encodedInputLength, inputLength);
    */

    /*printf("Output: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%d ", output[i]);
    }
    printf("\n");*/

    free(encoded_input);
    free(output);

    return 0;
}
