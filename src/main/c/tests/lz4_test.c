#include <time.h>

#include "../units/lz4/lz4.h"

#define DEBUG 0 

int main(int argc) {
    size_t inputLength = 1920*1080*4;
    // char input[] = { 0, 0, 0, 0, 5, 4, 4, 6 };
    char* input = (char*) malloc(inputLength);

    for(int i = 0; i < inputLength; ++i) { 
        input[i] = (char) (rand() % 256);
    }

    // size_t inputLength = sizeof(input);

#if DEBUG
    printf("Input: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%d ", input[i]);
    }
    printf("\n");
#endif

    size_t encodedInputMaxLength = LZ4_compressBound(inputLength);

    char* encoded_input = (char*) malloc(encodedInputMaxLength);
    char* output = (char*) malloc(inputLength);

    int encodedInputLength = lz4_encode(input, encoded_input, 0, 0, inputLength, encodedInputMaxLength);

    // printf("Encoding time: %f (Compression ratio: %f)\n", time_spent, (double)(encodedInputLength) / (inputLength));

    /*printf("Encoded input: ");
    for(int i = 0; i < encodedInputLength; ++i) {
        printf("%s ", ulongToBinary(encoded_input[i], 8));
    }
    printf("\n");*/

    lz4_decode(encoded_input, output, 0, 0, encodedInputLength, inputLength);

#if DEBUG
    printf("Output: ");
    for(int i = 0; i < inputLength; ++i) {
        printf("%d ", output[i]);
    }
    printf("\n");
#endif

    unsigned int diff = 0;
    for(int i = 0; i < inputLength; ++i) {
        if(input[i] != output[i]) {
            ++diff;
        }
    }

    printf("Diff: %d\n", diff);

    free(input);
    free(encoded_input);
    free(output);

    return 0;
}
