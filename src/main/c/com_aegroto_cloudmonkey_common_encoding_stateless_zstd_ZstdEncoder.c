#include "include/zstd/com_aegroto_cloudmonkey_common_encoding_stateless_zstd_ZstdEncoder.h"

#include "units/zstd/zstd.h"

typedef struct {
  ZSTD_CCtx *cContext;
  ZSTD_DCtx *dContext;
} zstd_context_t;

JNIEXPORT long JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_zstd_ZstdEncoder_initNative
  (JNIEnv * env, jobject obj, jint compressionLevel) {
    zstd_context_t* ctx = malloc(sizeof(zstd_context_t));

    ctx->cContext = ZSTD_createCCtx();
    ZSTD_CCtx_reset(ctx->cContext, ZSTD_reset_session_and_parameters);

    ZSTD_CCtx_setParameter(ctx->cContext, ZSTD_c_compressionLevel, compressionLevel);

    ZSTD_CCtx_setParameter(ctx->cContext, ZSTD_c_dictIDFlag, 0);
    ZSTD_CCtx_setParameter(ctx->cContext, ZSTD_c_windowLog, ZSTD_WINDOWLOG_MAX_64);
    ZSTD_CCtx_setParameter(ctx->cContext, ZSTD_c_hashLog, ZSTD_HASHLOG_MAX);

    ctx->dContext = ZSTD_createDCtx();
    ZSTD_DCtx_reset(ctx->dContext, ZSTD_reset_session_and_parameters);

    return (long) ctx;
}

JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_zstd_ZstdEncoder_encodeNative
  (JNIEnv * env, jobject obj, jlong ctxAddress, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint inputLength, jint outCapacity) {
    zstd_context_t* ctx = (zstd_context_t*) ctxAddress;

    char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    return zstd_encode(ctx->cContext, input, output, inputOffset, outputOffset, inputLength, outCapacity);
}

JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_zstd_ZstdEncoder_decodeNative
  (JNIEnv * env, jobject obj, jlong ctxAddress, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint inputLength, jint outCapacity) {
    zstd_context_t* ctx = (zstd_context_t*) ctxAddress;

    char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    return zstd_decode(ctx->dContext, input, output, inputOffset, outputOffset, inputLength, outCapacity); 
}
