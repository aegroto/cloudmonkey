#include "units/pixel_downsampling/pixel_downsampling.h"

typedef struct pixel {
	unsigned char r, g, b, a;
} pixel_t;

#define PUT_PIXEL(output, o, pixel) output[o] = pixel; ++o;
#define GET_PIXEL(input, r, c) input[(r) * width + (c)]

#define DEBUG(str, ...) \
	do{ if(0) printf(str, ##__VA_ARGS__); } while(0)

int pixel_downsampling_encode(unsigned char *input, unsigned char *output, int inputOffset, int outputOffset, int width, int height) {
	DEBUG("--- Encoding...\n");

	unsigned int pixelsPerDsRow = (2 + (width-2)/2);
	unsigned int outSize = (width * 2 + pixelsPerDsRow * (height - 2)) * 4;

	pixel_t* inputArr = (pixel_t*) input;
	pixel_t* outputArr = (pixel_t*) output;

	unsigned int o = outputOffset;

	// Output first row (not downsampled)
	for(int c = 0; c < width; ++c) {
		pixel_t pixel = GET_PIXEL(inputArr, 0, c);
		PUT_PIXEL(outputArr, o, pixel);
	}

	for(int r = 1; r < height - 1; ++r) {
		pixel_t firstPixel = GET_PIXEL(inputArr, r, 0);
		PUT_PIXEL(outputArr, o, firstPixel);

		for(int c = 1; c < width - 1; ++c) {
			if(c % 2 == r % 2) {
				pixel_t pixel = GET_PIXEL(inputArr, r, c);
				PUT_PIXEL(outputArr, o, pixel);
			}
		}

		pixel_t lastPixel = GET_PIXEL(inputArr, r, width - 1);
		PUT_PIXEL(outputArr, o, lastPixel);
	}

	// Output last row (not downsampled)
	for(int c = 0; c < width; ++c) {
		pixel_t pixel = GET_PIXEL(inputArr, height-1, c);
		PUT_PIXEL(outputArr, o, pixel);
	}

	/*printf("Encoded input:");
	for(int i = 0; i < outSize; ++i) {
		pixel_t pixel = outputArr[i];
		printf("[%u %u %u %u] ", pixel.r, pixel.g, pixel.b, pixel.a);
	}
	printf("\n");*/

	return outSize;
}

#undef DEBUG
#define DEBUG(str, ...) \
	do{ if(0) printf(str, ##__VA_ARGS__); } while(0)

#define FOUR_WAY_INTERP 1
#define TWO_WAY_INTERP 0

void interpolate(pixel_t p0, pixel_t p1, pixel_t p2, pixel_t p3, pixel_t *out) {
#if FOUR_WAY_INTERP
	// 4-way mean interpolation 
	(*out).r = (p0.r + p1.r + p2.r + p3.r) / 4;
	(*out).g = (p0.g + p1.g + p2.g + p3.g) / 4;
	(*out).b = (p0.b + p1.b + p2.b + p3.b) / 4;
	(*out).a = (p0.a + p1.a + p2.a + p3.a) / 4;
#endif

#if TWO_WAY_INTERP
	// 2-way mean interpolation 
	(*out).r = (p1.r + p3.r) / 2;
	(*out).g = (p1.g + p3.g) / 2;
	(*out).b = (p1.b + p3.b) / 2;
	(*out).a = (p1.a + p3.a) / 2;
#endif
}

void pixel_downsampling_decode(unsigned char *input, unsigned char *output, int inputOffset, int outputOffset, int width, int height, int encodedInputLength) {
	DEBUG("--- Decoding...\n");

	unsigned int pixelsPerDsRow = (2 + (width-2)/2);

	pixel_t* inputArr = (pixel_t*) input;
	pixel_t* outputArr = (pixel_t*) output;

	unsigned int o = outputOffset;

	// Copy first row as it is (not downsampled)
	for(int c = 0; c < width; ++c) {
		pixel_t pixel = GET_PIXEL(inputArr, 0, c);
		PUT_PIXEL(outputArr, o, pixel);
	}

	pixel_t interpolatedPixel;

	// Output middle rows
	for(int r = 1; r < height - 1; ++r) {
		int prevRowBaseIndex = r > 1 ? width + (r-2) * pixelsPerDsRow : 0;
		int rowBaseIndex = width + (r-1) * pixelsPerDsRow;
		int nextRowBaseIndex = width + (r) * pixelsPerDsRow;

		PUT_PIXEL(outputArr, o, inputArr[rowBaseIndex]);

		int last_row_index = r % 2;

		for(int c = 1; c < width - 1; ++c) {
			if(c % 2 == r % 2) {
				interpolate(
					inputArr[prevRowBaseIndex + c/2],
					inputArr[rowBaseIndex + c/2 - 1],
					inputArr[nextRowBaseIndex + c/2],
					inputArr[rowBaseIndex + c/2],
					&interpolatedPixel
				);

				PUT_PIXEL(outputArr, o, interpolatedPixel);
			} else {
				PUT_PIXEL(outputArr, o, inputArr[rowBaseIndex + last_row_index]);
				++last_row_index;
			}
		}

		PUT_PIXEL(outputArr, o, inputArr[rowBaseIndex + pixelsPerDsRow - 1]);
	}

	// Copy last row as it is (not downsampled)
	for(int c = 0; c < width; ++c) {
		PUT_PIXEL(outputArr, o, inputArr[encodedInputLength/4 - width + c]);
	}
}