#include "units/huffman/huffman.h"

#define DEBUG(str, ...) \
	do { if(0) printf(str, ##__VA_ARGS__); } while(0)

char* intToBinary(int a, int length) {
    char* out = malloc(sizeof(char) * (length + 1));
    for(int i = 0; i < length; ++i)
        out[i] = '0';
    out[length] = 0;

    int i = length-1;

    do {
        out[i--] = '0' + a % 2;
        a /= 2;
    } while(a > 0);

    return out;
}

void lookForCodes(CodeEntry* table, HuffmanNode** tree, HuffmanNode* root, int currentCode, int currentLength) {
    if(IS_LEAF_P(root)) {
        table[root->value].code = currentCode; 
        table[root->value].length = currentLength; 
    } else {
        lookForCodes(table, tree, tree[root->left], (currentCode << 1), currentLength + 1);
        lookForCodes(table, tree, tree[root->right], ((currentCode << 1) | 0b1), currentLength + 1);
    }
}

int huffman_encode(unsigned char *input, unsigned char *output, int inputOffset, int outputOffset, int inputLength)
{
	DEBUG("--- Encoding...\n");

	// Code base phase

    unsigned int frequencies[256] = { 0 };

    for(int i = 0; i < 256; ++i)
        frequencies[i] = 0;

    for(int i = inputOffset; i < inputOffset + inputLength; ++i) {
        ++frequencies[input[i]];
    }

    HuffmanPriorityQueue nodesQueue;
    HuffmanPriorityQueue_init(&nodesQueue, 256);
    
    for(int i = 0; i < 256; ++i) {
        if(frequencies[i] > 0) {
          HuffmanNode* node = (HuffmanNode*) malloc(sizeof(HuffmanNode));
          node->value = i;
          node->frequency = frequencies[i];
          node->right = -1;
          node->left = -1;
          HuffmanPriorityQueue_add(&nodesQueue, node);
        }
    }

    int treeLength = 2 * nodesQueue.size - 1;
    HuffmanNode* huffmanTree[treeLength];

    for(int i = 0; i < treeLength; ++i) {
        huffmanTree[i] = NULL;
    }

    int currentTreeIndex = treeLength - 1;

    while(nodesQueue.size > 1) {        
        HuffmanNode* firstNode = HuffmanPriorityQueue_pick(&nodesQueue);
        HuffmanNode* secondNode = HuffmanPriorityQueue_pick(&nodesQueue);
        
        if(IS_LEAF_P(firstNode)) {
            firstNode->index = currentTreeIndex;
            huffmanTree[firstNode->index] = firstNode;
            --currentTreeIndex;
        }

        if(IS_LEAF_P(secondNode)) {
            secondNode->index = currentTreeIndex;
            huffmanTree[secondNode->index] = secondNode;
            --currentTreeIndex;
        }

        HuffmanNode* fatherNode = malloc(sizeof(HuffmanNode));
        fatherNode->value = (char) 0;
        fatherNode->frequency = firstNode->frequency + secondNode->frequency;
        fatherNode->right = firstNode->index;
        fatherNode->left = secondNode->index;

        fatherNode->index = currentTreeIndex;
        huffmanTree[fatherNode->index] = fatherNode;
        --currentTreeIndex;

        HuffmanPriorityQueue_add(&nodesQueue, fatherNode);
    }

    int currentOutputIndex = outputOffset;
    PUT_INT(output, currentOutputIndex, treeLength * 5);
    currentOutputIndex += 8;

    for(int i = 0; i < treeLength; ++i) {
        PUT_BYTE(output, currentOutputIndex, huffmanTree[i]->value);
        currentOutputIndex += 1; 
        PUT_SHORT(output, currentOutputIndex, huffmanTree[i]->right);
        currentOutputIndex += 2;
        PUT_SHORT(output, currentOutputIndex, huffmanTree[i]->left);
        currentOutputIndex += 2;
    }

    CodeEntry codesTable[256];

    lookForCodes(codesTable, huffmanTree, huffmanTree[0], 0b0, 0);

    DEBUG("Codes table:\n");
    for(int i = 0; i < 256; ++i) {
        if(frequencies[i] > 0) {
            DEBUG("%d -> %s (%d)\n", i, intToBinary(codesTable[i].code, codesTable[i].length), codesTable[i].length);
        }
    }

	// Encoding phase

	unsigned int i = inputOffset;
	const unsigned int iLimit = inputOffset + inputLength;

	unsigned char* outPointer = output + outputOffset + 8 + treeLength * 5;
    *outPointer = 0;

	unsigned int outputLength = 0;

    unsigned int writtenChunkBits = 0;

	while(i < iLimit) {
		unsigned char value = input[i];
		unsigned char code = codesTable[value].code;
		unsigned char length = codesTable[value].length;

		// DEBUG("Value: %d, Code: %s (%d)\n", value, ulongToBinary(code, length), length);

        if(8 - writtenChunkBits >= length) {
		    *outPointer = *outPointer | (code << (8 - writtenChunkBits - length));
            writtenChunkBits += length;
        } else {
            unsigned int firstWritableBits = 8 - writtenChunkBits;
            *outPointer = *outPointer | (code >> length - firstWritableBits);

            // DEBUG("outPointer (first write): %s (%d)\n", ulongToBinary(*outPointer, 8), writtenChunkBits);

            ++outPointer;
            *outPointer = 0;

            *outPointer = *outPointer | (code << (8 - (length - firstWritableBits)));

            writtenChunkBits = length - firstWritableBits;

            // DEBUG("outPointer (second write): %s (%d)\n", ulongToBinary(*outPointer, 8), writtenChunkBits);
        }

        // DEBUG("outPointer: %s (%d)\n", ulongToBinary(*outPointer, 8), writtenChunkBits);

        outputLength += length;

		++i;
	}

    PUT_INT(output, outputOffset + 4, outputLength);

    return 8 + treeLength * 5 + (outputLength + 8 - 1) / 8; 
}

#undef DEBUG
#define DEBUG(str, ...) \
	do { if(0) printf(str, ##__VA_ARGS__); } while(0)

void huffman_decode(unsigned char *input, unsigned char *output, int inputOffset, int outputOffset, int inputLength) {
	DEBUG("--- Decoding...\n");

    unsigned int treeBytesSize = GET_INT(input, 0);
    unsigned int inputBitsLength = GET_INT(input, 4);

    DEBUG("Tree bytes size: %d, Input bits length: %d\n", treeBytesSize, inputBitsLength);

    unsigned int treeSize = treeBytesSize / 5;

    HuffmanNode huffmanTree[treeSize];

    unsigned int i = inputOffset + 8;

    for(int t = 0; t < treeSize; ++t) {
        huffmanTree[t].value = GET_BYTE(input, i);
        i += 1;
        huffmanTree[t].right = GET_SHORT(input, i);
        i += 2;
        huffmanTree[t].left = GET_SHORT(input, i);
        i += 2;
    }

    /*for(int t = 0; t < treeSize; ++t) {
        DEBUG("[%d] Tree node: %s %d %d\n", t, ulongToBinary(huffmanTree[t].value, 8), huffmanTree[t].right, huffmanTree[t].left);
    }*/

    unsigned int decodedBits = 0;
    
    unsigned char* chunkPointer = (unsigned char*) (input + 8 + treeBytesSize);
    unsigned int readChunkBits = 0;

    unsigned char bitmask = 0b10000000;
    unsigned char bitshift = 7;

    unsigned nodeIndex = 0;

    unsigned int o = outputOffset;

    while(decodedBits < inputBitsLength) {
        unsigned char currentBit = (*chunkPointer & bitmask) >> bitshift;

        DEBUG("Current bit: %d (%d)\n", currentBit, bitmask);

        nodeIndex = currentBit == 0 ? huffmanTree[nodeIndex].left : huffmanTree[nodeIndex].right;

        if(IS_LEAF(huffmanTree[nodeIndex])) {
            DEBUG("Outputting value: %d\n", huffmanTree[nodeIndex].value);

            output[o] = huffmanTree[nodeIndex].value;
            ++o;

            nodeIndex = 0;
        }

        if(bitmask == 0b00000001) {
            ++chunkPointer;

            bitmask = 0b10000000;
            bitshift = 7;
        } else {
            bitmask = bitmask >> 1;
            bitshift -= 1;
        }

        ++decodedBits;
    }
}