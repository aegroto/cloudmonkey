#include "units/differential_compare/differential_compare.h"

int differential_compare_encode(unsigned char* firstInput, unsigned char* secondInput, unsigned char* output, int firstInputOffset, int secondInputOffset, int outputOffset, int inputLength) {
    for(int i = 0; i < inputLength; ++i) {
      output[outputOffset + i] = secondInput[secondInputOffset + i] - firstInput[firstInputOffset + i];
    }
    return inputLength;
}

void differential_compare_decode(unsigned char* firstInput, unsigned char* secondInput, unsigned char* output, int firstInputOffset, int secondInputOffset, int outputOffset, int inputLength) {
    for(int i = 0; i < inputLength; ++i) {
      output[outputOffset + i] = secondInput[secondInputOffset + i] + firstInput[firstInputOffset + i];
    }
}