#include "units/runlength/runlength.h"

#define PUT_INT(array, i, x) *((unsigned int *)(array + i)) = x
#define GET_INT(array, i) *((unsigned int *)(array + i))

#define DEBUG(str, ...) \
	do{ if(0) printf(str, ##__VA_ARGS__); } while(0)

#define WRITE_ENTRY                                                                                                                                                            \
	*entryPointer = *entryPointer | (lastValue >> writtenChunkBits);                                                                                                           \
	DEBUG("wrote value (first part): %s\n", ulongToBinary(*entryPointer, 8));                                                                                                  \
	++entryPointer;                                                                                                                                                            \
	*entryPointer = 0;                                                                                                                                                         \
	*entryPointer = *entryPointer | (unsigned char)((lastValue << 8 - writtenChunkBits) & 0xFF);                                                                               \
	DEBUG("wrote value (second part): %s, written chunk bits: %d\n", ulongToBinary(*entryPointer, 8), writtenChunkBits);                                                       \
                                                                                                                                                                               \
	shiftedOccs = writtenChunkBits + bitsPerLength <= 8 ? occurrences << (8 - bitsPerLength - writtenChunkBits) : occurrences >> -(8 - bitsPerLength - writtenChunkBits); \
	*entryPointer = *entryPointer | shiftedOccs;                                                                                                                               \
	writtenChunkBits += bitsPerLength;                                                                                                                                         \
                                                                                                                                                                               \
	DEBUG("wrote occurrences (first part): %s, written chunk bits: %d\n", ulongToBinary(*entryPointer, 8), writtenChunkBits);                                                  \
                                                                                                                                                                               \
	if (writtenChunkBits >= 8)                                                                                                                                                 \
	{                                                                                                                                                                          \
		++entryPointer;                                                                                                                                                        \
		*entryPointer = 0;                                                                                                                                                     \
		writtenChunkBits -= 8;                                                                                                                                                 \
	}                                                                                                                                                                          \
	*entryPointer = *entryPointer | (occurrences << ((8 - bitsPerLength) + (bitsPerLength - writtenChunkBits)));                                                               \
	DEBUG("wrote occurrences (second part): %s, written chunk bits: %d\n", ulongToBinary(*entryPointer, 8), writtenChunkBits);

int runlength_encode(char *input, char *output, int inputOffset, int outputOffset, int inputLength)
{
	DEBUG("--- Encoding...\n");

	unsigned char currentValue = input[inputOffset];
	int jumps = 0;

	unsigned int i = inputOffset + 1;
	const unsigned int iLimit = inputOffset + inputLength;

	while (i < iLimit) {
		unsigned char nextValue = input[i++];

		if (currentValue != nextValue) {
			currentValue = nextValue;
			++jumps;
		}
	}

	int meanRunLength = (inputLength + jumps) / (jumps + 1);

	int bitsPerLength = meanRunLength > 1 ? min(1 + log2(meanRunLength) + (isPowerOfTwo(meanRunLength) ? 0 : 1), 8) : 1;

	DEBUG("Mean run length: %d\n", meanRunLength);
	DEBUG("Bits per length: %d\n", bitsPerLength);

	i = inputOffset + 1;

	unsigned int o = 0;
	unsigned int outputLength = 0;

	currentValue = input[inputOffset];
	unsigned char lastValue = currentValue;

	unsigned long occurrences = 1;

	const unsigned long maxOccurrences = (1L << bitsPerLength) - 1;

	o = outputOffset + 8;

	unsigned char *entryPointer = (unsigned char *)(output + outputOffset + 8);
	*entryPointer = 0;

	unsigned int writtenChunkBits = 0;

	unsigned const int chunksPerLength = (bitsPerLength + 8 - 1) / 8;

	unsigned char shiftedOccs;

	while (i < iLimit) {
		currentValue = input[i];

		if (currentValue == lastValue) {
			if (occurrences < maxOccurrences) {
				++occurrences;
			} else {
				DEBUG("[max occs] entry (%d, %lu)\n", lastValue, occurrences);

				WRITE_ENTRY

				outputLength += 8 + bitsPerLength;

				occurrences = 1;
			}
		} else {
			DEBUG("[jump] entry (%d, %lu)\n", lastValue, occurrences);

			WRITE_ENTRY

			outputLength += 8 + bitsPerLength;

			lastValue = currentValue;
			occurrences = 1;
		}

		++i;
	}

	DEBUG("[last pair] entry (%d, %lu)\n", lastValue, occurrences);

	WRITE_ENTRY

	outputLength += 8 + bitsPerLength;

	memcpy(output + outputOffset, &outputLength, sizeof(outputLength));
	memcpy(output + outputOffset + 4, &bitsPerLength, sizeof(bitsPerLength));

	return 8 + ((outputLength + 8 - 1) / 8);
}

#undef DEBUG
#define DEBUG(str, ...) \
	do{ if(0) printf(str, ##__VA_ARGS__); } while(0)

void runlength_decode(char *input, char *output, int inputOffset, int outputOffset, int inputLength) {
	DEBUG("--- Decoding...\n");

	unsigned int inputBitsLength = GET_INT(input, inputOffset);
	unsigned int bitsPerLength = GET_INT(input, inputOffset + 4);

	DEBUG("Input bits length: %d, bits per length: %d\n", inputBitsLength, bitsPerLength);

	unsigned int decodedBits = 0;

	unsigned char* chunkPointer = input + inputOffset + 8;
	unsigned int readChunkBits = 0;

	unsigned int o = outputOffset;

	while(decodedBits < inputBitsLength) {
		unsigned char value = 0;
		value |= *chunkPointer << readChunkBits;
		++chunkPointer;
		value |= *chunkPointer >> 8 - readChunkBits;	

		unsigned char occurrences = 0;
		occurrences |= ((*chunkPointer << readChunkBits) & 0xFF) >> (8 - bitsPerLength);
		readChunkBits += bitsPerLength;
		DEBUG("occurrences (first read): %s, read chunk bits: %d\n", ulongToBinary(occurrences, 8), readChunkBits);

		if(readChunkBits >= 8) {
			++chunkPointer;
			readChunkBits -= 8;

			occurrences |= (*chunkPointer >> (8 - readChunkBits));
		}
		DEBUG("occurrences (second read): %s, read chunk bits: %d\n", ulongToBinary(occurrences, 8), readChunkBits);

		if(readChunkBits >= 8) {
			++chunkPointer;

			readChunkBits -= 8;
		}

		decodedBits += 8 + bitsPerLength;

		DEBUG("Value: %d (%s), occurrences: %d (%s), decodedBits: %d/%d\n", value, ulongToBinary(value, 8), occurrences, ulongToBinary(occurrences, 8), decodedBits, inputBitsLength);

		unsigned int oLimit = o + occurrences;

		memset(output + o, value, occurrences);
		o += occurrences;
	}
}