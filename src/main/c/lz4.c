#include "units/lz4/lz4.h"

#define DEBUG(str, ...) \
	do{ if(0) printf(str, ##__VA_ARGS__); } while(0)

int lz4_encode(char *input, char *output, int inputOffset, int outputOffset, int inputLength, int dstCapacity) {
	DEBUG("--- Encoding...\n");

	size_t outVal = LZ4_compress_fast( 
		(char*) (input + inputOffset),
		(char*) (output + outputOffset),
		inputLength,
		dstCapacity,
		1
	);

	return outVal;
}

#undef DEBUG
#define DEBUG(str, ...) \
	do{ if(0) printf(str, ##__VA_ARGS__); } while(0)

int lz4_decode(char *input, char *output, int inputOffset, int outputOffset, int encodedInputLength, int dstCapacity) {
	DEBUG("--- Decoding...\n");

	int outVal = LZ4_decompress_safe(
	 	(char*) (input + inputOffset),
		(char*) (output + outputOffset),
		encodedInputLength,
		dstCapacity
	);

	return outVal;
}