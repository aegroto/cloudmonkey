#ifndef DIFFERENTIAL_COMPARE_UNIT_H
#define DIFFERENTIAL_COMPARE_UNIT_H

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../include/utils.h"

int differential_compare_encode(unsigned char* firstInput, unsigned char* secondInput, unsigned char* output, int firstInputOffset, int secondInputOffset, int outputOffset, int inputLength);
void differential_compare_decode(unsigned char* firstInput, unsigned char* secondInput, unsigned char* output, int firstInputOffset, int secondInputOffset, int outputOffset, int inputLength);

#endif