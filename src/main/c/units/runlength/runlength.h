#ifndef RUNLENGTH_UNIT_H
#define RUNLENGTH_UNIT_H

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../include/utils.h"

int runlength_encode(char* input, char* output, int inputOffset, int outputOffset, int inputLength);
void runlength_decode(char* input, char* output, int inputOffset, int outputOffset, int inputLength);

#endif