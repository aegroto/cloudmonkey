#ifndef LZ4_UNIT_H
#define LZ4_UNIT_H

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../include/utils.h"

#define LZ4_STATIC_LINKING_ONLY
#include "../../include/lz4/lz4.h"

int lz4_encode(char* input, char* output, int inputOffset, int outputOffset, int inputLength, int dstCapacity);
int lz4_decode(char* input, char* output, int inputOffset, int outputOffset, int inputLength, int dstCapacity);

#endif