#ifndef HUFFMAN_UNIT_H
#define HUFFMAN_UNIT_H

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../include/huffman_encoder/structures.h"
#include "../../include/huffman_encoder/priority_queue.h"
#include "../../include/huffman_encoder/array_utils.h"
#include "../../include/utils.h"

int huffman_encode(unsigned char* input, unsigned char* output, int inputOffset, int outputOffset, int inputLength);
void huffman_decode(unsigned char* input, unsigned char* output, int inputOffset, int outputOffset, int inputLength);

#endif