#ifndef CANONICAL_HUFFMAN_UNIT_H
#define CANONICAL_HUFFMAN_UNIT_H

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int canonical_huffman_encode(unsigned char* input, unsigned char* output, int inputOffset, int outputOffset, int inputLength);
void canonical_huffman_decode(unsigned char* input, unsigned char* output, int inputOffset, int outputOffset, int inputLength);

#endif