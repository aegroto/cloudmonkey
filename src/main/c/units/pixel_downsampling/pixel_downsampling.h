#ifndef PIXEL_DS_UNIT_H
#define PIXEL_DS_UNIT_H

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../include/utils.h"

int pixel_downsampling_encode(unsigned char* input, unsigned char* output, int inputOffset, int outputOffset, int width, int height);
void pixel_downsampling_decode(unsigned char* input, unsigned char* output, int inputOffset, int outputOffset, int width, int height, int encodedInputLength);

#endif