#ifndef APPROXIMATION_UNIT_H
#define APPROXIMATION_UNIT_H

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../include/utils.h"

int approximation_encode(unsigned char* input, unsigned char* output, int inputOffset, int outputOffset, int inputLength);
void approximation_decode(unsigned char* input, unsigned char* output, int inputOffset, int outputOffset, int inputLength);

#endif