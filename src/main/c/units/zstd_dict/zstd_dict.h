#ifndef ZSTD_DICT_UNIT_H
#define ZSTD_DICT_UNIT_H

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../include/utils.h"

#define ZSTD_STATIC_LINKING_ONLY
#include "../../include/zstd/zstd.h"

int zstd_dict_encode(ZSTD_CDict* cdict, ZSTD_CCtx *context, char* input, char* output, int inputOffset, int outputOffset, int inputLength, int dstCapacity);
int zstd_dict_decode(ZSTD_DDict* ddict, char* input, char* output, int inputOffset, int outputOffset, int inputLength, int dstCapacity);

#endif