#ifndef ZSTD_UNIT_H
#define ZSTD_UNIT_H

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../include/utils.h"

#define ZSTD_STATIC_LINKING_ONLY
#include "../../include/zstd/zstd.h"

int zstd_encode(ZSTD_CCtx *cContext, char* input, char* output, int inputOffset, int outputOffset, int inputLength, int dstCapacity);
int zstd_decode(ZSTD_DCtx *dContext, char* input, char* output, int inputOffset, int outputOffset, int inputLength, int dstCapacity);

#endif