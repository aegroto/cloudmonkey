#include "include/pixel_downsampling/com_aegroto_cloudmonkey_common_encoding_stateless_downsampling_NativePixelDownsamplingEncoder.h"

#include "units/pixel_downsampling/pixel_downsampling.h"

JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_downsampling_NativePixelDownsamplingEncoder_encodeNative
  (JNIEnv * env, jobject obj, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint width, jint height) {
  	char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    return pixel_downsampling_encode(input, output, inputOffset, outputOffset, width, height);
}

JNIEXPORT void JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_downsampling_NativePixelDownsamplingEncoder_decodeNative
  (JNIEnv * env, jobject obj, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint width, jint height, jint encodedInputLength) {
  	char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    pixel_downsampling_decode(input, output, inputOffset, outputOffset, width, height, encodedInputLength);
}
