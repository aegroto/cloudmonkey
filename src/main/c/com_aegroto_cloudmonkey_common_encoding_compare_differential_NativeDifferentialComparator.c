#include "include/differential_comparator/com_aegroto_cloudmonkey_common_encoding_compare_differential_NativeDifferentialComparator.h"

#include "units/differential_compare/differential_compare.h"

JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_compare_differential_NativeDifferentialComparator_encodeNative
  (JNIEnv * env, jobject obj, jobject firstInputBuffer, jobject secondInputBuffer, jobject outputBuffer, jint firstInputOffset, jint secondInputOffset, jint outputOffset, jint inputLength) {
    unsigned char* firstInput = (unsigned char*) (*env)->GetDirectBufferAddress(env, firstInputBuffer);
    unsigned char* secondInput = (unsigned char*) (*env)->GetDirectBufferAddress(env, secondInputBuffer);
    unsigned char* output = (unsigned char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    return differential_compare_encode(firstInput, secondInput, output, firstInputOffset, secondInputOffset, outputOffset, inputLength);
}

JNIEXPORT void JNICALL Java_com_aegroto_cloudmonkey_common_encoding_compare_differential_NativeDifferentialComparator_decodeNative
  (JNIEnv * env, jobject obj, jobject firstInputBuffer, jobject secondInputBuffer, jobject outputBuffer, jint firstInputOffset, jint secondInputOffset, jint outputOffset, jint inputLength) {

    unsigned char* firstInput = (char*) (*env)->GetDirectBufferAddress(env, firstInputBuffer);
    unsigned char* secondInput = (char*) (*env)->GetDirectBufferAddress(env, secondInputBuffer);
    unsigned char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    differential_compare_decode(firstInput, secondInput, output, firstInputOffset, secondInputOffset, outputOffset, inputLength);
}

