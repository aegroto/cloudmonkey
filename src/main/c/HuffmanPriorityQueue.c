#include "include/huffman_encoder/priority_queue.h"

#include <stdio.h>

void HuffmanPriorityQueue_init(HuffmanPriorityQueue* hpQueue, size_t capacity) {
    hpQueue->size = 0;
    hpQueue->heap = (HuffmanNode**) malloc(sizeof(HuffmanNode*) * capacity);
    hpQueue->capacity = capacity;
}

void HuffmanPriorityQueue_add(HuffmanPriorityQueue* hpQueue, HuffmanNode* element) {
    if(hpQueue->size < hpQueue->capacity) {
        int currentIndex = hpQueue->size;
        HuffmanNode** heap = hpQueue->heap;

        heap[currentIndex] = element;

        while(currentIndex > 0 && heap[HPARENT(currentIndex)]->frequency > element->frequency) {
            HuffmanNode* tmp = heap[HPARENT(currentIndex)];
            heap[HPARENT(currentIndex)] = element;
            heap[currentIndex] = tmp;

            currentIndex = HPARENT(currentIndex);
        }
            

        ++(hpQueue->size);
    }
}

HuffmanNode* HuffmanPriorityQueue_pick(HuffmanPriorityQueue* hpQueue) {
    if(hpQueue->size == 0)
        return NULL;

    HuffmanNode** heap = hpQueue->heap;
    HuffmanNode* root = heap[0];

    int currentIndex = 0;
    heap[currentIndex] = heap[hpQueue->size - 1];
    --(hpQueue->size);

    if(hpQueue->size > 1) {
        int smallestIndex = currentIndex; 
        
        do {
            int left = HLEFT(currentIndex);
            int right = HRIGHT(currentIndex);

            if(heap[smallestIndex]->frequency > heap[left]->frequency) {
                smallestIndex = left;
            }

            if(heap[smallestIndex]->frequency > heap[right]->frequency) {
                smallestIndex = right;
            }

            if(smallestIndex != currentIndex) {
                HuffmanNode* tmp = heap[currentIndex];
                heap[currentIndex] = heap[smallestIndex];
                heap[smallestIndex] = tmp;
                currentIndex = smallestIndex;
            } else break;

        } while(currentIndex < hpQueue->size / 2);
    }

    return root; 
}