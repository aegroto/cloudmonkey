#include "units/canonical_huffman/canonical_huffman.h"
#include "include/huffman_encoder/priority_queue.h"

#define DEBUG(str, ...) \
	do { if(1) printf(str, ##__VA_ARGS__); } while(0)

int canonical_huffman_encode(unsigned char *input, unsigned char *output, int inputOffset, int outputOffset, int inputLength)
{
	DEBUG("--- Encoding...\n");

    // TODO: Differential encoding
    /*unsigned char* diff_input = malloc(inputLength * sizeof(unsigned char));

    diff_input[0] = input[inputOffset];
    diff_input[1] = input[inputOffset + 1];
    diff_input[2] = input[inputOffset + 2];
    diff_input[3] = input[inputOffset + 3];

    unsigned int i = 4;
    while(i < inputLength) {
        diff_input[i] = input[inputOffset + i - 4] - input[inputOffset + i];
        ++i;
        diff_input[i] = input[inputOffset + i - 4] - input[inputOffset + i];
        ++i;
        diff_input[i] = input[inputOffset + i - 4] - input[inputOffset + i];
        ++i;
        diff_input[i] = input[inputOffset + i - 4] - input[inputOffset + i];
        ++i;
    }*/

    unsigned int frequencies[256] = { 0 };

    for(int i = 0; i < inputLength; ++i) {
        ++frequencies[input[i]];
    }

    for(int i = 0; i < 256; ++i) {
        if(frequencies[i] > 0) {
            printf("[%d] = %d\n", i, frequencies[i]);
        }
    }

    return 0;
}

#undef DEBUG
#define DEBUG(str, ...) \
	do { if(1) printf(str, ##__VA_ARGS__); } while(0)

void canonical_huffman_decode(unsigned char *input, unsigned char *output, int inputOffset, int outputOffset, int inputLength) {
	DEBUG("--- Decoding...\n");
}