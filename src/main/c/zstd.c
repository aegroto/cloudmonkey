#include "units/zstd/zstd.h"

#define DEBUG(str, ...) \
	do{ if(0) printf(str, ##__VA_ARGS__); } while(0)

int zstd_encode(ZSTD_CCtx *cContext, char *input, char *output, int inputOffset, int outputOffset, int inputLength, int dstCapacity) {
	DEBUG("--- Encoding...\n");

	size_t outVal = ZSTD_compress2(cContext, 
		(char*) (output + outputOffset), dstCapacity,
		(char*) (input + inputOffset), inputLength
	);

    ZSTD_CCtx_reset(cContext, ZSTD_reset_session_only);

	return outVal;
}

#undef DEBUG
#define DEBUG(str, ...) \
	do{ if(0) printf(str, ##__VA_ARGS__); } while(0)

int zstd_decode(ZSTD_DCtx *dContext, char *input, char *output, int inputOffset, int outputOffset, int encodedInputLength, int dstCapacity) {
	DEBUG("--- Decoding...\n");

	int outVal = ZSTD_decompressDCtx(dContext,
		(char*) (output + outputOffset), dstCapacity,
	 	(char*) (input + inputOffset), encodedInputLength
	);

    ZSTD_DCtx_reset(dContext, ZSTD_reset_session_only);

	return outVal;
}