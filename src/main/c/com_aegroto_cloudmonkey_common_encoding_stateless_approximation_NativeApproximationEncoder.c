#include "include/approximation/com_aegroto_cloudmonkey_common_encoding_stateless_approximation_NativeApproximationEncoder.h"

#include "units/approximation/approximation.h"

JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_approximation_NativeApproximationEncoder_encodeNative
  (JNIEnv * env, jobject obj, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint inputLength) {
  	char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    return approximation_encode(input, output, inputOffset, outputOffset, inputLength);
}

JNIEXPORT void JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_approximation_NativeApproximationEncoder_decodeNative
  (JNIEnv * env, jobject obj, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint encodedInputLength) {
  	char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    approximation_decode(input, output, inputOffset, outputOffset, encodedInputLength);
}
