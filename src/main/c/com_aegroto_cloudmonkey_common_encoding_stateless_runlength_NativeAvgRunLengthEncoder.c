#include "include/runlength/com_aegroto_cloudmonkey_common_encoding_stateless_runlength_NativeAvgRunLengthEncoder.h"

#include "units/runlength/runlength.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>

// 00001100 00110000 11010001 00001100 10110000 11010001 00001100 01110000 10110001  

JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_runlength_NativeAvgRunLengthEncoder_encodeNative
  (JNIEnv * env, jobject obj, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint inputLength) {
    char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    return runlength_encode(input, output, inputOffset, outputOffset, inputLength);
}

JNIEXPORT void JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_runlength_NativeAvgRunLengthEncoder_decodeNative
  (JNIEnv * env, jobject obj, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint inputLength) {
    char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    runlength_decode(input, output, inputOffset, outputOffset, inputLength);
}
