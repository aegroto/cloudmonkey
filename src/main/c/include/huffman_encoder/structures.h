#ifndef HUFFMAN_STRUCTURES_H
#define HUFFMAN_STRUCTURES_H

#define IS_LEAF_P(pnode) (pnode->right == pnode->left) && (pnode->left == -1)
#define IS_LEAF(node) (node.right == node.left) && (node.left == -1)

typedef struct _HuffmanNode {
    unsigned char value;
    int frequency;
    short right, left, index;
} HuffmanNode;

typedef struct _CodeEntry {
    int code;
    char length;
} CodeEntry;

#endif