#ifndef HUFFMAN_PRIORITY_QUEUE_H
#define HUFFMAN_PRIORITY_QUEUE_H

#include <stdlib.h>
#include "structures.h"

#define HPARENT(i) (i-1)/2
#define HLEFT(i) i*2 + 1 
#define HRIGHT(i) i*2 + 2

typedef struct _HuffmanPriorityQueue {
    HuffmanNode** heap;
    size_t capacity, size;
} HuffmanPriorityQueue;

void HuffmanPriorityQueue_init(HuffmanPriorityQueue* hpQueue, size_t capacity);
void HuffmanPriorityQueue_add(HuffmanPriorityQueue* hpQueue, HuffmanNode* element);
HuffmanNode* HuffmanPriorityQueue_pick(HuffmanPriorityQueue* hpQueue);


#endif