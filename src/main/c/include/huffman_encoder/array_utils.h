#ifndef ARRAY_UTILS_H
#define ARRAY_UTILS_H

#define PUT_BYTE(arr, i, b) arr[i] = b
#define PUT_INT(arr, i, d) *((int*)(arr + i)) = d
#define PUT_SHORT(arr, i, s) *((short*)(arr + i)) = s

#define GET_BYTE(arr, i) arr[i]
#define GET_INT(arr, i) *((int*)(arr + i))
#define GET_SHORT(arr, i) *((short*)(arr + i))

#endif