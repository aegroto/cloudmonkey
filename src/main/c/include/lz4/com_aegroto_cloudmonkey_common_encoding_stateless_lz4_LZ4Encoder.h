/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder */

#ifndef _Included_com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder
#define _Included_com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder
 * Method:    encodeNative
 * Signature: (JLjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIII)I
 */
JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder_encodeNative
  (JNIEnv *, jobject, jobject, jobject, jint, jint, jint, jint);

/*
 * Class:     com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder
 * Method:    decodeNative
 * Signature: (JLjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIII)I
 */
JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder_decodeNative
  (JNIEnv *, jobject, jobject, jobject, jint, jint, jint, jint);

/*
 * Class:     com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder
 * Method:    nativeOutputBufferMinLength
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder_nativeOutputBufferMinLength
  (JNIEnv *, jobject, jint);

#ifdef __cplusplus
}
#endif
#endif
