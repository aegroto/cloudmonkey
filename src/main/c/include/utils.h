#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>

char* ulongToBinary(unsigned long a, int length); 
int min(int a, int b);
int isPowerOfTwo(unsigned int x);

#endif