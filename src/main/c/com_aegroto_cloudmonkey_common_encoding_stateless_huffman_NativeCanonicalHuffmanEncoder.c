#include "include/canonical_huffman_encoder/com_aegroto_cloudmonkey_common_encoding_stateless_huffman_NativeCanonicalHuffmanEncoder.h"

#include "units/canonical_huffman/canonical_huffman.h"

JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_huffman_NativeCanonicalHuffmanEncoder_encodeNative
  (JNIEnv * env, jobject obj, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint inputLength) {
  	char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    // size_t outputLength = (size_t) (*env)->GetDirectBufferCapacity(env, outputBuffer);

    return canonical_huffman_encode(input, output, inputOffset, outputOffset, inputLength);
}

JNIEXPORT void JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_huffman_NativeCanonicalHuffmanEncoder_decodeNative
  (JNIEnv* env, jobject obj, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint inputLength) {
  	char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    canonical_huffman_decode(input, output, inputOffset, outputOffset, inputLength);
}