#include "include/lz4/com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder.h"

#include "units/lz4/lz4.h"

JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder_encodeNative
  (JNIEnv * env, jobject obj, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint inputLength, jint outCapacity) {

    char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    return lz4_encode(input, output, inputOffset, outputOffset, inputLength, outCapacity);
}

JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder_decodeNative
  (JNIEnv * env, jobject obj, jobject inputBuffer, jobject outputBuffer, jint inputOffset, jint outputOffset, jint inputLength, jint outCapacity) {
    char* input = (char*) (*env)->GetDirectBufferAddress(env, inputBuffer);
    char* output = (char*) (*env)->GetDirectBufferAddress(env, outputBuffer);

    return lz4_decode(input, output, inputOffset, outputOffset, inputLength, outCapacity); 
}

JNIEXPORT jint JNICALL Java_com_aegroto_cloudmonkey_common_encoding_stateless_lz4_LZ4Encoder_nativeOutputBufferMinLength
  (JNIEnv * env, jobject obj, jint inputLength) {
    return LZ4_COMPRESSBOUND(inputLength);
}
