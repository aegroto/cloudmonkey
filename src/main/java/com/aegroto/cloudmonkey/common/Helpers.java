package com.aegroto.cloudmonkey.common;

import com.jme3.math.FastMath;

public class Helpers {

    public static void putIntInByteArray(byte[] array, int a, int offset) {
        array[offset + 0] = (byte) (a >> 24);
        array[offset + 1] = (byte) (a >> 16);
        array[offset + 2] = (byte) (a >> 8);
        array[offset + 3] = (byte) (a >> 0);
    }

    public static int getIntFromByteArray(byte[] array, int offset) {
        return (array[offset + 0]) << 24 |
               (array[offset + 1] & 0xFF) << 16 |
               (array[offset + 2] & 0xFF) << 8  |
               (array[offset + 3] & 0xFF) << 0;
    } 
    
    public static void putShortInByteArray(byte[] array, short a, int offset) {
        array[offset + 0] = (byte) (a >> 8);
        array[offset + 1] = (byte) (a >> 0);
    }

    public static short getShortFromByteArray(byte[] array, int offset) {
        return (short) ((array[offset + 0]) << 8 |
                       (array[offset + 1] & 0xFF) << 0);
    } 

    public static void putBitsInByteArray(byte[] array, byte bits, int size, int offset) {
        int byteOffset = offset % 8;
        int firstSlot = offset / 8;
        int firstSlotLeftShift = 8 - size - byteOffset;
        byte firstSlotShiftedBits = (byte) (firstSlotLeftShift >= 0 ? ((bits & 0xFF) << firstSlotLeftShift) : ((bits & 0xFF) >>> -firstSlotLeftShift));

        array[firstSlot] = (byte) ((array[firstSlot] & 0xFF) | firstSlotShiftedBits);

        if(byteOffset + size > 8) {
            int secondSlot = offset / 8 + 1;
            int remainingDigits = byteOffset + size - 8;
            int secondSlotLeftShift = 8 - remainingDigits;
            byte secondSlotShiftedBits = (byte) ((bits & 0xFF) << secondSlotLeftShift);

            array[secondSlot] = (byte) ((array[secondSlot] & 0xFF) | secondSlotShiftedBits);
        }
    }

    public static byte getBitsFromByteArray(byte[] array, int size, int offset) {
        byte out = 0;

        int byteOffset = offset % 8;
        int firstSlot = offset / 8;
        int firstSlotClearLeftShift = byteOffset;
        int firstSlotClearRightShift = 8 - size;
        byte firstSlotBits = (byte) ((array[firstSlot] & 0xFF) << firstSlotClearLeftShift);
        firstSlotBits = (byte) (firstSlotClearRightShift >= 0 ?
             (firstSlotBits & 0xFF) >>> firstSlotClearRightShift :
             (firstSlotBits & 0xFF) << -firstSlotClearRightShift); 

        if(byteOffset + size > 8) {
            int secondSlot = offset / 8 + 1;
            int secondSlotClearRightShift = 8 - (byteOffset + size - 8);
            byte secondSlotBits = (byte) ((array[secondSlot] & 0xFF) >>> secondSlotClearRightShift);

            out = (byte) ((firstSlotBits & 0xFF) | (secondSlotBits & 0xFF));
        } else {
            out = firstSlotBits;
        }

        return out;
    }

    public static String byteToBinaryString(byte b) {
        return Integer.toBinaryString(b & 255 | 256).substring(1);
    }

    public static String shortToBinaryString(short s) {
        return String.format("%16s", Integer.toBinaryString(s)).replace(' ', '0');
    }

    public static int byteDigitsNumber(byte b) {
        int digits = 0;

        do {
            b = (byte) ((b & 0xFF) >>> 1);
            ++digits;
        } while(b != 0);

        return digits;
    }


    public static int lowestPowerof2(int n) { 
        int res = 1;
        int currentRes = 1;

        while(currentRes <= n) {
            res = currentRes;
            currentRes <<= 1;
        }

        return res; 
    } 

    public static byte sizeToBitmask(int size) {
        switch(size) {
            case 1: return 0b1;
            case 2: return 0b11;
            case 3: return 0b111;
            case 4: return 0b1111;
            case 5: return 0b11111;
            case 6: return 0b111111;
            case 7: return (byte) 0b1111111;
            case 8: return (byte) 0b11111111;
            default: return 0b0;
        }
    }

    public static int log2(int i) {
        if(i == 0) return 0;
        return 31 - Integer.numberOfLeadingZeros(i);
    }
}