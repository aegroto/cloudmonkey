package com.aegroto.cloudmonkey.common;

public enum CloudMonkeyMessage {
    HELLO(1),
    NEXT_FRAME(2),
    NEXT_FRAME_HEADER(5),
    NEXT_FRAME_CHUNK(5);

    final public byte id;
    final public int size;

    CloudMonkeyMessage(int size) {
        id = (byte) ordinal();
        this.size = size;
    }
}