package com.aegroto.cloudmonkey.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class InputEntry {
    public static final byte
        TYPE_ENDBLOCK = 0,
        TYPE_TOUCH = 1,
        TYPE_MOUSE_MOTION = 2,
        TYPE_MOUSE_BUTTON = 3,
        TYPE_KEY = 4,
        TYPE_JOY_BUTTON = 5,
        TYPE_JOY_AXIS = 6;
}

