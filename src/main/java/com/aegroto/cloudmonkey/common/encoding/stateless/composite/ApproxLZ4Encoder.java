package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.ApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.Differential4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;
import com.jme3.util.BufferUtils;

public class ApproxLZ4Encoder implements Encoder {
    protected final int inputLength;

    protected final ApproximationEncoder approxEncoder;
    protected final LZ4Encoder lz4Encoder;

    protected ByteBuffer intermediateBuffer;

    public ApproxLZ4Encoder(int inputLength) {
        this.inputLength = inputLength;

        approxEncoder = new ApproximationEncoder(inputLength);
        lz4Encoder = new LZ4Encoder(inputLength);

        intermediateBuffer = BufferUtils.createByteBuffer(approxEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        approxEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = lz4Encoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        lz4Encoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        approxEncoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return lz4Encoder.outputBufferMinLength();
	}

}