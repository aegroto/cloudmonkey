package com.aegroto.cloudmonkey.common.encoding.stateless.downsampling;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

import lombok.Getter;

public class PixelDownsamplingEncoder implements Encoder {
    protected final int width, height;

    @Getter protected final int pixelsPerDsRow;

    public PixelDownsamplingEncoder(int width, int height) {
        this.width = width;
        this.height = height;
        this.pixelsPerDsRow = (2 + (width-2)/2);
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int[] inputArr = new int[input.remaining() / 4];
        input.asIntBuffer().get(inputArr);

        // Output first row (not downsampled)
        for(int c = 0; c < width; ++c) {
            output.putInt(inputArr[c]);
        }

        // Output middle rows
        for(int r = 1; r < height - 1; ++r) {
            output.putInt(inputArr[r * width]);

            for(int c = 1; c < width - 1; ++c) {
                if(c % 2 == r % 2) {
                    output.putInt(inputArr[r * width + c]);
                }
            }

            output.putInt(inputArr[r * width + width - 1]);
        }
    
        // Output last row (not downsampled)
        for(int c = 0; c < width; ++c) {
            output.putInt(inputArr[((height-1) * width + c)]);
        }

        input.position(input.limit());

        return (width * 2 + pixelsPerDsRow * (height - 2)) * 4;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        int[] inputArr = new int[input.remaining() / 4];
        input.asIntBuffer().get(inputArr);

        /*System.out.print("Input: ");
        for(int i = 0; i < inputArr.length; ++i) {
            System.out.printf("%s ", Integer.toBinaryString(inputArr[i]));
        }
        System.out.print("\n");*/

        // Output first row (not downsampled)
        for(int c = 0; c < width; ++c) {
            output.putInt(inputArr[c]);
        }

        // Output middle rows
        for(int r = 1; r < height - 1; ++r) {
            int prevRowBaseIndex = r > 1 ? width + (r-2) * pixelsPerDsRow : 0;
            int rowBaseIndex = width + (r-1) * pixelsPerDsRow;
            int nextRowBaseIndex = width + (r) * pixelsPerDsRow;

            output.putInt(inputArr[rowBaseIndex]);

            int last_row_index = r % 2;

            for(int c = 1; c < width - 1; ++c) {
                if(c % 2 == r % 2) {
                    output.putInt(interp(
                        inputArr[prevRowBaseIndex + c/2],
                        inputArr[rowBaseIndex + c/2 - 1],
                        inputArr[nextRowBaseIndex + c/2],
                        inputArr[rowBaseIndex + c/2]
                    ));
                } else {
                    output.putInt(inputArr[rowBaseIndex + last_row_index]);
                    ++last_row_index;
                }
            }

            output.putInt(inputArr[rowBaseIndex + pixelsPerDsRow - 1]);
        }
    
        // Output last row (not downsampled)
        for(int c = 0; c < width; ++c) {
            output.putInt(inputArr[(inputArr.length - width) + c]);
        }

        input.position(input.limit());
    }

    protected int interp(int i0, int i1, int i2, int i3) {
        int output = 0;

        byte[] p0 = ByteBuffer.allocate(4).putInt(i0).array();
        byte[] p1 = ByteBuffer.allocate(4).putInt(i1).array();
        byte[] p2 = ByteBuffer.allocate(4).putInt(i2).array();
        byte[] p3 = ByteBuffer.allocate(4).putInt(i3).array();

        byte[] out = new byte[4];

        for(int i = 0; i < 4; ++i) {
            out[i] = (byte) (((p0[i] & 0xFF) + (p1[i] & 0xFF) + (p2[i] & 0xFF) + (p3[i] & 0xFF)) / 4);
        }

        output = ByteBuffer.wrap(out).getInt();

        return output;
    }

    @Override
    public int outputBufferMinLength() {
		return (width*2 + height*pixelsPerDsRow + 1) * 4;
	}

}

