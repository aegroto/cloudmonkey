package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelizer.Channelizer4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.DownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.NativePixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.PixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.ApproxZstdEncoder;
import com.jme3.util.BufferUtils;

public class PixelDSApproxZstdEncoder implements Encoder {
    protected final int width, height; 

    protected final NativePixelDownsamplingEncoder pixelDSEncoder;
    protected final ApproxZstdEncoder zstdEncoder;

    protected ByteBuffer encodeIntermediateBuffer, decodeIntermediateBuffer;

    public PixelDSApproxZstdEncoder(int width, int height) {
        this.width = width;
        this.height = height;

        pixelDSEncoder = new NativePixelDownsamplingEncoder(width, height);
        zstdEncoder = new ApproxZstdEncoder(pixelDSEncoder.outputBufferMinLength());
        
        encodeIntermediateBuffer = BufferUtils.createByteBuffer(pixelDSEncoder.outputBufferMinLength());
        decodeIntermediateBuffer = BufferUtils.createByteBuffer(zstdEncoder.outputBufferMinLength());
    }
    
    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        pixelDSEncoder.encode(input, encodeIntermediateBuffer);
        encodeIntermediateBuffer.flip();

        int outputLength = zstdEncoder.encode(encodeIntermediateBuffer, output);
        encodeIntermediateBuffer.clear();

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        zstdEncoder.decode(input, decodeIntermediateBuffer);
        decodeIntermediateBuffer.flip();

        pixelDSEncoder.decode(decodeIntermediateBuffer, output);
        decodeIntermediateBuffer.clear();
    }

    @Override
    public int outputBufferMinLength() {
		return zstdEncoder.outputBufferMinLength();
	}

}