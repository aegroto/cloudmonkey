package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.ParallelPixelDifferentialEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.VariableRunLengthEncoder;
import com.jme3.util.BufferUtils;

public class ParDVarRunLengthEncoder implements Encoder {
    protected final int inputLength;

    protected final ParallelPixelDifferentialEncoder pdEncoder;
    protected final VariableRunLengthEncoder varrlEncoder;

    protected ByteBuffer intermediateBuffer;

    public ParDVarRunLengthEncoder(int inputLength) {
        this.inputLength = inputLength;

        pdEncoder = new ParallelPixelDifferentialEncoder(inputLength);
        varrlEncoder = new VariableRunLengthEncoder(inputLength);

        intermediateBuffer = BufferUtils.createByteBuffer(pdEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        pdEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = varrlEncoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        varrlEncoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        pdEncoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return varrlEncoder.outputBufferMinLength();
	}

}