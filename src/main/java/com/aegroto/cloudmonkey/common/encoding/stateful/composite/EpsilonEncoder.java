package com.aegroto.cloudmonkey.common.encoding.stateful.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.compare.Comparator;
import com.aegroto.cloudmonkey.common.encoding.compare.differential.DifferentialComparator;
import com.aegroto.cloudmonkey.common.encoding.compare.differential.NativeDifferentialComparator;
import com.aegroto.cloudmonkey.common.encoding.compare.differential.ParallelDifferentialComparator;
import com.aegroto.cloudmonkey.common.encoding.compare.nearsim.ParallelNearsimComparator;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.NativeApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelredundancy.RGBAChannelRedundancyEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.ApproxPixelDSEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.ApproxZstdEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.MultithreadLZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.MultithreadZstdDictEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.MultithreadZstdEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.PixelDSLZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.PixelDSZstdEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.PixelRepDSLZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.PixelRepDSZstdEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelRepPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.dummy.NullEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdDictEncoder;
import com.jme3.math.FastMath;

import org.lwjgl.BufferUtils;

public class EpsilonEncoder implements Encoder {
    protected final int inputLength, width, height;

    protected Encoder preEncoder, postKeyframeEncoder;
    protected Encoder[] postReliantEncoders;
    protected Comparator[] comparators; 

    protected int currentEncodeFrame, currentDecodeFrame;

    protected ByteBuffer preIntermediateBuffer, postIntermediateBuffer,
                         lastFrameBuffer;

    protected int outBufferMinLength;

    protected boolean forceKeyFrame;
    protected int lastPipeId = 0;

    protected long encodersThresholds[];

    public EpsilonEncoder(int width, int height) {
        this.width = width;
        this.height = height;
        this.inputLength = width * height * 4;

        // Pre encoder
        preEncoder = new NativeApproximationEncoder(inputLength);
        // preEncoder = new ApproxPixelDSEncoder(width, height);
        // preEncoder = new RGBAChannelRedundancyEncoder(inputLength);
        // preEncoder = new NullEncoder(inputLength);

        // Comparator
        comparators = new Comparator[2];
        // preEncoder out size should be fixed and equal to the original frame size

        comparators[0] = new NativeDifferentialComparator(preEncoder.outputBufferMinLength()); 
        comparators[1] = new ParallelNearsimComparator(width, height); 

        // Post keyframe encoder
        postKeyframeEncoder = new MultithreadZstdEncoder(preEncoder.outputBufferMinLength(), 8, 1);
        // postKeyframeEncoder = new MultithreadLZ4Encoder(preEncoder.outputBufferMinLength(), 8);
        // postKeyframeEncoder = new PixelRepDSLZ4Encoder(width, height);
        // postKeyframeEncoder = new PixelRepDSZstdEncoder(width, height);
        // postKeyframeEncoder = new NullEncoder(preEncoder.outputBufferMinLength());

        // Post reliant encoders
        postReliantEncoders = new Encoder[comparators.length];

        postReliantEncoders[0] = new MultithreadLZ4Encoder(comparators[0].outputBufferMinLength(), 8);
        // postReliantEncoders[1] = new MultithreadLZ4Encoder(comparators[1].outputBufferMinLength(), 8);
        postReliantEncoders[1] = new MultithreadZstdEncoder(comparators[1].outputBufferMinLength(), 8, 1);

        // Encoders thresholds
        encodersThresholds = new long[postReliantEncoders.length];

        encodersThresholds[0] = 3;
        encodersThresholds[1] = 10;

        // Buffers allocation

        lastFrameBuffer = BufferUtils.createByteBuffer(preEncoder.outputBufferMinLength());

        outBufferMinLength = 1 + Math.max(preEncoder.outputBufferMinLength(), postKeyframeEncoder.outputBufferMinLength());

        preIntermediateBuffer = BufferUtils.createByteBuffer(preEncoder.outputBufferMinLength());

        int maxComparatorMinOutLength = comparators[0].outputBufferMinLength();
        for(int i = 1; i < comparators.length; ++i) {
            if(comparators[i].outputBufferMinLength() > maxComparatorMinOutLength) {
                maxComparatorMinOutLength = comparators[i].outputBufferMinLength();
            } 
        }

        postIntermediateBuffer = BufferUtils.createByteBuffer(Math.max(maxComparatorMinOutLength,
                                    Math.max(postKeyframeEncoder.outputBufferMinLength(), inputLength)));

        forceKeyFrame = true;

        System.out.println("EpsilonEncoder preEncoder class: " + preEncoder.getClass().getSimpleName());
        System.out.println("EpsilonEncoder postKeyframeEncoder class: " + postKeyframeEncoder.getClass().getSimpleName());

        for(int i = 0; i < comparators.length; ++i) {
            System.out.println("EpsilonEncoder subpipe #" + i + " threshold: " + encodersThresholds[i]);
            System.out.println("EpsilonEncoder subpipe #" + i + " comparator class: " + comparators[i].getClass().getSimpleName());
            System.out.println("EpsilonEncoder subpipe #" + i + " postReliantEncoder class: " + postReliantEncoders[i].getClass().getSimpleName());
        }
    }

    protected byte selectPipeline(ByteBuffer currentFrame, ByteBuffer lastFrame) {
        currentFrame.mark();
        lastFrame.mark();

        long totalDiff = 0;

        long samples = 0;

        while(currentFrame.hasRemaining()) {
            totalDiff += Math.abs(currentFrame.get() - lastFrame.get());

            int jump = Math.min(currentFrame.remaining(), FastMath.nextRandomInt(20000, 100000));

            currentFrame.position(currentFrame.position() + jump);
            lastFrame.position(lastFrame.position() + jump);

            ++samples;
        }

        currentFrame.reset();
        lastFrame.reset();

        long meanDiff = totalDiff / samples;

        // System.out.println("Mean diff: " + meanDiff);

        for(int t = 0; t < encodersThresholds.length; ++t) {
            if(meanDiff < encodersThresholds[t]) {
                return (byte) t;
            }
        }
        
        return -1;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outLength;
        
        // Pre encoding
        long preStartTime = System.currentTimeMillis();

        preEncoder.encode(input, preIntermediateBuffer);

        preIntermediateBuffer.flip();

        long preEndTime = System.currentTimeMillis();

        // Pipe selection 
        long selectStartTime = System.currentTimeMillis();

        byte pipeId = selectPipeline(preIntermediateBuffer, lastFrameBuffer);
        if(lastPipeId != pipeId) {
            forceKeyFrame = true;
        }
        lastPipeId = pipeId;

        boolean keyFrame = forceKeyFrame ? true : pipeId == -1;

        if(forceKeyFrame)
            forceKeyFrame = false;

        output.put(keyFrame ? -1 : pipeId);

        long selectEndTime = System.currentTimeMillis();

        if(keyFrame) {
            // System.out.println("[Encoder] Key frame");

            long keyEncodeStartTime = System.currentTimeMillis();

            // Key frame
            outLength = postKeyframeEncoder.encode(preIntermediateBuffer, output);

            long keyEncodeEndTime = System.currentTimeMillis();

            currentEncodeFrame = 1;

            /*System.out.printf("Keyframe encoding time: %d %d %d\n",
                (preEndTime - preStartTime),
                (selectStartTime - selectStartTime),
                (keyEncodeEndTime - keyEncodeStartTime)
            );*/
        } else {
            // System.out.println("[Encoder] Reliant frame (" + pipeId + ")");

            // Reliant frame

            long postCompareStartTime = System.currentTimeMillis();
            comparators[pipeId].encode(lastFrameBuffer, preIntermediateBuffer, postIntermediateBuffer);
            long postCompareEndTime = System.currentTimeMillis();

            postIntermediateBuffer.flip();

            long postEncodeStartTime = System.currentTimeMillis();
            outLength = postReliantEncoders[pipeId].encode(postIntermediateBuffer, output);
            long postEncodeEndTime = System.currentTimeMillis();

            postIntermediateBuffer.clear();

            ++currentEncodeFrame;

            /*System.out.printf("Reliant encoding time (%d): %d %d %d %d\n", pipeId,
                (preEndTime - preStartTime),
                (selectStartTime - selectStartTime),
                (postCompareEndTime - postCompareStartTime),
                (postEncodeEndTime - postEncodeStartTime)
            );*/
        }

        preIntermediateBuffer.flip();
        lastFrameBuffer.clear();
        lastFrameBuffer.put(preIntermediateBuffer);
        lastFrameBuffer.flip();
        preIntermediateBuffer.clear();

        return outLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        byte pipeId = input.get();

        if(pipeId == -1) {
            // System.out.println("[Decoder] Key frame");

            // Key frame
            postKeyframeEncoder.decode(input, postIntermediateBuffer);
            postIntermediateBuffer.flip();
            
            preEncoder.decode(postIntermediateBuffer, output);

            currentDecodeFrame = 1;

            postIntermediateBuffer.flip();
            lastFrameBuffer.clear();
            lastFrameBuffer.put(postIntermediateBuffer);
            lastFrameBuffer.flip();
            postIntermediateBuffer.clear();
        } else {
            // System.out.println("[Decoder] Reliant frame");

            // Reliant frame
            postReliantEncoders[pipeId].decode(input, postIntermediateBuffer);
            postIntermediateBuffer.flip();

            // System.out.println(lastFrameBuffer.remaining() + " " + postIntermediateBuffer.remaining() + " " + preIntermediateBuffer.remaining());

            comparators[pipeId].decode(lastFrameBuffer, postIntermediateBuffer, preIntermediateBuffer);

            preIntermediateBuffer.flip();

            preEncoder.decode(preIntermediateBuffer, output);

            postIntermediateBuffer.clear();

            ++currentDecodeFrame;

            preIntermediateBuffer.flip();
            lastFrameBuffer.clear();
            lastFrameBuffer.put(preIntermediateBuffer);
            lastFrameBuffer.flip();
            preIntermediateBuffer.clear();
        }
    }

    @Override
    public int outputBufferMinLength() {
        return outBufferMinLength;
    }

    public void reset() {
        forceKeyFrame = true;
    }
}