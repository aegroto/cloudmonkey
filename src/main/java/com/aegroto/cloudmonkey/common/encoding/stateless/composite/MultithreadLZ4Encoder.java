package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;

import org.lwjgl.BufferUtils;

public class MultithreadLZ4Encoder implements Encoder {
    protected Encoder encoders[];
    protected ByteBuffer inputBuffers[];
    protected ByteBuffer outputBuffers[];

    protected List<Callable<Object>> encodeTasks = new LinkedList<>();

    protected ExecutorService executor;

    protected final int inputLength, nThreads;

    public MultithreadLZ4Encoder(int inputLength) {
        this(inputLength, 4);
    }

    public MultithreadLZ4Encoder(int inputLength, int nThreads) {
        this.inputLength = inputLength;
        this.nThreads = nThreads;

        executor = Executors.newFixedThreadPool(nThreads);

        inputBuffers = new ByteBuffer[nThreads];
        outputBuffers = new ByteBuffer[nThreads];

        encoders = new LZ4Encoder[nThreads];

        for (int t = 0; t < nThreads; ++t) {
            int encoderInputLength = inputLength / nThreads;

            if(t == 0 && inputLength % nThreads != 0) {
                encoderInputLength += inputLength % nThreads;
            }

            inputBuffers[t] = BufferUtils.createByteBuffer(encoderInputLength);
            encoders[t] = new LZ4Encoder(encoderInputLength);
            outputBuffers[t] = BufferUtils.createByteBuffer(encoders[t].outputBufferMinLength());
        }

        for (int t = 0; t < nThreads; ++t) {
            final Encoder encoder = encoders[t];
            final ByteBuffer inputBuffer = inputBuffers[t];
            final ByteBuffer outputBuffer = outputBuffers[t];
            final int _t = t;

            encodeTasks.add(() -> {
                // System.out.printf("Encoding task #%d\n", _t);

                int encodedLength = encoder.encode(inputBuffer, outputBuffer);
                inputBuffer.clear();

                return encodedLength;
            });
        }
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outLength = 0;

        for (int t = 0; t < nThreads; ++t) {
            input.limit(input.position() + inputBuffers[t].remaining());
            inputBuffers[t].put(input);
            inputBuffers[t].flip();
        }

        try {
            List<Future<Object>> results = executor.invokeAll(encodeTasks);

            for (int t = 0; t < nThreads; ++t) {
                Future<Object> result = results.get(t);

                int encodedLength = (int) result.get();

                outputBuffers[t].flip();
                output.putInt(encodedLength);
                output.put(outputBuffers[t]);
                outputBuffers[t].clear();

                outLength += 4 + encodedLength;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return outLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        for (int t = 0; t < nThreads; ++t) {
            input.limit(input.position() + 4);
            int encodedLength = input.getInt();

            input.limit(input.position() + encodedLength);

            encoders[t].decode(input, output);
        }
    }

    @Override
    public int outputBufferMinLength() {
        int outMinLength = nThreads * 4; 

        for (int t = 0; t < nThreads; ++t) {
            outMinLength += encoders[t].outputBufferMinLength();
        }

        return outMinLength;
    }

}