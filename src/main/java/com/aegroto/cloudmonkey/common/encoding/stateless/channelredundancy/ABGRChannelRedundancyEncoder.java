package com.aegroto.cloudmonkey.common.encoding.stateless.channelredundancy;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

public class ABGRChannelRedundancyEncoder implements Encoder {
    protected final int inputLength;

    public ABGRChannelRedundancyEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int inLength = input.remaining();

        // long meanDiff = 0;

        while(input.hasRemaining()) {
            byte a = input.get();
            byte b = input.get();
            byte g = input.get();
            byte r = input.get();

            byte mean = (byte) ((r + g + b) / 3);

            byte bDiff = (byte) (b - mean);
            byte gDiff = (byte) (g - mean);
            byte rDiff = (byte) (r - mean);

            // meanDiff += Math.abs(bDiff) + Math.abs(gDiff) + Math.abs(rDiff);

            output.put(mean);
            output.put(bDiff);
            output.put(gDiff);
            output.put(rDiff);
        }

        // System.out.println("Mean diff: " + meanDiff / (inLength - inLength/4));

        return inLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        while(input.hasRemaining()) {
            byte mean = input.get(); 
            byte bDiff = input.get();
            byte gDiff = input.get();
            byte rDiff = input.get();

            output.put((byte) -1);
            output.put((byte) (mean + bDiff));
            output.put((byte) (mean + gDiff));
            output.put((byte) (mean + rDiff));
        }
    }

    @Override
    public int outputBufferMinLength() {
		return inputLength;
	}

}