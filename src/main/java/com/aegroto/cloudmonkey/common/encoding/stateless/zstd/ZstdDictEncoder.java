package com.aegroto.cloudmonkey.common.encoding.stateless.zstd;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.github.luben.zstd.Zstd;
import com.jme3.system.NativeLibraryLoader;
import com.jme3.system.Platform;

public class ZstdDictEncoder implements Encoder {
    static {
        NativeLibraryLoader.registerNativeLibrary("NativeClasses", Platform.Linux64, "nativeclasses.so");        
        
        NativeLibraryLoader.loadNativeLibrary("NativeClasses", true);
    }

    protected final int inputLength, compressionLevel;

    protected String dictionary;

    protected long contextAddress;

    public ZstdDictEncoder(int inputLength) {
        this(inputLength, "classic", 1);
    }

    public ZstdDictEncoder(int inputLength, String dictionary, int compressionLevel) {
        this.inputLength = inputLength;
        this.compressionLevel = compressionLevel;
        this.dictionary = dictionary;

        File zstdDictFile = new File("src/main/resources/zstd dicts/" + dictionary);

        try {
            byte[] dictData = Files.readAllBytes(zstdDictFile.toPath());

            contextAddress = initNative(compressionLevel, dictData);
        } catch (IOException e) {
            contextAddress = 0;
            e.printStackTrace();
        }
    }

    protected native long initNative(int compressionLevel, byte[] dictBuffer);
    protected native int encodeNative(long ctxAddress, ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int inputLength, int outCapacity); 
    protected native int decodeNative(long ctxAddress, ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int inputLength, int outCapacity);

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        long outval = encodeNative(contextAddress, input, output, input.position(), output.position(), input.remaining(), output.remaining());

        if(Zstd.isError(outval)) {
            System.err.println("Zstd dict compression error (" + Zstd.getErrorName(outval) + ")");
            return 0;
        } else {
            output.position(output.position() + (int) outval);
            input.position(input.limit());
        }

        return (int) outval;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        long outval = decodeNative(contextAddress, input, output, input.position(), output.position(), input.remaining(), output.remaining());

        if(Zstd.isError(outval)) {
            System.err.println("Zstd decompression error (" + Zstd.getErrorName(outval) + ")");
        } else {
            output.position(output.position() + (int) outval);
            input.position(input.limit());
        }
    }

    @Override
    public int outputBufferMinLength() {
		return (int) Zstd.compressBound(inputLength);
	}

}


