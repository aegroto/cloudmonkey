package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.Differential4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder;
import com.jme3.util.BufferUtils;

public class D4HuffmanEncoder implements Encoder {
    protected final int inputLength;

    protected final Differential4Encoder d4Encoder;
    protected final HuffmanEncoder huffmanEncoder;

    protected ByteBuffer intermediateBuffer;

    public D4HuffmanEncoder(int inputLength) {
        this.inputLength = inputLength;

        d4Encoder = new Differential4Encoder(inputLength);
        huffmanEncoder = new HuffmanEncoder(inputLength);

        intermediateBuffer = BufferUtils.createByteBuffer(d4Encoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        d4Encoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = huffmanEncoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        huffmanEncoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        d4Encoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return huffmanEncoder.outputBufferMinLength();
	}

}