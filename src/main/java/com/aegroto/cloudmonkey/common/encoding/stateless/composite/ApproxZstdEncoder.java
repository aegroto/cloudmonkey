package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.NativeApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder;
import com.jme3.util.BufferUtils;

public class ApproxZstdEncoder implements Encoder {
    protected final int inputLength;

    protected final NativeApproximationEncoder approxEncoder;
    protected final ZstdEncoder zstdEncoder;

    protected ByteBuffer intermediateBuffer;

    public ApproxZstdEncoder(int inputLength) {
        this.inputLength = inputLength;

        approxEncoder = new NativeApproximationEncoder(inputLength);
        zstdEncoder = new ZstdEncoder(inputLength);

        intermediateBuffer = BufferUtils.createByteBuffer(approxEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        approxEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = zstdEncoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        zstdEncoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        approxEncoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return zstdEncoder.outputBufferMinLength();
	}

}