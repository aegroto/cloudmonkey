package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.Differential4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder;
import com.jme3.util.BufferUtils;

public class BD4HuffmanEncoder implements Encoder {
    protected final int inputLength;

    protected final BlockD4Encoder bd4Encoder;
    protected final HuffmanEncoder huffmanEncoder;

    protected ByteBuffer intermediateBuffer;

    public BD4HuffmanEncoder(int inputLength) {
        this.inputLength = inputLength;

        bd4Encoder = new BlockD4Encoder(inputLength);
        huffmanEncoder = new HuffmanEncoder(inputLength);

        intermediateBuffer = BufferUtils.createByteBuffer(bd4Encoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        bd4Encoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = huffmanEncoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        huffmanEncoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        bd4Encoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return huffmanEncoder.outputBufferMinLength();
	}

}