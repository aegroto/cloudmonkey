package com.aegroto.cloudmonkey.common.encoding.stateless.approximation;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.jme3.system.NativeLibraryLoader;
import com.jme3.system.Platform;

import lombok.Getter;

public class NativeApproximationEncoder implements Encoder {
    static {
        NativeLibraryLoader.registerNativeLibrary("NativeClasses", Platform.Linux64, "nativeclasses.so");        
        
        NativeLibraryLoader.loadNativeLibrary("NativeClasses", true);
    }

    protected final int inputLength;

    public NativeApproximationEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    public native int encodeNative(ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int inputLength); 
    public native void decodeNative(ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int encodedInputLength);

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outLength = encodeNative(input, output, input.position(), output.position(), inputLength);

        input.position(input.limit());
        output.position(output.position() + outLength);

        return outLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        // bulk method, should be never called out of testing
        decodeNative(input, output, input.position(), output.position(), input.remaining());

        input.position(input.limit());
        output.position(output.position() + inputLength);
    }

    @Override
    public int outputBufferMinLength() {
		return inputLength;
	}

}

