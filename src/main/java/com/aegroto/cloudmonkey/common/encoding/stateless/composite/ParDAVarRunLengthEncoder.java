package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.ApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.ParallelPixelDifferentialEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.VariableRunLengthEncoder;
import com.jme3.util.BufferUtils;

public class ParDAVarRunLengthEncoder implements Encoder {
    protected final int inputLength;

    protected final ApproximationEncoder approxEncoder;
    protected final ParallelPixelDifferentialEncoder pdEncoder;
    protected final VariableRunLengthEncoder varrlEncoder;

    protected ByteBuffer intermediateBuffer0, intermediateBuffer1;

    public ParDAVarRunLengthEncoder(int inputLength) {
        this.inputLength = inputLength;

        approxEncoder = new ApproximationEncoder(inputLength);
        pdEncoder = new ParallelPixelDifferentialEncoder(inputLength);
        varrlEncoder = new VariableRunLengthEncoder(inputLength);

        intermediateBuffer0 = BufferUtils.createByteBuffer(approxEncoder.outputBufferMinLength());
        intermediateBuffer1 = BufferUtils.createByteBuffer(pdEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        approxEncoder.encode(input, intermediateBuffer0);
        intermediateBuffer0.flip();

        pdEncoder.encode(intermediateBuffer0, intermediateBuffer1);
        intermediateBuffer1.flip();

        int outputLength = varrlEncoder.encode(intermediateBuffer1, output);

        intermediateBuffer0.rewind();
        intermediateBuffer1.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        varrlEncoder.decode(input, intermediateBuffer1);
        intermediateBuffer1.flip();
        pdEncoder.decode(intermediateBuffer1, output);

        intermediateBuffer1.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return varrlEncoder.outputBufferMinLength();
	}

}