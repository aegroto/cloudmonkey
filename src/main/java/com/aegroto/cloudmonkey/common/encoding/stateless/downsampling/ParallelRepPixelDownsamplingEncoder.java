package com.aegroto.cloudmonkey.common.encoding.stateless.downsampling;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.opencl.OpenCLManager;
import com.jme3.opencl.Buffer;
import com.jme3.opencl.CommandQueue;
import com.jme3.opencl.Event;
import com.jme3.opencl.Kernel;
import com.jme3.opencl.Program;
import com.jme3.opencl.Kernel.WorkSize;

import lombok.Getter;

public class ParallelRepPixelDownsamplingEncoder implements Encoder {
    protected final int width, height;

    protected final int inputLength, outputLength;

    protected final int valuesPerDsRow;
    @Getter protected final int extraDataLength;

    protected Kernel encodeKernel, decodeKernel;
    protected Buffer encodeInputBuffer, encodeOutputBuffer;
    protected Buffer decodeInputBuffer, decodeOutputBuffer;

    protected WorkSize encodeLocalWorkSize;
    protected WorkSize encodeGlobalWorkSize;

    protected WorkSize decodeLocalWorkSize;
    protected WorkSize decodeGlobalWorkSize;

    protected final String programFile = "cl_src/rep_downsampling.ocl", encodeKernelName = "rep_downsampling_encode", decodeKernelName = "rep_downsampling_decode";

    public ParallelRepPixelDownsamplingEncoder(int width, int height) {
        this.width = width;
        this.height = height;

        this.inputLength = width * height * 4;

        this.valuesPerDsRow = (2 + (width-2)/2);

        this.extraDataLength = valuesPerDsRow * (height - 2);
        this.outputLength = (width * 2 + valuesPerDsRow * (height - 2)) * 4 + extraDataLength;

        Program program = OpenCLManager.loadProgram(programFile);
        program.build();

        encodeKernel = program.createKernel(encodeKernelName);
        decodeKernel = program.createKernel(decodeKernelName);

        encodeInputBuffer = OpenCLManager.getContext().createBuffer(inputLength);
        encodeOutputBuffer = OpenCLManager.getContext().createBuffer(outputBufferMinLength());

        decodeInputBuffer = OpenCLManager.getContext().createBuffer(outputBufferMinLength());
        decodeOutputBuffer = OpenCLManager.getContext().createBuffer(inputLength);

        int encodePreferredLwsX = 32;
        int encodeLwsX = encodePreferredLwsX > width ? width : encodePreferredLwsX;

        int encodePreferredLwsY = 32;
        int encodeLwsY = encodePreferredLwsY > height ? height  : encodePreferredLwsY;

        encodeLocalWorkSize = new WorkSize(encodeLwsX, encodeLwsY);
        encodeGlobalWorkSize = new WorkSize(bestGlobalWorksize(width, encodeLwsX), bestGlobalWorksize(height, encodeLwsY));

        int decodePreferredLwsX = 32;
        int decodeLwsX = decodePreferredLwsX > width ? width : decodePreferredLwsX;

        int decodePreferredLwsY = 32;
        int decodeLwsY = decodePreferredLwsY > height ? height : decodePreferredLwsY;

        decodeLocalWorkSize = new WorkSize(decodeLwsX, decodeLwsY);
        decodeGlobalWorkSize = new WorkSize(bestGlobalWorksize(width, decodeLwsX), bestGlobalWorksize(height, decodeLwsY));

        // System.out.println(encodeLocalWorkSize + " " + encodeGlobalWorkSize);
        // System.out.println(decodeLocalWorkSize + " " + decodeGlobalWorkSize);
    }

    protected int multUp(int a, int b) {
        return (a + b - 1) / b;
    }

    protected int bestGlobalWorksize(int length, int lws) {
        return ((length + lws - 1) / lws) * lws;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        CommandQueue queue = OpenCLManager.getCmdQueue(); 
        encodeInputBuffer.write(queue, input);

        Event encodeEvent = encodeKernel.Run2(queue,
            encodeGlobalWorkSize, 
            encodeLocalWorkSize,
            encodeInputBuffer, 
            encodeOutputBuffer,
            width,
            height
        );
        encodeEvent.waitForFinished();

        encodeOutputBuffer.read(queue, output, outputLength);

        input.position(input.limit());
        output.position(output.position() + outputLength);

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        CommandQueue queue = OpenCLManager.getCmdQueue(); 

        decodeInputBuffer.write(queue, input);

        Event decodeEvent = decodeKernel.Run2(queue,
            decodeGlobalWorkSize, 
            decodeLocalWorkSize,
            decodeInputBuffer, 
            decodeOutputBuffer,
            // new Kernel.LocalMem((int) (decodeLocalWorkSize.getSizes()[0] * 4)),
            width,
            height
        );
        decodeEvent.waitForFinished();

        decodeOutputBuffer.read(queue, output, inputLength);

        input.position(input.limit());
        output.position(output.position() + inputLength);
    }

    @Override
    public int outputBufferMinLength() {
		return outputLength;
	}
}