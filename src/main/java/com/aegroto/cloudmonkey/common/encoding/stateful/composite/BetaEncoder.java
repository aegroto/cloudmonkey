package com.aegroto.cloudmonkey.common.encoding.stateful.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.compare.Comparator;
import com.aegroto.cloudmonkey.common.encoding.compare.differential.DifferentialComparator;
import com.aegroto.cloudmonkey.common.encoding.compare.differential.ParallelDifferentialComparator;
import com.aegroto.cloudmonkey.common.encoding.compare.dummy.NullComparator;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.ApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.NativeApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.ApproxPixelDSEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4RunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4SelRLVHEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.RepPixelDSChRedEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.NativePixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.dummy.NullEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdDictEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder;

import org.lwjgl.BufferUtils;

public class BetaEncoder implements Encoder {
    protected final int inputLength, width, height;

    protected Encoder preEncoder, postKeyframeEncoder, postReliantEncoder;
    protected Comparator comparator; 

    protected final int lineLength = 4;
    protected int currentEncodeFrame, currentDecodeFrame;

    protected ByteBuffer preIntermediateBuffer, postIntermediateBuffer,
                         lastFrameBuffer;

    protected int outBufferMinLength;

    public BetaEncoder(int width, int height) {
        this.width = width;
        this.height = height;
        this.inputLength = width * height * 4;

        preEncoder = new ApproxPixelDSEncoder(width, height);
        // preEncoder = new ChannelRedundancyEncoder(inputLength);
        comparator = new ParallelDifferentialComparator(preEncoder.outputBufferMinLength()); // preEncoder out size should be fixed

        postKeyframeEncoder = new ZstdEncoder(comparator.outputBufferMinLength());
        postReliantEncoder = new ZstdDictEncoder(comparator.outputBufferMinLength(), "diff", 1);

        lastFrameBuffer = BufferUtils.createByteBuffer(preEncoder.outputBufferMinLength());

        outBufferMinLength = Math.max(preEncoder.outputBufferMinLength(), postKeyframeEncoder.outputBufferMinLength());

        preIntermediateBuffer = BufferUtils.createByteBuffer(preEncoder.outputBufferMinLength());
        postIntermediateBuffer = BufferUtils.createByteBuffer(postKeyframeEncoder.outputBufferMinLength());

        currentEncodeFrame = lineLength;
        currentDecodeFrame = lineLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outLength;

        if(currentEncodeFrame >= lineLength) {
            // System.out.println("[Encoder] Key frame");

            // Key frame
            preEncoder.encode(input, preIntermediateBuffer);
            preIntermediateBuffer.flip();

            outLength = postKeyframeEncoder.encode(preIntermediateBuffer, output);

            lastFrameBuffer.clear();

            currentEncodeFrame = 1;
        } else {
            // System.out.println("[Encoder] Reliant frame");

            // Reliant frame
            preEncoder.encode(input, preIntermediateBuffer);
            preIntermediateBuffer.flip();

            comparator.encode(lastFrameBuffer, preIntermediateBuffer, postIntermediateBuffer);
            postIntermediateBuffer.flip();

            outLength = postReliantEncoder.encode(postIntermediateBuffer, output);

            postIntermediateBuffer.clear();
            lastFrameBuffer.clear();

            ++currentEncodeFrame;
        }

        preIntermediateBuffer.flip();
        lastFrameBuffer.put(preIntermediateBuffer);
        lastFrameBuffer.flip();
        preIntermediateBuffer.clear();

        return outLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        if(currentDecodeFrame >= lineLength) {
            // System.out.println("[Decoder] Key frame");

            // Key frame
            postKeyframeEncoder.decode(input, postIntermediateBuffer);
            postIntermediateBuffer.flip();
            
            preEncoder.decode(postIntermediateBuffer, output);

            lastFrameBuffer.clear();

            currentDecodeFrame = 1;

            postIntermediateBuffer.flip();
            lastFrameBuffer.put(postIntermediateBuffer);
            lastFrameBuffer.flip();
            postIntermediateBuffer.clear();
        } else {
            // System.out.println("[Decoder] Reliant frame");

            // Reliant frame
            postReliantEncoder.decode(input, postIntermediateBuffer);
            postIntermediateBuffer.flip();

            // System.out.println(lastFrameBuffer.remaining() + " " + postIntermediateBuffer.remaining() + " " + preIntermediateBuffer.remaining());

            comparator.decode(lastFrameBuffer, postIntermediateBuffer, preIntermediateBuffer);
            preIntermediateBuffer.flip();

            preEncoder.decode(preIntermediateBuffer, output);

            postIntermediateBuffer.clear();
            lastFrameBuffer.clear();

            ++currentDecodeFrame;

            preIntermediateBuffer.flip();
            lastFrameBuffer.put(preIntermediateBuffer);
            lastFrameBuffer.flip();
            preIntermediateBuffer.clear();
        }
    }

    @Override
    public int outputBufferMinLength() {
        return outBufferMinLength;
    }

    public void reset() {
        currentEncodeFrame = lineLength;
        currentDecodeFrame = lineLength;
    }
}