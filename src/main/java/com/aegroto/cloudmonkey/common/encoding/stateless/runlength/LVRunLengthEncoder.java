package com.aegroto.cloudmonkey.common.encoding.stateless.runlength;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.Helpers;
import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.jme3.math.FastMath;

/* Less Variable RunLength Encoder */

public class LVRunLengthEncoder implements Encoder {
    protected final int inputLength;

    public LVRunLengthEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        // System.out.println("Input's remaining: " + input.remaining());

        byte currentValue = input.get();
        int currentRunLength = 1;
        int maxRunLength = 1;

        while(input.hasRemaining()) {
            byte nextValue = input.get();

            if(currentValue == nextValue) {
               ++currentRunLength; 
            } else {
                currentValue = nextValue;

                if(currentRunLength > maxRunLength) {
                    maxRunLength = currentRunLength;
                }

                currentRunLength = 1;
            }
        } 
        input.flip();

        if(currentRunLength > maxRunLength) {
            maxRunLength = currentRunLength;
        }

        int bitsPerLength = -1;

        if(maxRunLength > 1) {
            bitsPerLength = Math.min(1 + Helpers.log2(maxRunLength) + (FastMath.isPowerOfTwo(maxRunLength) ? 0 : 1), 24);
        } else {
            bitsPerLength = 1;
        }

        // System.out.println("Max run length: " + maxRunLength);
        // System.out.println("Bits per length: " + bitsPerLength);

        return encode(input, output, bitsPerLength);
    }

    public int encode(ByteBuffer input, ByteBuffer output, int bitsPerLength) {
        // Output writing
        int outputLength = 0;
        int outputOffset = output.position();

        byte value = input.get(), currentValue = value;
        long occurrences = 1;

        long maxOccurrences = (1L << bitsPerLength) - 1;

        output.position(outputOffset + 8);

        long chunk = 0;
        int remainingChunkBits = 32;

        while(input.hasRemaining()) {
            currentValue = input.get();

            if(value == currentValue) {
                if(occurrences >= maxOccurrences) {
                    chunk = chunk | ((((long) (value)) << (64 - 8)) >>> (32 - remainingChunkBits));

                    // System.out.printf("Encoded %d, current chunk: %s\n", value, Long.toBinaryString(chunk));

                    remainingChunkBits -= 8;
                    if(remainingChunkBits <= 0) {
                        output.putInt((int) ((chunk >>> 32) & 0xFFFFFFFFL));
                        chunk = chunk << 32;
                        remainingChunkBits = 32 + remainingChunkBits;
                    }

                    chunk = chunk | ((((occurrences) << (64 - bitsPerLength))) >>> (32 - remainingChunkBits));

                    // System.out.printf("Encoded length %d, current chunk: %s\n", occurrences, Long.toBinaryString(chunk));

                    remainingChunkBits -= bitsPerLength;
                    if(remainingChunkBits <= 0) {
                        output.putInt((int) ((chunk >>> 32) & 0xFFFFFFFFL));
                        chunk = chunk << 32;
                        remainingChunkBits = 32 + remainingChunkBits;
                    }

                    outputLength += 8 + bitsPerLength;

                    occurrences = 1;
                } else {
                    ++occurrences;
                }
            } else {
                chunk = chunk | ((((long) (value)) << (64 - 8)) >>> (32 - remainingChunkBits));

                // System.out.printf("Encoded %d, current chunk: %s\n", value, Long.toBinaryString(chunk));

                remainingChunkBits -= 8;
                if(remainingChunkBits <= 0) {
                    //System.out.println("Pushing chunk...");

                    output.putInt((int) ((chunk >>> 32) & 0xFFFFFFFFL));
                    chunk = chunk << 32;
                    remainingChunkBits = 32 + remainingChunkBits;
                }

                chunk = chunk | (((((long) occurrences) << (64 - bitsPerLength))) >>> (32 - remainingChunkBits));

                // System.out.printf("Encoded length %d, current chunk: %s\n", occurrences, Long.toBinaryString(chunk));

                remainingChunkBits -= bitsPerLength;
                if(remainingChunkBits <= 0) {
                    //System.out.println("Pushing chunk...");

                    output.putInt((int) ((chunk >>> 32) & 0xFFFFFFFFL));
                    chunk = chunk << 32;
                    remainingChunkBits = 32 + remainingChunkBits;
                }

                outputLength += 8 + bitsPerLength;

                value = currentValue;

                occurrences = 1;
            }
        }

        //System.out.println("Output length: " + outputLength);

        chunk = chunk | ((((long) (value)) << (64 - 8)) >>> (32 - remainingChunkBits));
        remainingChunkBits -= 8;

        // System.out.printf("Encoded last value %d, current chunk: %s\n", value, Long.toBinaryString(chunk));

        if(remainingChunkBits <= 0) {
            //System.out.println("Pushing chunk...");
            output.putInt((int) ((chunk >>> 32) & 0xFFFFFFFFL));
            chunk = chunk << 32;
            remainingChunkBits = 32 + remainingChunkBits;
        }

        long encodedOccurrences = occurrences;
        encodedOccurrences = encodedOccurrences << (64 - bitsPerLength); 
        encodedOccurrences = encodedOccurrences >>> (32 - remainingChunkBits);

        chunk = chunk | encodedOccurrences;
        remainingChunkBits -= bitsPerLength;

        // System.out.printf("Encoded last length %d, current chunk: %s\n", occurrences, Long.toBinaryString(chunk));

        if(remainingChunkBits <= 0) {
            //System.out.println("Pushing chunk...");
            output.putInt((int) ((chunk >>> 32) & 0xFFFFFFFFL));
            chunk = chunk << 32;
            remainingChunkBits = 32 + remainingChunkBits;
        }

        outputLength += 8 + bitsPerLength;
        
        //System.out.println("Remaining chunk bits: " + remainingChunkBits);

        while(remainingChunkBits < 32) {
            //System.out.println("Pushing last chunk...");
            output.put((byte) ((chunk >>> (64 - 8)) & 0xFF));
            chunk <<= 8;
            remainingChunkBits += 8;
        }

        output.putInt(outputOffset, outputLength);
        output.putInt(outputOffset + 4, bitsPerLength);

        /*System.out.println("Encoded length: " + outputLength);
        System.out.println("Encoded bits per length: " + bitsPerLength);

        System.out.print("Encoded output: ");
        int pos = output.position();
        output.position(0);
        while(output.hasRemaining()) {
            System.out.print(output.get() + " ");
        }
        System.out.print("\n");
        output.position(pos);*/

        return 8 + ((outputLength + 8 - 1) / 8);
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        
        /*System.out.print("Encoded input: ");
        int pos = input.position();
        input.position(0);
        while(input.hasRemaining()) {
            System.out.print(input.get() + " ");
        }
        System.out.print("\n");
        input.position(pos);*/

        int inputBitsLength = input.getInt();
        int bitsPerLength = input.getInt();
        int decodedBits = 0;

        // System.out.println("Input bits length: " + inputBitsLength);
        // System.out.println("Bits per length: " + bitsPerLength);
        
        long chunk = 0;
        int loadedBits = 0;

        while(decodedBits < inputBitsLength) {
            // //System.out.println("Current chunk: " + Long.toBinaryString(chunk));

            if(loadedBits < 8 + bitsPerLength) {
                long bitsToBeLoaded = 0;

                try {
                    bitsToBeLoaded = (long) (input.getInt());
                    bitsToBeLoaded = bitsToBeLoaded & 0xFFFFFFFFL;
                    bitsToBeLoaded <<= (32 - loadedBits);

                    chunk = chunk | bitsToBeLoaded;
                    loadedBits += 32;
                } catch(BufferUnderflowException e) {
                    while(input.hasRemaining()) { 
                        bitsToBeLoaded = input.get() & 0xFF;
                        bitsToBeLoaded <<= (56 - loadedBits);

                        chunk = chunk | bitsToBeLoaded;
                        loadedBits += 8;
                    }
                }

            }

            //System.out.println("Loaded chunk:  " + Long.toBinaryString(chunk));

            // byte value = input.get();
            // int occurrences = input.getInt();

            byte value = (byte) ((chunk >>> 56) & 0xFF);
            int occurrences = (int) ((chunk << 8) >>> (64 - bitsPerLength));
            chunk <<= 8 + bitsPerLength;
            loadedBits -= 8 + bitsPerLength;

            for(int i = 0; i < occurrences; ++i) {
                output.put(value);
            }

            decodedBits += 8 + bitsPerLength;
            // System.out.println("Decoded bits: " + decodedBits);
        }
    }

    @Override
    public int outputBufferMinLength() {
		return 8 + inputLength + inputLength * 8 + 4;
	}

}