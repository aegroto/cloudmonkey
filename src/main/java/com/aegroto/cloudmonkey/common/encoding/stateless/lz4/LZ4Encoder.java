package com.aegroto.cloudmonkey.common.encoding.stateless.lz4;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.jme3.system.NativeLibraryLoader;
import com.jme3.system.Platform;

public class LZ4Encoder implements Encoder {
    static {
        NativeLibraryLoader.registerNativeLibrary("NativeClasses", Platform.Linux64, "nativeclasses.so");        
        
        NativeLibraryLoader.loadNativeLibrary("NativeClasses", true);
    }

    protected final int inputLength;

    public LZ4Encoder(int inputLength) {
        this.inputLength = inputLength;
    }

    protected native int encodeNative(ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int inputLength, int outCapacity); 
    protected native int decodeNative(ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int inputLength, int outCapacity);
    protected native int nativeOutputBufferMinLength(int inputLength);

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outval = encodeNative(input, output, input.position(), output.position(), input.remaining(), output.remaining());

        output.position(output.position() + outval);
        input.position(input.limit());

        return outval;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        int outval = decodeNative(input, output, input.position(), output.position(), input.remaining(), output.remaining());

        output.position(output.position() + outval);
        input.position(input.limit());
    }

    @Override
    public int outputBufferMinLength() {
		return nativeOutputBufferMinLength(inputLength);
	}

}

