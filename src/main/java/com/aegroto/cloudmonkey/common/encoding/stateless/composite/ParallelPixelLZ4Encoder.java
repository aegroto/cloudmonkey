package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.ParallelPixelDifferentialEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;
import com.jme3.util.BufferUtils;

public class ParallelPixelLZ4Encoder implements Encoder {
    protected final int inputLength;

    protected final Encoder differentialEncoder;
    protected final Encoder lz4Encoder;

    protected ByteBuffer intermediateBuffer;

    public ParallelPixelLZ4Encoder(int inputLength) {
        this.inputLength = inputLength;

        differentialEncoder = new ParallelPixelDifferentialEncoder(inputLength);
        lz4Encoder = new MultithreadLZ4Encoder(inputLength, 8);

        intermediateBuffer = BufferUtils.createByteBuffer(differentialEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        differentialEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = lz4Encoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        lz4Encoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        differentialEncoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return lz4Encoder.outputBufferMinLength();
	}

}