package com.aegroto.cloudmonkey.common.encoding.compare.differential;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.compare.Comparator;

public class DifferentialComparator implements Comparator {
    protected final int inputLength;

    public DifferentialComparator(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output) {
        int i = 0;

        while(firstInput.hasRemaining() || secondInput.hasRemaining()) {
            byte b0 = secondInput.get();
            byte b1 = firstInput.get();
            byte out = (byte) (b0 - b1);

            // System.out.println("[Encode " + i + "] " + b0 + " - " + b1 + " = " + out);

            output.put(out);
            ++i;
        }

        return inputLength;
    }

    @Override
    public void decode(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output) {
        int i = 0;

        while(firstInput.hasRemaining() || secondInput.hasRemaining()) {
            byte b0 = firstInput.get();
            byte b1 = secondInput.get();
            byte out = (byte) (b0 + (b1 & 0xFF));

            // System.out.println("[Decode " + i + "] " + b0 + " + " + b1 + " = " + out);

            output.put(out);
            ++i;
        }
    }

    @Override
    public int outputBufferMinLength() {
        return inputLength;
    }
}