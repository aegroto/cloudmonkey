package com.aegroto.cloudmonkey.common.encoding.stateless.zstd;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.github.luben.zstd.Zstd;
import com.github.luben.zstd.ZstdDictCompress;
import com.github.luben.zstd.ZstdDictDecompress;
import com.github.luben.zstd.ZstdDictTrainer;
import com.jme3.system.NativeLibraryLoader;
import com.jme3.system.Platform;

public class ZstdEncoder implements Encoder {
    static {
        NativeLibraryLoader.registerNativeLibrary("NativeClasses", Platform.Linux64, "nativeclasses.so");        
        
        NativeLibraryLoader.loadNativeLibrary("NativeClasses", true);
    }

    protected final int inputLength, compressionLevel;

    protected final long contextAddress;

    public ZstdEncoder(int inputLength) {
        this(inputLength, 1);
    }

    public ZstdEncoder(int inputLength, int compressionLevel) {
        this.inputLength = inputLength;
        this.compressionLevel = compressionLevel;

        contextAddress = initNative(compressionLevel);
    }

    protected native long initNative(int compressionLevel);
    protected native int encodeNative(long ctxAddress, ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int inputLength, int outCapacity); 
    protected native int decodeNative(long ctxAddress, ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int inputLength, int outCapacity);

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        long outval = encodeNative(contextAddress, input, output, input.position(), output.position(), input.remaining(), output.remaining());

        if(Zstd.isError(outval)) {
            System.err.println("Zstd compression error (" + Zstd.getErrorName(outval) + ")");
            return 0;
        } else {
            output.position(output.position() + (int) outval);
            input.position(input.limit());
        }

        return (int) outval;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        long outval = decodeNative(contextAddress, input, output, input.position(), output.position(), input.remaining(), output.remaining());

        if(Zstd.isError(outval)) {
            System.err.println("Zstd decompression error (" + Zstd.getErrorName(outval) + ")");
        } else {
            output.position(output.position() + (int) outval);
            input.position(input.limit());
        }
    }

    @Override
    public int outputBufferMinLength() {
		return (int) Zstd.compressBound(inputLength);
	}

}


