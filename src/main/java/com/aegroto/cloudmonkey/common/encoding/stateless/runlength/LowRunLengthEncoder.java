package com.aegroto.cloudmonkey.common.encoding.stateless.runlength;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

public class LowRunLengthEncoder implements Encoder {
    protected final int inputLength;

    public LowRunLengthEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outputLength = 0;
        int outputOffset = output.position();

        byte value = input.get(), currentValue = value;
        short occurrences = 1;

        output.position(outputOffset + 4);

        while(input.hasRemaining()) {
            currentValue = input.get();

            if(value == currentValue) {
                ++occurrences;
            } else {

                output.put(value);
                output.putShort(occurrences);
                outputLength += 3;

                value = currentValue;

                occurrences = 1;
            }
        }

        output.put(value);
        output.putShort(occurrences);
        outputLength += 3;

        output.putInt(outputOffset, outputLength);

        /*while(input.hasRemaining()) {
            occurrences = 0;

            do {
                ++occurrences;

                if(input.hasRemaining()) {
                    currentValue = input.get();
                } else break;
            } while(value == currentValue);
            
            output.put(value);
            output.putInt(occurrences);

            outputLength += 5;

            value = currentValue;
        }*/

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        int inputBytesLength = input.getInt();
        int decodedBytes = 0;

        while(decodedBytes < inputBytesLength) {
            byte value = input.get();
            short occurrences = input.getShort();

            for(int i = 0; i < occurrences; ++i)
                output.put(value);

            decodedBytes += 3;
        }
    }

    @Override
    public int outputBufferMinLength() {
		return 4 + inputLength + inputLength * 2;
	}

}