package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.VariableRunLengthEncoder;

public class SelectiveRLVHEncoder implements Encoder {
    public static final double RLVH_CONST = 0.15;

    public static final byte RL_ID = 0;
    public static final byte HUFF_ID = 1;

    protected final int inputLength; 

    protected HuffmanEncoder huffEncoder;
    protected VariableRunLengthEncoder varrlEncoder;

    public SelectiveRLVHEncoder(int inputLength) {
        this.inputLength = inputLength;

        huffEncoder = new HuffmanEncoder(inputLength);
        varrlEncoder = new VariableRunLengthEncoder(inputLength);
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        // Counting length/jump ratio
        int jumps = 0;

        input.mark();
        byte prevByte = input.get();
        while(input.hasRemaining()) {
            byte currentByte = input.get();
            if(currentByte != prevByte) {
                ++jumps;
                prevByte = currentByte;
            }
        }
        input.reset();

        double ratio =  (double)jumps / inputLength;

        // System.out.printf("Input jumps/length ratio (%d/%d) : %f\n", jumps, inputLength, ratio);

        int outputLength = 0;

        if(ratio <= RLVH_CONST) {
            output.put(RL_ID);
            outputLength = varrlEncoder.encode(input, output);
        } else {
            output.put(HUFF_ID);
            outputLength = huffEncoder.encode(input, output);
        }

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        byte encoderId = input.get();

        if(encoderId == RL_ID) {
            varrlEncoder.decode(input, output);
        } else {
            huffEncoder.decode(input, output);
        }
    }

    @Override
    public int outputBufferMinLength() {
		return 1 + Math.max(varrlEncoder.outputBufferMinLength(), huffEncoder.outputBufferMinLength());
	}
}