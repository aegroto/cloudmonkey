package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.block.BlockEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.Differential4Encoder;
import com.jme3.util.BufferUtils;

public class BlockD4Encoder implements Encoder {
    private static final int bytesPerBlock = 8;

    protected final int inputLength;

    protected final BlockEncoder blockEncoder;
    protected final Differential4Encoder d4Encoder;

    protected ByteBuffer intermediateBuffer;

    public BlockD4Encoder(int inputLength) {
        this.inputLength = inputLength;

        blockEncoder = new BlockEncoder(bytesPerBlock);
        d4Encoder = new Differential4Encoder(blockEncoder.outputBufferMinLength());

        intermediateBuffer = BufferUtils.createByteBuffer(blockEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        while(input.hasRemaining()) {
            blockEncoder.encode(input, intermediateBuffer);
            intermediateBuffer.flip();
            d4Encoder.encode(intermediateBuffer, output);
            intermediateBuffer.rewind();
        }

        intermediateBuffer.clear();

        return input.capacity();
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        while(input.hasRemaining()) {
            blockEncoder.encode(input, intermediateBuffer);
            intermediateBuffer.flip();
            d4Encoder.decode(intermediateBuffer, output);
            intermediateBuffer.rewind();
        }

        intermediateBuffer.clear();
    }

    @Override
    public int outputBufferMinLength() {
		return inputLength;
	}

}