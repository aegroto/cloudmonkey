package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelizer.Channelizer4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.DownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.NativePixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelRepPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.PixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;
import com.jme3.util.BufferUtils;

public class PixelRepDSLZ4Encoder implements Encoder {
    protected final int width, height; 

    protected final Encoder pixelDSEncoder;
    protected final Encoder lz4Encoder;

    protected ByteBuffer encodeIntermediateBuffer, decodeIntermediateBuffer;

    public PixelRepDSLZ4Encoder(int width, int height) {
        this.width = width;
        this.height = height;

        pixelDSEncoder = new ParallelRepPixelDownsamplingEncoder(width, height);
        lz4Encoder = new MultithreadLZ4Encoder(pixelDSEncoder.outputBufferMinLength(), 8);
        
        encodeIntermediateBuffer = BufferUtils.createByteBuffer(pixelDSEncoder.outputBufferMinLength());
        decodeIntermediateBuffer = BufferUtils.createByteBuffer(lz4Encoder.outputBufferMinLength());
    }
    
    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        pixelDSEncoder.encode(input, encodeIntermediateBuffer);
        encodeIntermediateBuffer.flip();

        int outputLength = lz4Encoder.encode(encodeIntermediateBuffer, output);

        encodeIntermediateBuffer.clear();

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        lz4Encoder.decode(input, decodeIntermediateBuffer);
        decodeIntermediateBuffer.flip();

        pixelDSEncoder.decode(decodeIntermediateBuffer, output);
        decodeIntermediateBuffer.clear();
    }

    @Override
    public int outputBufferMinLength() {
		return lz4Encoder.outputBufferMinLength();
	}

}