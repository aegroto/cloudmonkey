package com.aegroto.cloudmonkey.common.encoding.stateless.wavelet;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;

import com.aegroto.cloudmonkey.common.Helpers;
import com.aegroto.cloudmonkey.common.encoding.Encoder;

import org.lwjgl.BufferUtils;

public class HaarWaveletEncoder implements Encoder {
    public static final int MAX_CYCLES = 8;

    protected final int inputLength;

    protected final int inputSizeThreshold = 1;

    public HaarWaveletEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    protected double byteToFloat(byte b) {
        if(b > 0)
            return b / 127.0;
        else
            return b / 128.0;
    }

    protected byte floatToByte(double f) {
        if(f > 0f)
            return (byte) (f * 127.0);
        else
            return (byte) (f * 128.0);
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        DoubleBuffer doubleInput = BufferUtils.createDoubleBuffer(input.remaining());

        // Converting bytes to doubles for better precision during the process
        while(input.hasRemaining()) {
            byte b = input.get();

            doubleInput.put(byteToFloat(b));
        }
        doubleInput.flip();

        int inputMaxLimit = doubleInput.limit();
        int maxSize = doubleInput.remaining();
       
        // Applying the haar transform to our first lowest-power-of-two elements
        // and iterate that procedure to remainings elements.
        while(maxSize > inputSizeThreshold) {
            int size = Helpers.lowestPowerof2(maxSize);

            DoubleBuffer sumBuffer = BufferUtils.createDoubleBuffer(size);
            DoubleBuffer diffBuffer = BufferUtils.createDoubleBuffer(size); 
       
            // Data buffer contains all elements which should be transformed during the current iteration
            DoubleBuffer dataBuffer = BufferUtils.createDoubleBuffer(size);

            doubleInput.limit(doubleInput.position() + size);

            dataBuffer.put(doubleInput);

            dataBuffer.flip();

            int cycles = Helpers.log2(size);
            if(cycles > MAX_CYCLES)
                cycles = MAX_CYCLES;

            int currentCycles = 0;
            
            while(currentCycles < cycles) {
                sumBuffer.clear();

                for(int j = 0; j < size; j += 2) {
                    double a = dataBuffer.get();
                    double b = dataBuffer.get();

                    sumBuffer.put((a + b) / 2.0);
                    diffBuffer.put((a - b) / 2.0);
                }    

                size /= 2;
                ++currentCycles;

                sumBuffer.flip();
                dataBuffer.clear();
                dataBuffer.put(sumBuffer);
                dataBuffer.flip();
            }

            sumBuffer.flip();
            diffBuffer.flip();

            while(diffBuffer.hasRemaining()) {
                double f = diffBuffer.get();

                output.put(floatToByte(f));
            }

            while(sumBuffer.hasRemaining()) {
                double f = sumBuffer.get();

                output.put(floatToByte(f));
            }

            doubleInput.limit(inputMaxLimit);

            maxSize = doubleInput.remaining();
        }

        return inputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        int inputMaxLimit = input.limit();

        int maxSize = input.remaining();

        while(maxSize > inputSizeThreshold) {
            int size = Helpers.lowestPowerof2(maxSize);

            ByteBuffer sumBuffer = BufferUtils.createByteBuffer(size);
            ByteBuffer diffBuffer = BufferUtils.createByteBuffer(size);
            
            ByteBuffer dataBuffer = BufferUtils.createByteBuffer(size);

            input.limit(input.position() + size);

            dataBuffer.put(input);
            dataBuffer.flip();

            int cycles = Helpers.log2(size);
            if(cycles > MAX_CYCLES)
                cycles = MAX_CYCLES;

            int currentCycles = 0;

            int currentSize = 1;

            currentSize <<= (Helpers.log2(size) - cycles);

            while(currentCycles < cycles) {
                diffBuffer.clear();
                sumBuffer.clear();

                currentSize <<= 1; 
               
                dataBuffer.position(dataBuffer.limit() - currentSize);

                dataBuffer.limit(dataBuffer.position() + currentSize / 2);
                diffBuffer.put(dataBuffer);
                diffBuffer.flip();

                dataBuffer.limit(dataBuffer.position() + currentSize / 2);
                sumBuffer.put(dataBuffer);
                sumBuffer.flip();

                dataBuffer.position(dataBuffer.limit() - currentSize);

                while(sumBuffer.hasRemaining()) {
                    byte sum = sumBuffer.get();
                    byte diff = diffBuffer.get();

                    dataBuffer.put((byte) (sum + diff));
                    dataBuffer.put((byte) (sum - diff));
                } 
            
                ++currentCycles;
            }

            dataBuffer.position(0);

            output.put(dataBuffer);

            input.limit(inputMaxLimit);

            maxSize = input.remaining();
        }

        input.limit(inputMaxLimit);
        output.put(input);
    }

    @Override
    public int outputBufferMinLength() {
		return inputLength;
	}
}