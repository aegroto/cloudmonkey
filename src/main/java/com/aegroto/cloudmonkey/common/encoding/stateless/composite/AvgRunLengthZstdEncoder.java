package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.LowRunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.NativeAvgRunLengthEncoder;
import com.jme3.util.BufferUtils;

public class AvgRunLengthZstdEncoder implements Encoder {
    protected final int inputLength;

    protected final NativeAvgRunLengthEncoder avgrlEncoder;
    protected final ZstdEncoder zstdEncoder;

    protected ByteBuffer intermediateBuffer;

    public AvgRunLengthZstdEncoder(int inputLength) {
        this.inputLength = inputLength;

        avgrlEncoder = new NativeAvgRunLengthEncoder(inputLength);
        zstdEncoder = new ZstdEncoder(avgrlEncoder.outputBufferMinLength());

        intermediateBuffer = BufferUtils.createByteBuffer(avgrlEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        avgrlEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = zstdEncoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        zstdEncoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        avgrlEncoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return zstdEncoder.outputBufferMinLength();
	}

}