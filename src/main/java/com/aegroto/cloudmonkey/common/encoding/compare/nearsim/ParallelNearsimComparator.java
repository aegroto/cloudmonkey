package com.aegroto.cloudmonkey.common.encoding.compare.nearsim;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.compare.Comparator;
import com.aegroto.cloudmonkey.common.opencl.OpenCLManager;
import com.jme3.opencl.Buffer;
import com.jme3.opencl.CommandQueue;
import com.jme3.opencl.Event;
import com.jme3.opencl.Kernel;
import com.jme3.opencl.Program;
import com.jme3.opencl.Kernel.WorkSize;

public class ParallelNearsimComparator implements Comparator {
    protected final int width, height, inputLength, outputLength;

    protected Kernel encodeKernel, decodeKernel;
    protected Buffer lastFrameBuffer, currentFrameBuffer, encodedBuffer;

    protected WorkSize encodeLocalWorkSize;
    protected WorkSize encodeGlobalWorkSize;

    protected WorkSize decodeLocalWorkSize;
    protected WorkSize decodeGlobalWorkSize;

    public ParallelNearsimComparator(int width, int height) {
        this.width = width;
        this.height = height;
        this.inputLength = width * height * 4;
        this.outputLength = width * height;

        Program program = OpenCLManager.loadProgram("cl_src/nearsim/nearsim_compare_square.ocl");
        program.build();

        encodeKernel = program.createKernel("nearsim_compare_encode");
        decodeKernel = program.createKernel("nearsim_compare_decode");

        lastFrameBuffer = OpenCLManager.getContext().createBuffer(inputLength);
        currentFrameBuffer = OpenCLManager.getContext().createBuffer(inputLength);

        encodedBuffer = OpenCLManager.getContext().createBuffer(outputBufferMinLength());

        int encodePreferredLwsX = 32;
        int encodeLwsX = encodePreferredLwsX > width ? width : encodePreferredLwsX;

        int encodePreferredLwsY = 32;
        int encodeLwsY = encodePreferredLwsY > height ? height  : encodePreferredLwsY;

        encodeLocalWorkSize = new WorkSize(encodeLwsX, encodeLwsY);
        encodeGlobalWorkSize = new WorkSize(bestGlobalWorksize(width, encodeLwsX), bestGlobalWorksize(height, encodeLwsY));

        int decodePreferredLwsX = 32;
        int decodeLwsX = decodePreferredLwsX > width ? width : decodePreferredLwsX;

        int decodePreferredLwsY = 32;
        int decodeLwsY = decodePreferredLwsY > height ? height : decodePreferredLwsY;

        decodeLocalWorkSize = new WorkSize(decodeLwsX, decodeLwsY);
        decodeGlobalWorkSize = new WorkSize(bestGlobalWorksize(width, decodeLwsX), bestGlobalWorksize(height, decodeLwsY));
    }

    protected int multUp(int a, int b) {
        return (a + b - 1) / b;
    }

    protected int bestGlobalWorksize(int length, int lws) {
        return ((length + lws - 1) / lws) * lws;
    }

    @Override
    public int encode(ByteBuffer lastFrameInput, ByteBuffer currentFrameInput, ByteBuffer encodedOutput) {
        CommandQueue queue = OpenCLManager.getCmdQueue(); 

        lastFrameBuffer.write(queue, lastFrameInput);
        currentFrameBuffer.write(queue, currentFrameInput);

        queue.finish();

        lastFrameInput.position(lastFrameInput.limit());
        currentFrameInput.position(currentFrameInput.limit());

        Event encodeEvent = encodeKernel.Run2(queue,
            encodeGlobalWorkSize, 
            encodeLocalWorkSize,
            lastFrameBuffer, 
            currentFrameBuffer, 
            encodedBuffer,
            width,
            height
        );
        encodeEvent.waitForFinished();

        queue.finish();

        encodedBuffer.read(queue, encodedOutput, outputLength);

        queue.finish();

        encodedOutput.position(encodedOutput.position() + outputLength);

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer lastFrameInput, ByteBuffer encodedFrameInput, ByteBuffer currentFrameOutput) {
        CommandQueue queue = OpenCLManager.getCmdQueue(); 

        lastFrameBuffer.write(queue, lastFrameInput);
        encodedBuffer.write(queue, encodedFrameInput);

        queue.finish();

        lastFrameInput.position(lastFrameInput.limit());
        encodedFrameInput.position(encodedFrameInput.limit());

        Event decodeEvent = decodeKernel.Run2(queue,
            decodeGlobalWorkSize, 
            decodeLocalWorkSize,
            lastFrameBuffer, 
            encodedBuffer,
            currentFrameBuffer, 
            width,
            height
        );
        decodeEvent.waitForFinished();

        currentFrameBuffer.read(queue, currentFrameOutput, inputLength);

        queue.finish();

        currentFrameOutput.position(currentFrameOutput.position() + inputLength);
    }

    @Override
    public int outputBufferMinLength() {
        return outputLength;
    }
}