package com.aegroto.cloudmonkey.common.encoding.stateless.huffman;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.jme3.system.NativeLibraryLoader;
import com.jme3.system.Platform;

public class NativeCanonicalHuffmanEncoder implements Encoder {
    static {
        NativeLibraryLoader.registerNativeLibrary("NativeClasses", Platform.Linux64, "libs/nativeclasses.so");        
        
        NativeLibraryLoader.loadNativeLibrary("NativeClasses", true);
    }

    protected final int inputLength;

    public NativeCanonicalHuffmanEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    public native int encodeNative(ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int inputLength);
    public native void decodeNative(ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int inputLength);

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outLength = encodeNative(input, output, input.position(), output.position(), inputLength);

        input.position(input.limit());
        output.position(output.position() + outLength);

        return outLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        decodeNative(input, output, input.position(), output.position(), inputLength);

        input.position(input.limit());
        output.position(output.position() + inputLength);
    }

    @Override
    public int outputBufferMinLength() {
        return 8 + ((256 * 2 - 1) * 5) + inputLength;
    }
}