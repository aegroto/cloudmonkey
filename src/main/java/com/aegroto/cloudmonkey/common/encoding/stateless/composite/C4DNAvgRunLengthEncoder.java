package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.aegroto.cloudmonkey.common.Helpers;
import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelizer.Channelizer4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.DifferentialEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.NativeAvgRunLengthEncoder;
import com.jme3.util.BufferUtils;

public class C4DNAvgRunLengthEncoder implements Encoder {
    protected final int inputLength; 

    protected final Channelizer4Encoder c4Encoder;
    protected final DifferentialEncoder diffEncoder;
    protected final NativeAvgRunLengthEncoder varRlEncoder;

    protected ByteBuffer intermediateBuffer0, intermediateBuffer1, inputChannelBuffer, outputChannelBuffer;

    public C4DNAvgRunLengthEncoder(int inputLength) {
        this.inputLength = inputLength;

        c4Encoder = new Channelizer4Encoder(inputLength);
        diffEncoder = new DifferentialEncoder(inputLength / 4);
        varRlEncoder = new NativeAvgRunLengthEncoder(inputLength / 4);
        
        intermediateBuffer0 = BufferUtils.createByteBuffer(c4Encoder.outputBufferMinLength());
        
        inputChannelBuffer = BufferUtils.createByteBuffer(inputLength / 4).order(getByteOrder());
        intermediateBuffer1 = BufferUtils.createByteBuffer(inputLength / 4).order(getByteOrder());
        outputChannelBuffer = BufferUtils.createByteBuffer(varRlEncoder.outputBufferMinLength()).order(getByteOrder());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        c4Encoder.encode(input, intermediateBuffer0);
        intermediateBuffer0.position(0);

        int outputLength = 0;

        for(int i = 0; i < 4; ++i) {
            intermediateBuffer0.limit((i + 1) * (inputLength / 4));
            inputChannelBuffer.put(intermediateBuffer0);
            inputChannelBuffer.flip();

            diffEncoder.encode(inputChannelBuffer, intermediateBuffer1);
            intermediateBuffer1.flip();

            /*System.out.print("Channel buffer: ");
            while(channelBuffer.hasRemaining()) {
                System.out.print(channelBuffer.get() + " ");
            }
            System.out.print("\n");
            channelBuffer.flip();*/

            // System.out.printf("Intermediate buffer stats: %d %d\n", intermediateBuffer.position(), intermediateBuffer.limit());

            /*int jumps = 0;
            byte currentValue = channelBuffer.get();
            while(channelBuffer.hasRemaining()) {
                byte newValue = channelBuffer.get();

                if(newValue != currentValue) {
                    currentValue = newValue;
                    ++jumps;
                }                    
            }
            channelBuffer.flip();
            //System.out.println("Jumps: " + jumps + "/" + inputLength / 4);*/

            outputLength += varRlEncoder.encode(intermediateBuffer1, output);

            /*int outputPos = output.position();
            int outputLimit = output.limit();
            output.flip();
            //System.out.print("Output buffer: ");
            while(output.hasRemaining()) {
                //System.out.print(output.get() + " ");
            }
            //System.out.print("\n");
            output.position(outputPos);
            output.limit(outputLimit);*/

            inputChannelBuffer.clear();
            intermediateBuffer1.clear();
        }

        /*int bufPos = intermediateBuffer.position();
        intermediateBuffer.position(0);
        //System.out.print("Intermediate buffer: ");
        while(intermediateBuffer.hasRemaining()) {
            //System.out.print(intermediateBuffer.get() + " ");
        }
        //System.out.print("\n\n");
        intermediateBuffer.position(bufPos);*/

        intermediateBuffer0.clear();

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        int inputMaxLimit = input.limit();

        for(int i = 0; i < 4; ++i) {
            input.limit(inputMaxLimit);

            /*int inputPos = input.position();
            //System.out.print("\nRemaining input: ");
            while(input.hasRemaining()) {
                //System.out.print(input.get() + " ");
            }
            //System.out.print("\n");
            input.position(inputPos);*/

            //System.out.printf("\nInput buffer stats: %d %d\n", input.position(), input.limit());

            int inputBits = input.getInt();
            int encodedChannelLength = (inputBits + 8 - 1) / 8;

            // System.out.printf("Encoded channel %d length: %d (%d)\n", i, encodedChannelLength, inputBits);

            input.limit(input.position() + encodedChannelLength + 4);
            outputChannelBuffer.putInt(inputBits);
            outputChannelBuffer.put(input);
            outputChannelBuffer.flip();

            /*System.out.print("Channel buffer: ");
            while(channelBuffer.hasRemaining()) {
                System.out.print(Helpers.byteToBinaryString(channelBuffer.get()) + " ");
            }
            System.out.print("\n");
            channelBuffer.flip();*/

            //System.out.printf("Channel buffer stats: %d %d\n", channelBuffer.position(), channelBuffer.limit());

            // System.out.printf("Decoding... (%d)\n", intermediateBuffer1.remaining());

            varRlEncoder.decode(outputChannelBuffer, intermediateBuffer1);
            intermediateBuffer1.flip();
            
            /*int bufPos = intermediateBuffer.position();
            intermediateBuffer.position(0);
            System.out.print("Intermediate buffer: ");
            while(intermediateBuffer.hasRemaining()) {
                System.out.print(intermediateBuffer.get() + " ");
            }
            System.out.print("\n");
            intermediateBuffer.position(bufPos);*/

            diffEncoder.decode(intermediateBuffer1, intermediateBuffer0);

            intermediateBuffer1.clear();
            outputChannelBuffer.clear();
        }

        intermediateBuffer0.clear();

        //System.out.printf("Intermediate buffer stats: %d %d\n", intermediateBuffer.position(), intermediateBuffer.limit());

        c4Encoder.decode(intermediateBuffer0, output);
        intermediateBuffer0.clear();
    }

    @Override
    public int outputBufferMinLength() {
		return varRlEncoder.outputBufferMinLength() * 4;
    }

    @Override
    public ByteOrder getByteOrder() {
        return ByteOrder.nativeOrder();
    }

}