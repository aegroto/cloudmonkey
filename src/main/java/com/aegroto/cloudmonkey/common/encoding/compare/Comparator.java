package com.aegroto.cloudmonkey.common.encoding.compare;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public interface Comparator {
    public abstract int encode(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output);
    public abstract void decode(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output);
    public abstract int outputBufferMinLength();

    default ByteOrder getByteOrder() { return ByteOrder.BIG_ENDIAN; }
}