package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.ApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder;
import com.jme3.util.BufferUtils;

public class ApproxHuffmanEncoder implements Encoder {
    protected final int inputLength;

    protected final ApproximationEncoder approxEncoder;
    protected final HuffmanEncoder huffmanEncoder;

    protected ByteBuffer intermediateBuffer;

    public ApproxHuffmanEncoder(int inputLength) {
        this.inputLength = inputLength;

        approxEncoder = new ApproximationEncoder(inputLength);
        huffmanEncoder = new HuffmanEncoder(inputLength);

        intermediateBuffer = BufferUtils.createByteBuffer(approxEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        approxEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = huffmanEncoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
       huffmanEncoder.decode(input, output); 
    }

    @Override
    public int outputBufferMinLength() {
		return huffmanEncoder.outputBufferMinLength();
	}

}