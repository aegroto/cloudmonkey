package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.DownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder;
import com.jme3.util.BufferUtils;

import lombok.Getter;

public class DownsamplingZstdEncoder implements Encoder {
    protected final int width, height;

    @Getter protected final DownsamplingEncoder downsamplingEncoder;
    protected final ZstdEncoder zstdEncoder;

    protected ByteBuffer intermediateBuffer;

    public DownsamplingZstdEncoder(int width, int height) {
        this.width = width;
        this.height = height;

        downsamplingEncoder = new DownsamplingEncoder(width, height);
        zstdEncoder = new ZstdEncoder(downsamplingEncoder.outputBufferMinLength());

        intermediateBuffer = BufferUtils.createByteBuffer(downsamplingEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        downsamplingEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = zstdEncoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        zstdEncoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        downsamplingEncoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return zstdEncoder.outputBufferMinLength();
    }

}