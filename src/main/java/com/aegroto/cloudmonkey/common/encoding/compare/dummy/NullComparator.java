package com.aegroto.cloudmonkey.common.encoding.compare.dummy;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.compare.Comparator;

public class NullComparator implements Comparator {
    protected final int inputLength;

    public NullComparator(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output) {
        while(secondInput.hasRemaining()) {
            output.put(secondInput.get());
        }

        return inputLength;
    }

    @Override
    public void decode(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output) {
        while(secondInput.hasRemaining()) {
            output.put(secondInput.get());
        }
    }

    @Override
    public int outputBufferMinLength() {
        return inputLength;
    }
}