package com.aegroto.cloudmonkey.common.encoding.stateless.downsampling;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

public class DownsampledRunLengthEncoder implements Encoder {
    protected final int inputLength;

    protected final int maxDelta = 10;

    public DownsampledRunLengthEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outputLength = 0;
        int outputOffset = output.position();

        byte value = input.get();
        byte lastValue = value;

        byte currentValue = value;
        int occurrences = 1;

        output.position(outputOffset + 4);

        while(input.hasRemaining()) {
            currentValue = input.get();

            if(Math.abs(value - currentValue) < maxDelta) {
                lastValue = currentValue;
                ++occurrences;
            } else {
                output.put(value);
                output.put(lastValue);
                output.putInt(occurrences);
                outputLength += 6;

                value = currentValue;

                occurrences = 1;
            }
        }

        output.put(value);
        output.put(lastValue);
        output.putInt(occurrences);
        outputLength += 6;

        output.putInt(outputOffset, outputLength);

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        int inputBytesLength = input.getInt();
        int decodedBytes = 0;

        while(decodedBytes < inputBytesLength) {
            byte value = input.get();
            byte lastValue = input.get();
            int occurrences = input.getInt();

            // System.out.printf("Value: %d, Last value: %d, occurrences: %d\n", value, lastValue, occurrences);

            for(int i = 0; i < occurrences - 1; ++i) {
                byte lerpedByte = lerp(value, lastValue, ((float) i) / (occurrences)); 
                // System.out.printf("Value: %d, Last value: %d, %d-th byte on %d: %d\n", value, lastValue, i, occurrences - 1, lerpedByte);
                output.put(lerpedByte);
            }

            output.put(lastValue);

            decodedBytes += 6;
        }
    }

    protected byte lerp(byte first, byte last, float factor) {
        return (byte) (first + (last - first) * factor);
    }

    @Override
    public int outputBufferMinLength() {
		return 4 + inputLength*2 + inputLength * 4;
	}

}