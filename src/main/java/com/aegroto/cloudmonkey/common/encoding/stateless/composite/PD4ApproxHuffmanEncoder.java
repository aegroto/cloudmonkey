package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.ApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.PredictiveDifferential4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder;
import com.jme3.util.BufferUtils;

public class PD4ApproxHuffmanEncoder implements Encoder {
    protected final int inputLength;

    protected final ApproximationEncoder approxEncoder;
    protected final PredictiveDifferential4Encoder d4Encoder;
    protected final HuffmanEncoder huffmanEncoder;

    protected ByteBuffer intermediateBuffer0, intermediateBuffer1;

    public PD4ApproxHuffmanEncoder(int inputLength) {
        this.inputLength = inputLength;

        approxEncoder = new ApproximationEncoder(inputLength);
        d4Encoder = new PredictiveDifferential4Encoder(inputLength);
        huffmanEncoder = new HuffmanEncoder(inputLength);

        intermediateBuffer0 = BufferUtils.createByteBuffer(approxEncoder.outputBufferMinLength());
        intermediateBuffer1 = BufferUtils.createByteBuffer(d4Encoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        approxEncoder.encode(input, intermediateBuffer0);
        intermediateBuffer0.flip();

        d4Encoder.encode(intermediateBuffer0, intermediateBuffer1);
        intermediateBuffer1.flip();

        int outputLength = huffmanEncoder.encode(intermediateBuffer1, output);

        intermediateBuffer0.rewind();
        intermediateBuffer1.rewind();

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        huffmanEncoder.decode(input, intermediateBuffer1);
        intermediateBuffer1.flip();
        d4Encoder.decode(intermediateBuffer1, output);

        intermediateBuffer1.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return huffmanEncoder.outputBufferMinLength();
	}

}