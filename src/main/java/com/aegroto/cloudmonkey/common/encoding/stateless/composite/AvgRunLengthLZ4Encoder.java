package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.LowRunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.NativeAvgRunLengthEncoder;
import com.jme3.util.BufferUtils;

public class AvgRunLengthLZ4Encoder implements Encoder {
    protected final int inputLength;

    protected final NativeAvgRunLengthEncoder avgrlEncoder;
    protected final LZ4Encoder lz4Encoder;

    protected ByteBuffer intermediateBuffer;

    public AvgRunLengthLZ4Encoder(int inputLength) {
        this.inputLength = inputLength;

        avgrlEncoder = new NativeAvgRunLengthEncoder(inputLength);
        lz4Encoder = new LZ4Encoder(avgrlEncoder.outputBufferMinLength());

        intermediateBuffer = BufferUtils.createByteBuffer(avgrlEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        avgrlEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = lz4Encoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        lz4Encoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        avgrlEncoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return lz4Encoder.outputBufferMinLength();
	}

}