package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.Differential4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;
import com.jme3.util.BufferUtils;

public class D4LZ4Encoder implements Encoder {
    protected final int inputLength;

    protected final Differential4Encoder d4Encoder;
    protected final LZ4Encoder lz4Encoder;

    protected ByteBuffer intermediateBuffer;

    public D4LZ4Encoder(int inputLength) {
        this.inputLength = inputLength;

        d4Encoder = new Differential4Encoder(inputLength);
        lz4Encoder = new LZ4Encoder(inputLength);

        intermediateBuffer = BufferUtils.createByteBuffer(d4Encoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        d4Encoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = lz4Encoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        lz4Encoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        d4Encoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return lz4Encoder.outputBufferMinLength();
	}

}