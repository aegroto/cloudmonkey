package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelizer.Channelizer4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder;
import com.jme3.util.BufferUtils;

public class C4HuffmanEncoder implements Encoder {
    protected final int inputLength; 

    protected final Channelizer4Encoder c4Encoder;
    protected final HuffmanEncoder huffmanEncoder;

    protected ByteBuffer intermediateBuffer;
    public C4HuffmanEncoder(int inputLength) {
        this.inputLength = inputLength;

        c4Encoder = new Channelizer4Encoder(inputLength);
        huffmanEncoder = new HuffmanEncoder(inputLength / 4);
        
        intermediateBuffer = BufferUtils.createByteBuffer(c4Encoder.outputBufferMinLength());
    }
    
    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        /*System.out.print("Input: ");
        while(input.hasRemaining()) {
            System.out.print(input.get() + " ");
        }
        System.out.print("\n");
        input.flip();*/

        c4Encoder.encode(input, intermediateBuffer);
        intermediateBuffer.position(0);

        ByteBuffer channelBuffer = BufferUtils.createByteBuffer(inputLength / 4).order(getByteOrder());

        int outputLength = 0;

        for(int i = 0; i < 4; ++i) {
            intermediateBuffer.limit((i + 1) * (inputLength / 4));
            // System.out.printf("Intermediate buffer #%d stats: %d %d\n", i, intermediateBuffer.position(), intermediateBuffer.limit());
            channelBuffer.put(intermediateBuffer);
            channelBuffer.flip();

            // System.out.printf("Intermediate buffer stats: %d %d\n", intermediateBuffer.position(), intermediateBuffer.limit());
           
            /*System.out.print("Channel buffer: ");
            while(channelBuffer.hasRemaining()) {
                System.out.print(channelBuffer.get() + " ");
            }
            System.out.print("\n");
            channelBuffer.flip();*/

            // System.out.printf("Intermediate buffer stats: %d %d\n", intermediateBuffer.position(), intermediateBuffer.limit());

            outputLength += huffmanEncoder.encode(channelBuffer, output);

            /*int outputPos = output.position();
            int outputLimit = output.limit();
            output.flip();
            System.out.print("Output buffer: ");
            while(output.hasRemaining()) {
                System.out.print(output.get() + " ");
            }
            System.out.print("\n");
            output.position(outputPos);
            output.limit(outputLimit);*/

            channelBuffer.clear();
        }

        /*int bufPos = output.position();
        output.position(0);
        System.out.print("Output buffer: ");
        while(output.hasRemaining()) {
            System.out.print(output.get() + " ");
        }
        System.out.print("\n\n");
        output.position(bufPos);*/

        intermediateBuffer.clear();

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        ByteBuffer channelBuffer = BufferUtils.createByteBuffer(huffmanEncoder.outputBufferMinLength()).order(getByteOrder());

        int inputMaxLimit = input.limit();

        while(intermediateBuffer.hasRemaining()) {
            intermediateBuffer.put((byte) 0);
        }
        intermediateBuffer.clear();

        for(int i = 0; i < 4; ++i) {
            input.limit(inputMaxLimit);
           
            /*int inputPos = input.position();
            System.out.print("\nRemaining input: ");
            while(input.hasRemaining()) {
                System.out.print(input.get() + " ");
            }
            System.out.print("\n");
            input.position(inputPos);*/

            // System.out.printf("\nInput buffer stats: %d %d\n", input.position(), input.limit());

            int huffmanTreeSize = input.getInt(input.position());
            int inputStringLength = input.getInt(input.position() + 4);
            //System.out.println("Huffman tree size: " + huffmanTreeSize);
            //System.out.println("Input string length: " + inputStringLength);
            int encodedChannelLength = 
                (huffmanTreeSize > 1 ? huffmanTreeSize : 1)
              + (inputStringLength + 8 - 1) / 8;

            // System.out.printf("Encoded channel %d length: %d\n", i, encodedChannelLength);

            input.limit(input.position() + encodedChannelLength + 8);
            channelBuffer.put(input);
            channelBuffer.flip();

            /*System.out.print("Channel buffer: ");
            while(channelBuffer.hasRemaining()) {
                System.out.print(channelBuffer.get() + " ");
            }
            System.out.print("\n");
            channelBuffer.flip();*/

            // System.out.printf("Channel buffer stats: %d %d\n", channelBuffer.position(), channelBuffer.limit());

            // System.out.printf("Decoding... (%d)\n", intermediateBuffer.remaining());

            huffmanEncoder.decode(channelBuffer, intermediateBuffer);

            /*int bufPos = intermediateBuffer.position();
            intermediateBuffer.position(0);
            System.out.print("Intermediate buffer: ");
            while(intermediateBuffer.hasRemaining()) {
                System.out.print(intermediateBuffer.get() + " ");
            }
            System.out.print("\n");
            intermediateBuffer.position(bufPos);*/

            channelBuffer.clear();
        }

        intermediateBuffer.flip();

        c4Encoder.decode(intermediateBuffer, output);
        intermediateBuffer.clear();
    }

    @Override
    public int outputBufferMinLength() {
		return huffmanEncoder.outputBufferMinLength() * 4;
	}

}