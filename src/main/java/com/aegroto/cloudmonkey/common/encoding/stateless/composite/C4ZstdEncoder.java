package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelizer.Channelizer4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder;
import com.jme3.util.BufferUtils;

public class C4ZstdEncoder implements Encoder {
    protected final int inputLength; 

    protected final Channelizer4Encoder c4Encoder;
    protected final ZstdEncoder zstdEncoder;

    protected ByteBuffer intermediateBuffer;

    public C4ZstdEncoder(int inputLength) {
        this.inputLength = inputLength;

        c4Encoder = new Channelizer4Encoder(inputLength);
        zstdEncoder = new ZstdEncoder(inputLength / 4);
        
        intermediateBuffer = BufferUtils.createByteBuffer(c4Encoder.outputBufferMinLength());
    }
    
    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        c4Encoder.encode(input, intermediateBuffer);
        intermediateBuffer.position(0);

        ByteBuffer channelBuffer = BufferUtils.createByteBuffer(inputLength / 4).order(getByteOrder());
        ByteBuffer channelOutBuffer = BufferUtils.createByteBuffer(zstdEncoder.outputBufferMinLength()).order(getByteOrder());

        int outputLength = 0;

        for(int i = 0; i < 4; ++i) {
            intermediateBuffer.limit((i + 1) * (inputLength / 4));
            channelBuffer.put(intermediateBuffer);
            channelBuffer.flip();

            /*System.out.printf("Channel buffer: ");
            while(channelBuffer.hasRemaining()) {
                System.out.printf("%d ", channelBuffer.get());
            }
            System.out.printf("\n");
            channelBuffer.flip();*/

            int channelOutLength = zstdEncoder.encode(channelBuffer, channelOutBuffer);
            outputLength += channelOutLength;

            output.putInt(channelOutLength);

            channelOutBuffer.flip();
            output.put(channelOutBuffer);

            // System.out.println("Input length: " + channelOutLength);

            channelBuffer.clear();
            channelOutBuffer.clear();
        }

        intermediateBuffer.clear();

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        ByteBuffer channelBuffer = BufferUtils.createByteBuffer(zstdEncoder.outputBufferMinLength()).order(getByteOrder());

        int inputMaxLimit = input.limit();

        for(int i = 0; i < 4; ++i) {
            input.limit(inputMaxLimit);

            int encodedChannelLength = input.getInt();

            // System.out.println("Output length: " + encodedChannelLength + " -- " + input.remaining() + " -- " + intermediateBuffer.position());

            input.limit(input.position() + encodedChannelLength);

            channelBuffer.put(input);
            channelBuffer.flip();

            zstdEncoder.decode(channelBuffer, intermediateBuffer);

            channelBuffer.clear();
        }

        intermediateBuffer.flip();

        c4Encoder.decode(intermediateBuffer, output);
        intermediateBuffer.clear();
    }

    @Override
    public int outputBufferMinLength() {
        return (4 + zstdEncoder.outputBufferMinLength()) * 4;
	}
}