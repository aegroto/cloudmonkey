package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelizer.Channelizer4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.DifferentialEncoder;
import com.jme3.util.BufferUtils;

public class C4DSelRLVHEncoder implements Encoder {
    protected final int inputLength; 

    protected final Channelizer4Encoder c4Encoder;
    protected final DifferentialEncoder dEncoder;
    protected final SelectiveRLVHEncoder selectiveEncoder;

    protected ByteBuffer intermediateBuffer;

    public C4DSelRLVHEncoder(int inputLength) {
        this.inputLength = inputLength;

        c4Encoder = new Channelizer4Encoder(inputLength);
        dEncoder = new DifferentialEncoder(inputLength);
        selectiveEncoder = new SelectiveRLVHEncoder(inputLength / 4);
        
        intermediateBuffer = BufferUtils.createByteBuffer(c4Encoder.outputBufferMinLength());
    }
    
    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        c4Encoder.encode(input, intermediateBuffer);
        intermediateBuffer.position(0);

        ByteBuffer channelBuffer0 = BufferUtils.createByteBuffer(inputLength / 4).order(getByteOrder());
        ByteBuffer channelBuffer1 = BufferUtils.createByteBuffer(inputLength / 4).order(getByteOrder());

        int outputLength = 0;

        for(int i = 0; i < 4; ++i) {
            intermediateBuffer.limit((i + 1) * (inputLength / 4));
            // System.out.printf("Intermediate buffer #%d stats: %d %d\n", i, intermediateBuffer.position(), intermediateBuffer.limit());
            channelBuffer0.put(intermediateBuffer);
            channelBuffer0.flip();

            // System.out.printf("Intermediate buffer stats: %d %d\n", intermediateBuffer.position(), intermediateBuffer.limit());
           
            /*System.out.print("Channel buffer 0 : ");
            while(channelBuffer0.hasRemaining()) {
                System.out.print(channelBuffer0.get() + " ");
            }
            System.out.print("\n");
            channelBuffer0.flip();*/

            // System.out.printf("Intermediate buffer stats: %d %d\n", intermediateBuffer.position(), intermediateBuffer.limit());

            dEncoder.encode(channelBuffer0, channelBuffer1);

            /*channelBuffer1.flip();
            System.out.print("Channel buffer 1 : ");
            while(channelBuffer1.hasRemaining()) {
                System.out.print(channelBuffer1.get() + " ");
            }
            System.out.print("\n");*/

            channelBuffer1.flip();
            outputLength += selectiveEncoder.encode(channelBuffer1, output);

            /*int outputPos = output.position();
            int outputLimit = output.limit();
            output.flip();
            System.out.print("Output buffer: ");
            while(output.hasRemaining()) {
                System.out.print(output.get() + " ");
            }
            System.out.print("\n");
            output.position(outputPos);
            output.limit(outputLimit);*/

            channelBuffer0.clear();
            channelBuffer1.clear();
        }

        /*int bufPos = intermediateBuffer.position();
        intermediateBuffer.position(0);
        System.out.print("Intermediate buffer: ");
        while(intermediateBuffer.hasRemaining()) {
            System.out.print(intermediateBuffer.get() + " ");
        }
        System.out.print("\n\n");
        intermediateBuffer.position(bufPos);*/

        intermediateBuffer.clear();

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        ByteBuffer channelBuffer0 = BufferUtils.createByteBuffer(selectiveEncoder.outputBufferMinLength()).order(getByteOrder());
        ByteBuffer channelBuffer1 = BufferUtils.createByteBuffer(dEncoder.outputBufferMinLength()).order(getByteOrder());

        int inputMaxLimit = input.limit();

        for(int i = 0; i < 4; ++i) {
            input.limit(inputMaxLimit);

            // get input length
            input.mark();
            int encodedChannelLength = 0;
            byte encoderId = input.get();

            if(encoderId == SelectiveRLVHEncoder.HUFF_ID) {
                int huffmanTreeSize = input.getInt(input.position());
                int inputStringLength = input.getInt(input.position() + 4);
                encodedChannelLength = 
                    (huffmanTreeSize > 1 ? huffmanTreeSize : 1)
                + (inputStringLength + 8 - 1) / 8;
            
                input.limit(input.position() + encodedChannelLength + 8);
            } else {
                encodedChannelLength = (input.getInt(input.position()) + 8 - 1) / 8;
                
                input.limit(input.position() + encodedChannelLength + 8);
            }
            input.reset();

            channelBuffer0.put(input);
            channelBuffer0.flip();

            selectiveEncoder.decode(channelBuffer0, channelBuffer1);

            channelBuffer1.flip();

            dEncoder.decode(channelBuffer1, intermediateBuffer);

            channelBuffer0.clear();
            channelBuffer1.clear();
        }

        intermediateBuffer.flip();

        c4Encoder.decode(intermediateBuffer, output);
        intermediateBuffer.clear();
    }

    @Override
    public int outputBufferMinLength() {
		return selectiveEncoder.outputBufferMinLength() * 4;
	}

}