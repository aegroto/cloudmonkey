package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.NativeApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelRepPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder;
import com.jme3.util.BufferUtils;

public class ApproxPixelDSEncoder implements Encoder {
    protected final int width, height, inputLength;

    protected final NativeApproximationEncoder approxEncoder;
    protected final ParallelRepPixelDownsamplingEncoder pixelDSEncoder;

    protected ByteBuffer intermediateBuffer;

    public ApproxPixelDSEncoder(int width, int height) {
        this.width = width;
        this.height = height;
        this.inputLength = width * height * 4;

        approxEncoder = new NativeApproximationEncoder(inputLength);
        pixelDSEncoder = new ParallelRepPixelDownsamplingEncoder(width, height);

        intermediateBuffer = BufferUtils.createByteBuffer(approxEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        approxEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();

        int outputLength = pixelDSEncoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        pixelDSEncoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();

        approxEncoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return pixelDSEncoder.outputBufferMinLength();
	}

}