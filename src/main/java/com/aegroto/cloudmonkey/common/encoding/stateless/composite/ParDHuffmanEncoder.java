package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.Differential4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.ParallelPixelDifferentialEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder;
import com.jme3.util.BufferUtils;

public class ParDHuffmanEncoder implements Encoder {
    protected final int inputLength;

    protected final ParallelPixelDifferentialEncoder pdEncoder;
    protected final HuffmanEncoder huffmanEncoder;

    protected ByteBuffer intermediateBuffer;

    public ParDHuffmanEncoder(int inputLength) {
        this.inputLength = inputLength;

        pdEncoder = new ParallelPixelDifferentialEncoder(inputLength);
        huffmanEncoder = new HuffmanEncoder(inputLength);

        intermediateBuffer = BufferUtils.createByteBuffer(pdEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        pdEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();
        int outputLength = huffmanEncoder.encode(intermediateBuffer, output);

        intermediateBuffer.rewind();
        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        huffmanEncoder.decode(input, intermediateBuffer);
        intermediateBuffer.flip();
        pdEncoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return huffmanEncoder.outputBufferMinLength();
	}

}