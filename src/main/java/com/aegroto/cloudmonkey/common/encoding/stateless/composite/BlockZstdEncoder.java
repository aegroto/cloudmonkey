package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.block.BlockEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder;
import com.jme3.util.BufferUtils;

public class BlockZstdEncoder implements Encoder {
    private static final int bytesPerBlock = 1024;

    protected final int inputLength;

    protected final BlockEncoder blockEncoder;
    protected final ZstdEncoder zstdEncoder;

    protected ByteBuffer encodeIntermediateBuffer, decodeIntermediateBuffer;

    public BlockZstdEncoder(int inputLength) {
        this.inputLength = inputLength;

        blockEncoder = new BlockEncoder(bytesPerBlock);
        zstdEncoder = new ZstdEncoder(blockEncoder.outputBufferMinLength());

        encodeIntermediateBuffer = BufferUtils.createByteBuffer(blockEncoder.outputBufferMinLength());
        decodeIntermediateBuffer = BufferUtils.createByteBuffer(zstdEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int totalOutputLength = 0;

        while(input.hasRemaining()) {
            blockEncoder.encode(input, encodeIntermediateBuffer);
            encodeIntermediateBuffer.flip();

            int outPosition = output.position();
            output.position(outPosition + 2);

            int outputLength = zstdEncoder.encode(encodeIntermediateBuffer, output);
            output.position(outPosition);
            output.putShort((short) outputLength);
            output.position(output.position() + outputLength);

            encodeIntermediateBuffer.rewind();

            totalOutputLength += outputLength;

            // System.out.println("Output length: " + outputLength + " (" + input.remaining() + ")");
        }

        encodeIntermediateBuffer.clear();

        return totalOutputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        while(input.hasRemaining()) {
            int encodedInputLength = input.getShort();

            if(encodedInputLength == 0)
                return;

            // System.out.println("Encoded input length: " + encodedInputLength + ", Input's remaining: " + input.remaining());

            int inputLimit = input.limit();
            input.limit(input.position() + encodedInputLength);

            blockEncoder.encode(input, decodeIntermediateBuffer);
            decodeIntermediateBuffer.flip();
            zstdEncoder.decode(decodeIntermediateBuffer, output);
            decodeIntermediateBuffer.clear();

            if(input.limit() < input.capacity() - 2) {
                input.limit(inputLimit);
            }
        }

        decodeIntermediateBuffer.clear();
    }

    @Override
    public int outputBufferMinLength() {
		return (4 * (inputLength/bytesPerBlock)) + inputLength;
	}

}