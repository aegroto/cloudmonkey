package com.aegroto.cloudmonkey.common.encoding.stateless.differential;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

public class Differential4Encoder implements Encoder {
    protected final int inputLength;

    public Differential4Encoder(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        byte lastBytes[] = new byte[4],
             inputBytes[] = new byte[4],
             diffBytes[] = new byte[4];

        for(int i = 0; i < 4; ++i)
            lastBytes[i] = input.get();
        
        output.put(lastBytes);

        while(input.hasRemaining()) {
        for(int i = 0; i < 4; ++i)
            inputBytes[i] = input.get();

        for(int i = 0; i < 4; ++i)
            diffBytes[i] = (byte) (inputBytes[i] - lastBytes[i]);

            output.put(diffBytes);

        for(int i = 0; i < 4; ++i)
            lastBytes[i] = inputBytes[i];
        }

        return input.capacity();
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        byte decodedBytes[] = new byte[4];
        
        for(int i = 0; i < 4; ++i)
            decodedBytes[i] = input.get();

        output.put(decodedBytes);

        while(input.hasRemaining()) {
            for(int i = 0; i < 4; ++i)
                decodedBytes[i] = (byte) ((decodedBytes[i]) + (input.get() & 0xFF));
            
            output.put(decodedBytes);
        }
    }

    @Override
    public int outputBufferMinLength() {
		return inputLength;
	}

}