package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelizer.Channelizer4Encoder;
import com.jme3.util.BufferUtils;

public class C4SelRLVHEncoder implements Encoder {
    protected final int inputLength; 

    protected final Channelizer4Encoder c4Encoder;
    protected final SelectiveRLVHEncoder selectiveEncoder;

    protected ByteBuffer intermediateBuffer;
    public C4SelRLVHEncoder(int inputLength) {
        this.inputLength = inputLength;

        c4Encoder = new Channelizer4Encoder(inputLength);
        selectiveEncoder = new SelectiveRLVHEncoder(inputLength / 4);
        
        intermediateBuffer = BufferUtils.createByteBuffer(c4Encoder.outputBufferMinLength());
    }
    
    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        c4Encoder.encode(input, intermediateBuffer);
        intermediateBuffer.position(0);

        ByteBuffer channelBuffer = BufferUtils.createByteBuffer(inputLength / 4).order(getByteOrder());

        int outputLength = 0;

        for(int i = 0; i < 4; ++i) {
            intermediateBuffer.limit((i + 1) * (inputLength / 4));
            channelBuffer.put(intermediateBuffer);
            channelBuffer.flip();

            outputLength += selectiveEncoder.encode(channelBuffer, output);

            channelBuffer.clear();
        }

        intermediateBuffer.clear();

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        ByteBuffer channelBuffer = BufferUtils.createByteBuffer(selectiveEncoder.outputBufferMinLength()).order(getByteOrder());

        int inputMaxLimit = input.limit();

        while(intermediateBuffer.hasRemaining()) {
            intermediateBuffer.put((byte) 0);
        }
        intermediateBuffer.clear();

        for(int i = 0; i < 4; ++i) {
            input.limit(inputMaxLimit);

            // get input length
            input.mark();
            int encodedChannelLength = 0;
            byte encoderId = input.get();

            if(encoderId == SelectiveRLVHEncoder.HUFF_ID) {
                int huffmanTreeSize = input.getInt(input.position());
                int inputStringLength = input.getInt(input.position() + 4);
                encodedChannelLength = 
                    (huffmanTreeSize > 1 ? huffmanTreeSize : 1)
                + (inputStringLength + 8 - 1) / 8;
            
                input.limit(input.position() + encodedChannelLength + 8);
            } else {
                encodedChannelLength = (input.getInt(input.position()) + 8 - 1) / 8;
                
                input.limit(input.position() + encodedChannelLength + 8);
            }
            input.reset();

            channelBuffer.put(input);
            channelBuffer.flip();

            selectiveEncoder.decode(channelBuffer, intermediateBuffer);

            channelBuffer.clear();
        }

        intermediateBuffer.flip();

        c4Encoder.decode(intermediateBuffer, output);
        intermediateBuffer.clear();
    }

    @Override
    public int outputBufferMinLength() {
		return selectiveEncoder.outputBufferMinLength() * 4;
	}
}