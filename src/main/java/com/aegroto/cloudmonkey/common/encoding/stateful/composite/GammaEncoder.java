package com.aegroto.cloudmonkey.common.encoding.stateful.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.compare.Comparator;
import com.aegroto.cloudmonkey.common.encoding.compare.differential.DifferentialComparator;
import com.aegroto.cloudmonkey.common.encoding.compare.differential.NativeDifferentialComparator;
import com.aegroto.cloudmonkey.common.encoding.compare.differential.ParallelDifferentialComparator;
import com.aegroto.cloudmonkey.common.encoding.compare.dummy.NullComparator;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.ApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.NativeApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelredundancy.ABGRChannelRedundancyEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelredundancy.RGBAChannelRedundancyEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.ApproxPixelDSEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4RunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4SelRLVHEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.MultithreadLZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.MultithreadZstdDictEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.MultithreadZstdEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.ParallelPixelLZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.RepPixelDSChRedEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.DifferentialEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.ParallelPixelDifferentialEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.NativePixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelRepPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.dummy.NullEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdDictEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder;
import com.jme3.math.FastMath;

import org.lwjgl.BufferUtils;

public class GammaEncoder implements Encoder {
    protected static final byte KEY_FRAME_CODE = 0, RELIANT_FRAME_CODE = 1;

    protected final int inputLength, width, height;

    protected Encoder preEncoder, postKeyframeEncoder, postReliantEncoder;
    protected Comparator comparator; 

    protected ByteBuffer preIntermediateBuffer, postIntermediateBuffer,
                         lastFrameBuffer;

    protected int outBufferMinLength;

    protected boolean forceKeyFrame;

    public GammaEncoder(int width, int height) {
        this.width = width;
        this.height = height;
        this.inputLength = width * height * 4;

        // preEncoder = new ApproxPixelDSEncoder(width, height);
        // preEncoder = new ParallelRepPixelDownsamplingEncoder(width, height);
        preEncoder = new NativeApproximationEncoder(inputLength);

        comparator = new NativeDifferentialComparator(preEncoder.outputBufferMinLength()); // preEncoder out size should be fixed

        postKeyframeEncoder = new ParallelPixelLZ4Encoder(preEncoder.outputBufferMinLength());
        // postReliantEncoder = new MultithreadZstdDictEncoder(comparator.outputBufferMinLength(), 4, 1, "diff");
        postReliantEncoder = new MultithreadLZ4Encoder(comparator.outputBufferMinLength(), 8);

        // postKeyframeEncoder = new ZstdEncoder(comparator.outputBufferMinLength(), 1);
        // postReliantEncoder = new ZstdDictEncoder(comparator.outputBufferMinLength(), "diff", 1);

        // postKeyframeEncoder = new LZ4Encoder(comparator.outputBufferMinLength());
        // postReliantEncoder = new LZ4Encoder(comparator.outputBufferMinLength());

        lastFrameBuffer = BufferUtils.createByteBuffer(preEncoder.outputBufferMinLength());

        outBufferMinLength = 1 + Math.max(preEncoder.outputBufferMinLength(), postKeyframeEncoder.outputBufferMinLength());

        preIntermediateBuffer = BufferUtils.createByteBuffer(preEncoder.outputBufferMinLength());
        postIntermediateBuffer = BufferUtils.createByteBuffer(postKeyframeEncoder.outputBufferMinLength());

        forceKeyFrame = true;

        System.out.println("GammaEncoder preEncoder class: " + preEncoder.getClass().getSimpleName());
        System.out.println("GammaEncoder comparator class: " + comparator.getClass().getSimpleName());
        System.out.println("GammaEncoder postKeyframeEncoder class: " + postKeyframeEncoder.getClass().getSimpleName());
        System.out.println("GammaEncoder postReliantEncoder class: " + postReliantEncoder.getClass().getSimpleName());
    }

    protected boolean shouldEncodeKeyframe(ByteBuffer currentFrame, ByteBuffer lastFrame) {
        currentFrame.mark();
        lastFrame.mark();

        long totalDiff = 0;

        long samples = 0;

        while(currentFrame.hasRemaining()) {
            totalDiff += Math.abs(currentFrame.get() - lastFrame.get());

            int jump = Math.min(currentFrame.remaining(), FastMath.nextRandomInt(20000, 100000));

            currentFrame.position(currentFrame.position() + jump);
            lastFrame.position(lastFrame.position() + jump);

            ++samples;
        }

        currentFrame.reset();
        lastFrame.reset();

        long meanDiff = totalDiff / samples;

        boolean condition = meanDiff > 5;

        return condition;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        // long totalStartTime = System.currentTimeMillis();

        int outLength;

        // long preStartTime = System.currentTimeMillis();
        preEncoder.encode(input, preIntermediateBuffer);
        // long preEndTime = System.currentTimeMillis();

        preIntermediateBuffer.flip();

        // long searchStartTime = System.currentTimeMillis();
        boolean keyFrame = forceKeyFrame ? true : shouldEncodeKeyframe(preIntermediateBuffer, lastFrameBuffer);
        // long searchEndTime = System.currentTimeMillis();

        if(forceKeyFrame)
            forceKeyFrame = false;

        // long comparatorStartTime = 0, comparatorEndTime = 0, postStartTime, postEndTime;

        if(keyFrame) {
            System.out.println("[Encoder] Key frame");

            output.put(KEY_FRAME_CODE);

            // postStartTime = System.currentTimeMillis();
            // Key frame
            outLength = postKeyframeEncoder.encode(preIntermediateBuffer, output);

            // postEndTime = System.currentTimeMillis();

            lastFrameBuffer.clear();
        } else {
            // System.out.println("[Encoder] Reliant frame");

            output.put(RELIANT_FRAME_CODE);

            // Reliant frame
            // comparatorStartTime = System.currentTimeMillis();
            comparator.encode(lastFrameBuffer, preIntermediateBuffer, postIntermediateBuffer);
            // comparatorEndTime = System.currentTimeMillis();

            postIntermediateBuffer.flip();

            // postStartTime = System.currentTimeMillis();
            outLength = postReliantEncoder.encode(postIntermediateBuffer, output);
            // postEndTime = System.currentTimeMillis();

            postIntermediateBuffer.clear();
            lastFrameBuffer.clear();
        }

        preIntermediateBuffer.flip();
        lastFrameBuffer.put(preIntermediateBuffer);
        lastFrameBuffer.flip();
        preIntermediateBuffer.clear();

        // long totalEndTime = System.currentTimeMillis();

        /*System.out.printf("Times: %d %d %d %d = %d\n", 
            (preEndTime - preStartTime),
            (searchEndTime - searchStartTime),
            (comparatorEndTime - comparatorStartTime),
            (postEndTime - postStartTime),
            (totalEndTime - totalStartTime)
        );*/

        return outLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        boolean keyFrame = input.get() == KEY_FRAME_CODE;

        if(keyFrame) {
            System.out.println("[Decoder] Key frame");

            // Key frame
            postKeyframeEncoder.decode(input, postIntermediateBuffer);
            postIntermediateBuffer.flip();
            
            preEncoder.decode(postIntermediateBuffer, output);

            lastFrameBuffer.clear();

            postIntermediateBuffer.flip();
            lastFrameBuffer.put(postIntermediateBuffer);
            lastFrameBuffer.flip();
            postIntermediateBuffer.clear();
        } else {
            // System.out.println("[Decoder] Reliant frame");

            // Reliant frame
            postReliantEncoder.decode(input, postIntermediateBuffer);
            postIntermediateBuffer.flip();

            // System.out.println(lastFrameBuffer.remaining() + " " + postIntermediateBuffer.remaining() + " " + preIntermediateBuffer.remaining());

            comparator.decode(lastFrameBuffer, postIntermediateBuffer, preIntermediateBuffer);
            preIntermediateBuffer.flip();

            preEncoder.decode(preIntermediateBuffer, output);

            postIntermediateBuffer.clear();
            lastFrameBuffer.clear();

            preIntermediateBuffer.flip();
            lastFrameBuffer.put(preIntermediateBuffer);
            lastFrameBuffer.flip();
            preIntermediateBuffer.clear();
        }
    }

    @Override
    public int outputBufferMinLength() {
        return outBufferMinLength;
    }

    public void reset() {
        forceKeyFrame = true;
    }
}