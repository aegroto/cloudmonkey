package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelizer.Channelizer4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.DownsamplingEncoder;
import com.jme3.util.BufferUtils;

public class C4DownsamplingEncoder implements Encoder {
    protected final int width, height; 

    protected final Channelizer4Encoder c4Encoder;
    protected final DownsamplingEncoder downsamplingEncoder;

    protected ByteBuffer intermediateBuffer;

    public C4DownsamplingEncoder(int width, int height) {
        this.width = width;
        this.height = height;

        c4Encoder = new Channelizer4Encoder(width * height * 4);
        downsamplingEncoder = new DownsamplingEncoder(width, height);
        
        intermediateBuffer = BufferUtils.createByteBuffer(c4Encoder.outputBufferMinLength());
    }
    
    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        c4Encoder.encode(input, intermediateBuffer);
        intermediateBuffer.position(0);

        ByteBuffer channelBuffer = BufferUtils.createByteBuffer(width * height).order(getByteOrder());

        int outputLength = 0;

        for(int i = 0; i < 4; ++i) {
            intermediateBuffer.limit((i + 1) * (width * height));
            // System.out.printf("Intermediate buffer #%d stats: %d %d\n", i, intermediateBuffer.position(), intermediateBuffer.limit());
            channelBuffer.put(intermediateBuffer);
            channelBuffer.flip();

            // System.out.printf("Intermediate buffer stats: %d %d\n", intermediateBuffer.position(), intermediateBuffer.limit());
           
            /*System.out.print("Channel buffer: ");
            while(channelBuffer.hasRemaining()) {
                System.out.print(channelBuffer.get() + " ");
            }
            System.out.print("\n");
            channelBuffer.flip();*/

            // System.out.printf("Intermediate buffer stats: %d %d\n", intermediateBuffer.position(), intermediateBuffer.limit());

            /*int jumps = 0;
            byte currentValue = channelBuffer.get();
            while(channelBuffer.hasRemaining()) {
                byte newValue = channelBuffer.get();

                if(newValue != currentValue) {
                    currentValue = newValue;
                    ++jumps;
                }                    
            }
            channelBuffer.flip();
            System.out.println("Jumps: " + jumps + "/" + inputLength / 4);*/

            outputLength += downsamplingEncoder.encode(channelBuffer, output);

            /*int outputPos = output.position();
            int outputLimit = output.limit();
            output.flip();
            System.out.print("Output buffer: ");
            while(output.hasRemaining()) {
                System.out.print(output.get() + " ");
            }
            System.out.print("\n");
            output.position(outputPos);
            output.limit(outputLimit);*/

            channelBuffer.clear();
        }

        /*int bufPos = intermediateBuffer.position();
        intermediateBuffer.position(0);
        System.out.print("Intermediate buffer: ");
        while(intermediateBuffer.hasRemaining()) {
            System.out.print(intermediateBuffer.get() + " ");
        }
        System.out.print("\n\n");
        intermediateBuffer.position(bufPos);*/

        intermediateBuffer.clear();

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        ByteBuffer channelBuffer = BufferUtils.createByteBuffer(downsamplingEncoder.outputBufferMinLength()).order(getByteOrder());

        int inputMaxLimit = input.limit();

        for(int i = 0; i < 4; ++i) {
            input.limit(inputMaxLimit);
           
            /*int inputPos = input.position();
            System.out.print("\nRemaining input: ");
            while(input.hasRemaining()) {
                System.out.print(input.get() + " ");
            }
            System.out.print("\n");
            input.position(inputPos);*/

            // System.out.printf("\nInput buffer stats: %d %d\n", input.position(), input.limit());

            // System.out.printf("Encoded channel %d length: %d\n", i, encodedChannelLength);

            input.limit(input.position() + width * 2 + downsamplingEncoder.getValuesPerDsRow() *(height - 2));
            channelBuffer.put(input);
            channelBuffer.flip();

            /*System.out.print("Channel buffer: ");
            while(channelBuffer.hasRemaining()) {
                System.out.print(channelBuffer.get() + " ");
            }
            System.out.print("\n");
            channelBuffer.flip();*/

            // System.out.printf("Channel buffer stats: %d %d\n", channelBuffer.position(), channelBuffer.limit());

            // System.out.printf("Decoding... (%d)\n", intermediateBuffer.remaining());

            downsamplingEncoder.decode(channelBuffer, intermediateBuffer);

            /*int bufPos = intermediateBuffer.position();
            intermediateBuffer.position(0);
            System.out.print("Intermediate buffer: ");
            while(intermediateBuffer.hasRemaining()) {
                System.out.print(intermediateBuffer.get() + " ");
            }
            System.out.print("\n");
            intermediateBuffer.position(bufPos);*/

            channelBuffer.clear();
        }

        intermediateBuffer.flip();

        c4Encoder.decode(intermediateBuffer, output);
        intermediateBuffer.clear();
    }

    @Override
    public int outputBufferMinLength() {
		return downsamplingEncoder.outputBufferMinLength() * 4;
	}

}