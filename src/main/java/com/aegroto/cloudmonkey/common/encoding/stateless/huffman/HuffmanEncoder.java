package com.aegroto.cloudmonkey.common.encoding.stateless.huffman;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aegroto.cloudmonkey.common.Helpers;
import com.aegroto.cloudmonkey.common.encoding.Encoder;

import lombok.AllArgsConstructor;

/**
 * WARNING: Can't be called on strings which belong to one-char alphabets.
 */

public class HuffmanEncoder implements Encoder {
    protected final int inputLength;

    public HuffmanEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    protected class HuffmanNode implements Comparable<HuffmanNode> {
        final byte value;
        final int frequency;
        final short right, left;
        short index;
        
        HuffmanNode(byte value, int frequency, short right, short left) {
            this.value = value;
            this.frequency = frequency;
            this.right = right;
            this.left = left;
        }

        @Override
        public int compareTo(HuffmanNode o) {
            return frequency - o.frequency;
        }

        public boolean isLeaf() {
            return (right == left) && (left == -1);
        }

        @Override
        public String toString() {
            return "{" + value + ", " + frequency + ", " + right + ", " + left + "}";
        }
    }

    @AllArgsConstructor
    protected class CodeEntry {
        final int code;
        final byte length;

        /*public String toString() {
            return "[" + Helpers.byteToBinaryString(code) + ", " + length + "]";
        }*/
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outputOffset = output.position();

        int[] frequencies = new int[256];

        // collect frequencies
        while(input.hasRemaining()) { 
            frequencies[input.get() & 0xFF]++;
        }

        /*System.out.println("Frequencies: ");
        for(int i = 0; i < 256; ++i) {
            if(frequencies[i] > 0)
                System.out.println("\t" + i + " = " + frequencies[i]);
        }*/

        // create nodes queue
        PriorityQueue<HuffmanNode> nodesHeap = new PriorityQueue<>();
        
        for(char i = 0; i < frequencies.length; ++i) {
            if(frequencies[i] > 0) {
                nodesHeap.offer(new HuffmanNode((byte) i, frequencies[i], (short) -1, (short) -1));
            }
        }

        // creating huffman tree
        int treeLength = 2 * nodesHeap.size() - 1;
        HashMap<Byte, CodeEntry> codesTable = new HashMap<>();
        HuffmanNode[] huffmanTree;

        // if(treeLength > 1) {
            huffmanTree = new HuffmanNode[treeLength]; 
            short currentTreeIndex = (short) (treeLength - 1);

            while(nodesHeap.size() > 1) {
                HuffmanNode firstNode = nodesHeap.poll();
                HuffmanNode secondNode = nodesHeap.poll();

                if(firstNode.isLeaf()) {
                    firstNode.index = currentTreeIndex;
                    huffmanTree[firstNode.index] = firstNode;
                    --currentTreeIndex;
                }

                if(secondNode.isLeaf()) {
                    secondNode.index = currentTreeIndex;
                    huffmanTree[secondNode.index] = secondNode;
                    --currentTreeIndex;
                }

                HuffmanNode fatherNode = new HuffmanNode((byte) 0,
                    firstNode.frequency + secondNode.frequency,
                    firstNode.index, secondNode.index);

                fatherNode.index = currentTreeIndex;
                huffmanTree[fatherNode.index] = fatherNode;
                --currentTreeIndex;

                nodesHeap.offer(fatherNode);
            }

            output.putInt(huffmanTree.length * 5);

            output.position(output.position() + 4);

            for(int i = 0; i < huffmanTree.length; ++i) {
                output.put(huffmanTree[i].value);
                output.putShort(huffmanTree[i].right);
                output.putShort(huffmanTree[i].left);
            }

            // creating codes tables
            HuffmanNode root = nodesHeap.poll();
            lookForCodes(codesTable, huffmanTree, root, (byte) 0b0, 0);
        /*} else {
            System.out.println("-- Single char case");

            treeLength = 0;
            HuffmanNode singleNode = nodesHeap.poll();

            codesTable.put(singleNode.value, new CodeEntry(0b1, (byte) 1));
            
            output.putInt(outputOffset, 0); // it's 0

            output.put(outputOffset + 8, singleNode.value);

            output.position(output.position() + 5);
        }*/

        /*System.out.println("Codes table: ");

        for(Entry<Byte, CodeEntry> entry : codesTable.entrySet()) {
            System.out.println(entry.getKey() + " => " + Integer.toBinaryString(entry.getValue().code) + " (" + entry.getValue().length + ")");
        }*/

        // adding encoded string
        // int currentBitOffset = (currentOutputIndex) * 8;
        int outputLength = 0;
        int remainingChunkBits = 8;
        int chunk = 0;

        input.flip();

        while(input.hasRemaining()) {
            if(remainingChunkBits > 0) {
                byte b = input.get();
                CodeEntry codeEntry = codesTable.get(b);

                /* System.out.println("Encoding " + b + " (" + Integer.toBinaryString(codeEntry.code) + ", " + codeEntry.length + "), chunk is " 
                                    + Helpers.byteToBinaryString((byte) (chunk >>> 24)) 
                                    + " (" + remainingChunkBits + ")"); */

                chunk = (int) (chunk | ((((codeEntry.code << (32 - codeEntry.length))) >>> (8 - remainingChunkBits))));
                remainingChunkBits -= codeEntry.length;
            } else {
                // System.out.println("Pushed chunk " + Integer.toBinaryString(chunk) + ", remaining chunk bits are " + remainingChunkBits);
                // System.out.println(Helpers.byteToBinaryString((byte) ((chunk >>> 24) & 0xFF)));

                output.put((byte) ((chunk >>> 24) & 0xFF));
                chunk = (int) (chunk << 8);
                remainingChunkBits = 8 + remainingChunkBits;

                outputLength += 8;                
            }
        }

        if(remainingChunkBits < 8) {
            // System.out.println("Pushing last chunk " + Integer.toBinaryString(chunk) + "("  + remainingChunkBits + ")");

            // output.putInt(chunk);
            // outputLength += 8 - remainingChunkBits;

            outputLength += 8 - remainingChunkBits;

            while(remainingChunkBits < 8) {
                output.put((byte) ((chunk >>> 24) & 0xFF));
                chunk = (int) (chunk << 8);
                remainingChunkBits = 8 + remainingChunkBits;
            }
        }

        /*System.out.print("Encoded string: ");
        output.flip();
        while(output.hasRemaining()) {
            System.out.print(Helpers.byteToBinaryString(output.get()) + " ");
        }*/

        /*for(byte b : input) {
            CodeEntry codeEntry = codesTable.get(b);
            Helpers.putBitsInByteArray(output, codeEntry.code, codeEntry.length, currentBitOffset + outputLength);
            outputLength += codeEntry.length;
        }*/

        // Helpers.putIntInByteArray(output, outputLength, offset + 4);

        output.putInt(outputOffset + 4, outputLength);

        // System.out.println("Encode huffman tree size: " + output.getInt(outputOffset));
        // System.out.println("Encode input string size: " + output.getInt(outputOffset + 4));

        return 8 + treeLength * 5 + (outputLength / 8 + 1);
    }

    protected void lookForCodes(Map<Byte, CodeEntry> table, HuffmanNode[] tree, HuffmanNode root, int currentCode, int currentLength) {
        if(root.isLeaf()) {
            table.put(root.value, new CodeEntry(currentCode, (byte) currentLength));
        } else {
            lookForCodes(table, tree, tree[root.left], (currentCode << 1), currentLength + 1);
            lookForCodes(table, tree, tree[root.right], ((currentCode << 1) | 0b1), currentLength + 1);
        }
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        // int huffmanTreeSize = Helpers.getIntFromByteArray(input, offset);
        // int inputStringLength = Helpers.getIntFromByteArray(input, offset + 4);
        int huffmanTreeSize = input.getInt(); 
        int inputStringLength = input.getInt(); 

        /*System.out.println("Total input length (bytes): " + input.limit()); 
        System.out.println("Decode huffman tree size: " + huffmanTreeSize);
        System.out.println("Decode Input string length: " + inputStringLength);*/

        if(inputStringLength > 0) {
            /*input.flip();
            System.out.print("Total input string: ");
            while(input.hasRemaining()) {
                System.out.print(input.get() + " ");
            }
            System.out.print("\n");
            input.position(8);*/

            // retrieve sizes

            // int currentInputIndex = offset + 8;

            // if(huffmanTreeSize > 0) {
            // retrieve huffman tree
                HuffmanNode[] huffmanTree = new HuffmanNode[huffmanTreeSize / 5];

                // System.out.println("Huffman tree length: " + huffmanTree.length);

                for(int i = 0; i < huffmanTree.length; ++i) {
                    // System.out.println("Current input index: " + currentInputIndex);
                    // byte value = input[currentInputIndex];
                    // short right = Helpers.getShortFromByteArray(input, currentInputIndex + 1);
                    // short left = Helpers.getShortFromByteArray(input, currentInputIndex + 3);
                    byte value = input.get();
                    short right = input.getShort();
                    short left = input.getShort();

                    huffmanTree[i] = new HuffmanNode(value, 0, right, left);
                }

                /*System.out.print("Input string: ");
                StringBuilder inputString = new StringBuilder();
                int prePos = input.position();
                while(input.hasRemaining()) {
                    inputString.append(Helpers.byteToBinaryString(input.get()) + " ");
                }
                System.out.println(inputString.toString());
                input.position(prePos);*/

                // decode string
                HuffmanNode node = huffmanTree[0];
                int decodedBits = 0;
                // int decodedWords = 0;
                int wordBitCounter = 1, currentBitIndex = 0;
                byte currentWord = input.get();

                while(decodedBits < inputStringLength) {
                    // byte currentWord = input[currentInputIndex];
                    int currentBit = (currentWord >>> (7 - currentBitIndex)) & 1;

                    // System.out.println("Current word: " + Helpers.byteToBinaryString(currentWord));
                    // System.out.println("Current bit: " + currentBit + " (" + currentBitIndex + ", " + wordBitCounter + ")");

                    if(currentBit == 0) {
                        node = huffmanTree[node.left];
                    } else {
                        node = huffmanTree[node.right];
                    }

                    if(node.isLeaf()) {
                        // output[decodedWords] = node.value;
                        if(output.hasRemaining()) {
                            output.put(node.value);
                        } else { 
                            break;
                        }

                        decodedBits += wordBitCounter;
                        // currentInputIndex += wordBitCounter / 8;
                        wordBitCounter = 1;

                        // System.out.println("Found leaf, value is " + node.value + " (" + decodedBits + "/" + inputStringLength + ")");

                        // ++decodedWords;
                        node = huffmanTree[0];
                    } else {
                        ++wordBitCounter;
                    }

                    ++currentBitIndex;
                    
                    if(currentBitIndex == 8 && input.hasRemaining()) {
                        currentBitIndex = 0;
                        currentWord = input.get();
                        // System.out.println("Next word: " + Helpers.byteToBinaryString(currentWord));
                    }
                }
            /*} else {                
                int decodedBits = 0;
                byte singleValue = input.get(8);

                System.out.println("Single case, length: " + inputStringLength);

                while(decodedBits < inputStringLength) {
                    // System.out.printf("Output stats: %d %d\n", output.position(), output.limit());
                    // System.out.printf("Decoded: %d/%d\n", decodedBits, inputStringLength);
                    output.put(singleValue);
                    decodedBits += 8;
                }
            }*/
        }
    }

    @Override
    public int outputBufferMinLength() {
        return 8 + ((256 * 2 - 1) * 5) + inputLength + 4;
    }
}