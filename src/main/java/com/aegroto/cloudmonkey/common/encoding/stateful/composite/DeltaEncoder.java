package com.aegroto.cloudmonkey.common.encoding.stateful.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.compare.Comparator;
import com.aegroto.cloudmonkey.common.encoding.compare.differential.ParallelDifferentialComparator;
import com.aegroto.cloudmonkey.common.encoding.stateless.approximation.NativeApproximationEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.ApproxPixelDSEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.MultithreadZstdDictEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.MultithreadZstdEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelRepPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdDictEncoder;
import com.jme3.math.FastMath;

import org.lwjgl.BufferUtils;

public class DeltaEncoder implements Encoder {
    protected final int inputLength, width, height;

    protected Encoder preEncoder, postKeyframeEncoder;
    protected Encoder[] postReliantEncoders;
    protected Comparator comparator; 

    protected ByteBuffer preIntermediateBuffer, postIntermediateBuffer,
                         lastFrameBuffer;

    protected int outBufferMinLength;

    protected boolean forceKeyFrame;

    protected long encodersThresholds[];

    public DeltaEncoder(int width, int height) {
        this.width = width;
        this.height = height;
        this.inputLength = width * height * 4;

        // Pre encoder
        // preEncoder = new ApproxPixelDSEncoder(width, height);
        // preEncoder = new ParallelRepPixelDownsamplingEncoder(width, height);
        preEncoder = new NativeApproximationEncoder(inputLength);

        // Comparator
        comparator = new ParallelDifferentialComparator(preEncoder.outputBufferMinLength()); // preEncoder out size should be fixed

        // Post keyframe encoder
        postKeyframeEncoder = new MultithreadZstdEncoder(comparator.outputBufferMinLength(), 4, 1);

        // Post reliant encoders
        postReliantEncoders = new Encoder[2];

        postReliantEncoders[0] = new LZ4Encoder(comparator.outputBufferMinLength());
        postReliantEncoders[1] = new ZstdDictEncoder(comparator.outputBufferMinLength(), "diff", 1);
        // postReliantEncoders[1] = new MultithreadZstdEncoder(comparator.outputBufferMinLength(), 4, 1);

        encodersThresholds = new long[postReliantEncoders.length];

        encodersThresholds[0] = 1;
        encodersThresholds[1] = 5;

        lastFrameBuffer = BufferUtils.createByteBuffer(preEncoder.outputBufferMinLength());

        outBufferMinLength = 1 + Math.max(preEncoder.outputBufferMinLength(), postKeyframeEncoder.outputBufferMinLength());

        preIntermediateBuffer = BufferUtils.createByteBuffer(preEncoder.outputBufferMinLength());
        postIntermediateBuffer = BufferUtils.createByteBuffer(postKeyframeEncoder.outputBufferMinLength());

        forceKeyFrame = true;
    }

    protected byte selectEncoder(ByteBuffer currentFrame, ByteBuffer lastFrame) {
        currentFrame.mark();
        lastFrame.mark();

        long totalDiff = 0;

        long samples = 0;

        while(currentFrame.hasRemaining()) {
            totalDiff += Math.abs(currentFrame.get() - lastFrame.get());

            int jump = Math.min(currentFrame.remaining(), FastMath.nextRandomInt(20000, 100000));

            currentFrame.position(currentFrame.position() + jump);
            lastFrame.position(lastFrame.position() + jump);

            ++samples;
        }

        currentFrame.reset();
        lastFrame.reset();

        long meanDiff = totalDiff / samples;

        // System.out.println("Mean diff: " + meanDiff);

        for(int t = 0; t < encodersThresholds.length; ++t) {
            if(meanDiff < encodersThresholds[t]) {
                return (byte) t;
            }
        }
        
        return -1;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outLength;

        preEncoder.encode(input, preIntermediateBuffer);

        preIntermediateBuffer.flip();

        byte encoderId = selectEncoder(preIntermediateBuffer, lastFrameBuffer);
        boolean keyFrame = forceKeyFrame ? true : encoderId == -1;

        if(forceKeyFrame)
            forceKeyFrame = false;

        output.put(encoderId);

        if(keyFrame) {
            // System.out.println("[Encoder] Key frame");

            // Key frame
            outLength = postKeyframeEncoder.encode(preIntermediateBuffer, output);

            lastFrameBuffer.clear();
        } else {
            // System.out.println("[Encoder] Reliant frame (" + encoderId + ")");

            // Reliant frame
            comparator.encode(lastFrameBuffer, preIntermediateBuffer, postIntermediateBuffer);

            postIntermediateBuffer.flip();

            outLength = postReliantEncoders[encoderId].encode(postIntermediateBuffer, output);

            postIntermediateBuffer.clear();
            lastFrameBuffer.clear();
        }

        preIntermediateBuffer.flip();
        lastFrameBuffer.put(preIntermediateBuffer);
        lastFrameBuffer.flip();
        preIntermediateBuffer.clear();

        return outLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        byte encoderId = input.get();

        if(encoderId == -1) {
            // System.out.println("[Decoder] Key frame");

            // Key frame
            postKeyframeEncoder.decode(input, postIntermediateBuffer);
            postIntermediateBuffer.flip();
            
            preEncoder.decode(postIntermediateBuffer, output);

            lastFrameBuffer.clear();

            postIntermediateBuffer.flip();
            lastFrameBuffer.put(postIntermediateBuffer);
            lastFrameBuffer.flip();
            postIntermediateBuffer.clear();
        } else {
            // System.out.println("[Decoder] Reliant frame");

            // Reliant frame
            postReliantEncoders[encoderId].decode(input, postIntermediateBuffer);
            postIntermediateBuffer.flip();

            // System.out.println(lastFrameBuffer.remaining() + " " + postIntermediateBuffer.remaining() + " " + preIntermediateBuffer.remaining());

            comparator.decode(lastFrameBuffer, postIntermediateBuffer, preIntermediateBuffer);
            preIntermediateBuffer.flip();

            preEncoder.decode(preIntermediateBuffer, output);

            postIntermediateBuffer.clear();
            lastFrameBuffer.clear();

            preIntermediateBuffer.flip();
            lastFrameBuffer.put(preIntermediateBuffer);
            lastFrameBuffer.flip();
            preIntermediateBuffer.clear();
        }
    }

    @Override
    public int outputBufferMinLength() {
        return outBufferMinLength;
    }

    public void reset() {
        forceKeyFrame = true;
    }
}