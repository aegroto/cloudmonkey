package com.aegroto.cloudmonkey.common.encoding.compare.differential;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.compare.Comparator;
import com.aegroto.cloudmonkey.common.opencl.OpenCLManager;
import com.jme3.opencl.Buffer;
import com.jme3.opencl.CommandQueue;
import com.jme3.opencl.Event;
import com.jme3.opencl.Kernel;
import com.jme3.opencl.Program;
import com.jme3.opencl.Kernel.WorkSize;

public class ParallelDifferentialComparator implements Comparator {
    protected final int inputLength;

    protected Kernel encodeKernel, decodeKernel;
    protected Buffer firstInputBuffer, secondInputBuffer, outputBuffer;

    protected WorkSize encodeLocalWorkSize;
    protected WorkSize encodeGlobalWorkSize;

    protected WorkSize decodeLocalWorkSize;
    protected WorkSize decodeGlobalWorkSize;

    public ParallelDifferentialComparator(int inputLength) {
        this.inputLength = inputLength;

        Program program = OpenCLManager.loadProgram("cl_src/differential_compare.ocl");
        program.build();

        encodeKernel = program.createKernel("differential_compare_encode");
        decodeKernel = program.createKernel("differential_compare_decode");

        firstInputBuffer = OpenCLManager.getContext().createBuffer(inputLength);
        secondInputBuffer = OpenCLManager.getContext().createBuffer(inputLength);

        outputBuffer = OpenCLManager.getContext().createBuffer(outputBufferMinLength());

        long encodePreferredLws = 1024;
        long encodeLws = encodePreferredLws > inputLength ? inputLength : encodePreferredLws;

        encodeLocalWorkSize = new WorkSize(encodeLws);
        encodeGlobalWorkSize = new WorkSize(((inputLength + encodeLws - 1) / encodeLws) * encodeLws);

        long decodePreferredLws = 1024;
        long decodeLws = decodePreferredLws > inputLength ? inputLength : decodePreferredLws;

        decodeLocalWorkSize = new WorkSize(decodeLws);
        decodeGlobalWorkSize = new WorkSize(((inputLength + decodeLws - 1) / decodeLws) * decodeLws);
    }

    @Override
    public int encode(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output) {
        CommandQueue queue = OpenCLManager.getCmdQueue(); 

        long startTime = System.currentTimeMillis();

        firstInputBuffer.write(queue, firstInput);
        secondInputBuffer.write(queue, secondInput);

        queue.finish();

        firstInput.position(firstInput.limit());
        secondInput.position(secondInput.limit());

        Event encodeEvent = encodeKernel.Run2(queue,
            encodeGlobalWorkSize, 
            encodeLocalWorkSize,
            firstInputBuffer, 
            secondInputBuffer, 
            outputBuffer,
            firstInputBuffer.getSize() 
        );
        encodeEvent.waitForFinished();
        long endTime = System.currentTimeMillis();

        System.out.println("Kernel time: " + (endTime - startTime));

        outputBuffer.read(queue, output, inputLength);

        queue.finish();

        output.position(output.position() + inputLength);

        return inputLength;
    }

    @Override
    public void decode(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output) {
        CommandQueue queue = OpenCLManager.getCmdQueue(); 

        firstInputBuffer.write(queue, firstInput);
        secondInputBuffer.write(queue, secondInput);

        queue.finish();

        firstInput.position(firstInput.limit());
        secondInput.position(secondInput.limit());

        Event decodeEvent = decodeKernel.Run2(queue,
            decodeGlobalWorkSize, 
            decodeLocalWorkSize,
            firstInputBuffer, 
            secondInputBuffer, 
            outputBuffer,
            firstInputBuffer.getSize() 
        );
        decodeEvent.waitForFinished();

        outputBuffer.read(queue, output, inputLength);

        queue.finish();

        output.position(output.position() + inputLength);
    }

    @Override
    public int outputBufferMinLength() {
        return inputLength;
    }
}