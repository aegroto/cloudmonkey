package com.aegroto.cloudmonkey.common.encoding.stateless.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.channelredundancy.RGBAChannelRedundancyEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelRepPixelDownsamplingEncoder;
import com.jme3.util.BufferUtils;

public class RepPixelDSChRedEncoder implements Encoder {
    protected final int width, height, inputLength;

    protected final ParallelRepPixelDownsamplingEncoder repPixelDSEncoder;
    protected final RGBAChannelRedundancyEncoder chRedEncoder;

    protected ByteBuffer intermediateBuffer;

    public RepPixelDSChRedEncoder(int width, int height) {
        this.width = width;
        this.height = height;
        this.inputLength = width * height * 4;

        repPixelDSEncoder = new ParallelRepPixelDownsamplingEncoder(width, height);
        chRedEncoder = new RGBAChannelRedundancyEncoder(repPixelDSEncoder.outputBufferMinLength() - repPixelDSEncoder.getExtraDataLength());

        intermediateBuffer = BufferUtils.createByteBuffer(repPixelDSEncoder.outputBufferMinLength());
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outputLength = repPixelDSEncoder.encode(input, intermediateBuffer);
        intermediateBuffer.flip();

        intermediateBuffer.limit(intermediateBuffer.limit() - repPixelDSEncoder.getExtraDataLength());

        chRedEncoder.encode(intermediateBuffer, output);

        intermediateBuffer.limit(intermediateBuffer.limit() + repPixelDSEncoder.getExtraDataLength());

        output.put(intermediateBuffer);

        intermediateBuffer.rewind();

        return outputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        input.limit(input.limit() - repPixelDSEncoder.getExtraDataLength());

        chRedEncoder.decode(input, intermediateBuffer);
        intermediateBuffer.put(input);
        intermediateBuffer.flip();

        repPixelDSEncoder.decode(intermediateBuffer, output);

        intermediateBuffer.rewind();
    }

    @Override
    public int outputBufferMinLength() {
		return repPixelDSEncoder.outputBufferMinLength();
	}

}