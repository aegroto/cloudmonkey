package com.aegroto.cloudmonkey.common.encoding.stateless.differential;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.opencl.OpenCLManager;
import com.jme3.opencl.Buffer;
import com.jme3.opencl.CommandQueue;
import com.jme3.opencl.Event;
import com.jme3.opencl.Kernel;
import com.jme3.opencl.Program;
import com.jme3.opencl.Kernel.WorkSize;

public class ParallelPixelDifferentialEncoder implements Encoder {
    protected final int inputLength;

    protected Kernel encodeKernel, decodeKernel;
    protected Buffer inputBuffer, outputBuffer;

    protected WorkSize encodeLocalWorkSize;
    protected WorkSize encodeGlobalWorkSize;

    protected WorkSize decodeLocalWorkSize;
    protected WorkSize decodeGlobalWorkSize;

    public ParallelPixelDifferentialEncoder(int inputLength) {
        this.inputLength = inputLength;

        Program program = OpenCLManager.loadProgram("cl_src/differential.ocl");
        program.build();

        encodeKernel = program.createKernel("differential_encode");
        decodeKernel = program.createKernel("differential_decode");

        inputBuffer = OpenCLManager.getContext().createBuffer(inputLength);
        outputBuffer = OpenCLManager.getContext().createBuffer(outputBufferMinLength());

        long encodePreferredLws = 256;
        long encodeLws = encodePreferredLws > inputLength ? inputLength : encodePreferredLws;

        encodeLocalWorkSize = new WorkSize(encodeLws);
        encodeGlobalWorkSize = new WorkSize(((inputLength + encodeLws - 1) / encodeLws) * encodeLws);

        long decodePreferredLws = 1024;
        long decodeLws = decodePreferredLws > inputLength ? inputLength : decodePreferredLws;

        decodeLocalWorkSize = new WorkSize(decodeLws);
        decodeGlobalWorkSize = new WorkSize(((inputLength + decodeLws - 1) / decodeLws) * decodeLws);
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        CommandQueue queue = OpenCLManager.getCmdQueue(); 
        inputBuffer.write(queue, input);

        Event encodeEvent = encodeKernel.Run2(queue,
            encodeGlobalWorkSize, 
            encodeLocalWorkSize,
            inputBuffer, 
            outputBuffer,
            inputBuffer.getSize() 
        );
        encodeEvent.waitForFinished();

        outputBuffer.read(queue, output, inputLength);

        input.position(input.limit());
        output.position(output.position() + inputLength);

        return inputLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        CommandQueue queue = OpenCLManager.getCmdQueue(); 

        inputBuffer.write(queue, input);

        Event decodeEvent = decodeKernel.Run2(queue,
            decodeGlobalWorkSize, 
            decodeLocalWorkSize,
            inputBuffer, 
            outputBuffer,
            new Kernel.LocalMem((int) (decodeLocalWorkSize.getSizes()[0] * 4)),
            inputBuffer.getSize() 
        );
        decodeEvent.waitForFinished();

        input.position(input.limit());
        outputBuffer.read(queue, output, inputLength);

        output.position(output.position() + inputLength);
    }

    @Override
    public int outputBufferMinLength() {
		return inputLength;
	}
}