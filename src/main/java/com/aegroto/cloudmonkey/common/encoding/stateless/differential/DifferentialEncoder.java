package com.aegroto.cloudmonkey.common.encoding.stateless.differential;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

public class DifferentialEncoder implements Encoder {
    protected final int inputLength;

    public DifferentialEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        // long startTime = System.currentTimeMillis();

        byte lastByte, inputByte, diffByte;

        lastByte = input.get();
       
        output.put(lastByte);

        while(input.hasRemaining()) {
            inputByte = input.get();

            diffByte = (byte) (inputByte - lastByte);

            output.put(diffByte);

            lastByte = inputByte;
        }

        // long endTime = System.currentTimeMillis();

        // System.out.println("Sequential delta exection time: " + (endTime - startTime));

        return input.capacity();
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        byte decodedByte;
        
        decodedByte = input.get();

        output.put(decodedByte);

        while(input.hasRemaining()) {
            decodedByte = (byte) ((decodedByte) + (input.get() & 0xFF));
            
            output.put(decodedByte);
        }
    }

    @Override
    public int outputBufferMinLength() {
		return inputLength;
	}

}