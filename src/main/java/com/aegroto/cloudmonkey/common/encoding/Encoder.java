package com.aegroto.cloudmonkey.common.encoding;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public interface Encoder {
    public abstract int encode(ByteBuffer input, ByteBuffer output);
    public abstract void decode(ByteBuffer input, ByteBuffer output);
    public abstract int outputBufferMinLength();

    default ByteOrder getByteOrder() { return ByteOrder.BIG_ENDIAN; }
    default void reset() { }
}