package com.aegroto.cloudmonkey.common.encoding.stateless.downsampling;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

import lombok.Getter;

public class DownsamplingEncoder implements Encoder {
    protected final int width, height;

    @Getter protected final int valuesPerDsRow;

    public DownsamplingEncoder(int width, int height) {
        this.width = width;
        this.height = height;
        this.valuesPerDsRow = (2 + (width-2)/2);
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        byte[] inputArr = new byte[input.remaining()];
        input.get(inputArr);

        // Output first row (not downsampled)
        for(int c = 0; c < width; ++c) {
            output.put(inputArr[c]);
        }

        // Output middle rows
        for(int r = 1; r < height - 1; ++r) {
            output.put(inputArr[r * width]);

            // for(int c = 1 + r % 2; c < width - 1; c += 2) {
            for(int c = 1; c < width - 1; ++c) {
                if(c % 2 == r % 2) {
                    output.put(inputArr[r * width + c]);
                }
            }

            output.put(inputArr[r * width + width - 1]);
        }
    
        // Output last row (not downsampled)
        for(int c = 0; c < width; ++c) {
            output.put(inputArr[(height-1) * width + c]);
        }

        return width * 2 + valuesPerDsRow * (height - 2);
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        byte[] inputArr = new byte[input.remaining()];
        input.get(inputArr);

        // Output first row (not downsampled)
        for(int c = 0; c < width; ++c) {
            output.put(inputArr[c]);
        }

        // Output middle rows
        for(int r = 1; r < height - 1; ++r) {
            int prevRowBaseIndex = r > 1 ? width + (r-2) * valuesPerDsRow : 0;
            int rowBaseIndex = width + (r-1) * valuesPerDsRow;
            int nextRowBaseIndex = width + (r) * valuesPerDsRow;

            output.put(inputArr[rowBaseIndex]);

            int last_row_index = r % 2;

            for(int c = 1; c < width - 1; ++c) {
                if(c % 2 == r % 2) {
                    int value = 0;

                    value = value + (inputArr[prevRowBaseIndex + c/2] & 0xFF);
                    value = value + (inputArr[rowBaseIndex + c/2 - 1] & 0xFF);
                    value = value + (inputArr[nextRowBaseIndex + c/2] & 0xFF);
                    value = value + (inputArr[rowBaseIndex + c/2] & 0xFF);

                    value = value / 4;

                    /*System.out.printf("Approximated [%d, %d] = %d from [%d, %d] = %d, [%d, %d] = %d, [%d, %d] = %d, [%d, %d] = %d\n",
                        r, c, value,
                        r-1, c, inputArr[prevRowBaseIndex + c - r % 2] & 0xFF,
                        r, c - 1, inputArr[rowBaseIndex + c - r % 2] & 0xFF,
                        r+1, c, inputArr[nextRowBaseIndex + c] & 0xFF,
                        r, c, inputArr[rowBaseIndex + c] & 0xFF
                    );*/

                    output.put((byte) value);
                } else {
                    output.put(inputArr[rowBaseIndex + last_row_index]);
                    // output.put((byte) 255);
                    ++last_row_index;
                }
            }

            output.put(inputArr[rowBaseIndex + valuesPerDsRow - 1]);
        }
    
        // Output last row (not downsampled)
        for(int c = 0; c < width; ++c) {
            output.put(inputArr[(inputArr.length - width) + c]);
        }
    }

    protected byte interp(byte first, byte second) {
        return (byte) ((first + second) / 2);
    }

    @Override
    public int outputBufferMinLength() {
		return width*2 + (width*height)/2 + 1;
	}

}

