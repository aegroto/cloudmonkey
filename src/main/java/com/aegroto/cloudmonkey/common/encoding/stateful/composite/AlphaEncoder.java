package com.aegroto.cloudmonkey.common.encoding.stateful.composite;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.compare.Comparator;
import com.aegroto.cloudmonkey.common.encoding.compare.differential.DifferentialComparator;
import com.aegroto.cloudmonkey.common.encoding.compare.dummy.NullComparator;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4AvgRunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4RunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4SelRLVHEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.PixelDSApproxZstdEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.PixelDSZstdEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.SelectiveRLVHEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.DifferentialEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.NativePixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelPixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.dummy.NullEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.RunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder;

import org.lwjgl.BufferUtils;

public class AlphaEncoder implements Encoder {
    protected final int inputLength, width, height;

    protected Encoder keyframeEncoder, reliantEncoder;
    // protected PixelDSZstdEncoder encoder;
    protected Comparator comparator; 

    protected final int lineLength = 4;
    protected int currentEncodeFrame, currentDecodeFrame;

    protected ByteBuffer intermediateBuffer, lastFrameBuffer;

    protected int outBufferMinLength;

    public AlphaEncoder(int width, int height) {
        this.width = width;
        this.height = height;
        this.inputLength = width * height * 4;

        keyframeEncoder = new ParallelPixelDownsamplingEncoder(width, height);
        reliantEncoder = new ZstdEncoder(inputLength);

        comparator = new DifferentialComparator(inputLength);

        lastFrameBuffer = BufferUtils.createByteBuffer(inputLength);

        int intermediateBufferMinLength = 0;

        if(keyframeEncoder.outputBufferMinLength() > intermediateBufferMinLength)
            intermediateBufferMinLength = keyframeEncoder.outputBufferMinLength();
        
        if(reliantEncoder.outputBufferMinLength() > intermediateBufferMinLength)
            intermediateBufferMinLength = reliantEncoder.outputBufferMinLength();

        outBufferMinLength = intermediateBufferMinLength;

        if(comparator.outputBufferMinLength() > intermediateBufferMinLength)
            intermediateBufferMinLength = comparator.outputBufferMinLength();

        intermediateBuffer = BufferUtils.createByteBuffer(intermediateBufferMinLength);

        currentEncodeFrame = lineLength;
        currentDecodeFrame = lineLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outLength;

        if(currentEncodeFrame >= lineLength) {
            // System.out.println("[Encoder] Key frame");

            // Key frame
            outLength = keyframeEncoder.encode(input, output);

            currentEncodeFrame = 1;
        } else {
            // System.out.println("[Encoder] Reliant frame");

            // Reliant frame
            comparator.encode(lastFrameBuffer, input, intermediateBuffer);
            intermediateBuffer.flip();

            outLength = reliantEncoder.encode(intermediateBuffer, output);

            intermediateBuffer.clear();
            lastFrameBuffer.clear();

            ++currentEncodeFrame;
        }

        input.flip();
        lastFrameBuffer.put(input);
        lastFrameBuffer.flip();

        return outLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        if(currentDecodeFrame >= lineLength) {
            // System.out.println("[Decoder] Key frame");

            // Key frame
            keyframeEncoder.decode(input, output);

            currentDecodeFrame = 1;
        } else {
            // System.out.println("[Decoder] Reliant frame");

            // Reliant frame
            reliantEncoder.decode(input, intermediateBuffer);
            intermediateBuffer.flip();

            comparator.decode(lastFrameBuffer, intermediateBuffer, output);

            intermediateBuffer.clear();
            lastFrameBuffer.clear();

            ++currentDecodeFrame;
        }

        output.flip();
        lastFrameBuffer.put(output);
        lastFrameBuffer.flip();
    }

    @Override
    public int outputBufferMinLength() {
        return outBufferMinLength;
    }

    public void reset() {
        currentEncodeFrame = lineLength;
        currentDecodeFrame = lineLength;
    }
}