package com.aegroto.cloudmonkey.common.encoding.stateless.approximation;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

public class ApproximationEncoder implements Encoder {
    protected final int inputLength;
    protected static final int valuesPerLevel = 8;

    public ApproximationEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int frequencies[] = new int[256];
        byte codes[] = new byte[256 / valuesPerLevel];
        
        while(input.hasRemaining()) {
            ++frequencies[input.get() & 0xFF];
        }

        /*System.out.println("Frequencies:");
        for(int i = 0; i < frequencies.length; ++i) {
            if(frequencies[i] > 0)
                System.out.println((byte) i + " = " + frequencies[i]);
        }*/

        for(int i = 0; i < frequencies.length; i += valuesPerLevel) {
            // find most frequent code
            byte mostFrequentCode = (byte) i;
            for(int j = 1; j < valuesPerLevel; ++j) {
                if(frequencies[i + j] > frequencies[mostFrequentCode & 0xFF]) {
                    mostFrequentCode = (byte) ((i + j) & 0xFF);
                }
            }

            codes[(i & 0xFF) / valuesPerLevel] = mostFrequentCode;

            // System.out.println("Most frequent code for range [" + i + ", " + (i + valuesPerLevel) + "] is " + mostFrequentCode);
        }

        input.rewind();
        while(input.hasRemaining()) {
            output.put(codes[(input.get() & 0xFF) / valuesPerLevel]);
        }

        return input.capacity();
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        /** That's a bulk decode method and should never be called */
        output.put(input);
    }

    @Override
    public int outputBufferMinLength() {
        return inputLength;
    }
}