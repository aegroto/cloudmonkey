package com.aegroto.cloudmonkey.common.encoding.compare.differential;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.compare.Comparator;
import com.jme3.system.NativeLibraryLoader;
import com.jme3.system.Platform;

public class NativeDifferentialComparator implements Comparator {
    static {
        NativeLibraryLoader.registerNativeLibrary("NativeClasses", Platform.Linux64, "nativeclasses.so");        
        
        NativeLibraryLoader.loadNativeLibrary("NativeClasses", true);
    }

    protected final int inputLength;

    public NativeDifferentialComparator(int inputLength) {
        this.inputLength = inputLength;
    }

    public native int encodeNative(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output, int firstInputOffset, int secondInputOffset, int outputOffset, int inputLength); 
    public native void decodeNative(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output, int firstInputOffset, int secondInputOffset, int outputOffset, int encodedInputLength);

    @Override
    public int encode(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output) {
        int outLength = encodeNative(firstInput, secondInput, output, firstInput.position(), secondInput.position(), output.position(), inputLength);

        firstInput.position(firstInput.position() + inputLength);
        secondInput.position(secondInput.position() + inputLength);
        output.position(output.position() + outLength);

        return outLength;
    }

    @Override
    public void decode(ByteBuffer firstInput, ByteBuffer secondInput, ByteBuffer output) {
        decodeNative(firstInput, secondInput, output, firstInput.position(), secondInput.position(), output.position(), inputLength);

        firstInput.position(firstInput.position() + inputLength);
        secondInput.position(secondInput.position() + inputLength);
        output.position(output.position() + inputLength);
    }

    @Override
    public int outputBufferMinLength() {
        return inputLength;
    }
}