package com.aegroto.cloudmonkey.common.encoding.stateless.dummy;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

public class NullEncoder implements Encoder {

    protected final int inputLength;

    public NullEncoder(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int inLength = input.remaining();

        output.put(input);
        return inLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        output.put(input);
    }

    @Override
    public int outputBufferMinLength() {
		return inputLength;
	}

}

