package com.aegroto.cloudmonkey.common.encoding.stateless.block;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

public class BlockEncoder implements Encoder {
    private final int bytesPerBlock;

    public BlockEncoder(int bytesPerBlock) {
        this.bytesPerBlock = bytesPerBlock;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        /*int currentByte = 0;

        while(currentByte < bytesPerBlock && input.hasRemaining()) {
            output.put(input.get());
            ++currentByte;
        }*/

        int bytesToCopy = input.remaining() < bytesPerBlock ? input.remaining() : bytesPerBlock;

        int inputLimit = input.limit();

        input.limit(input.position() + bytesToCopy);
        output.put(input);

        input.limit(inputLimit);

        return bytesPerBlock;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        // bulk method
    }

    @Override
    public int outputBufferMinLength() {
		return bytesPerBlock;
	}
}