package com.aegroto.cloudmonkey.common.encoding.stateless.differential;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

public class PredictiveDifferential4Encoder implements Encoder {
    protected final int inputLength;

    public PredictiveDifferential4Encoder(int inputLength) {
        this.inputLength = inputLength;
    }

    private byte predict(byte[] predDiffBytes, byte[] diffBytes, int i) {
        return (byte) ((predDiffBytes[i] + diffBytes[i]) / 2);
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        byte lastBytes[] = new byte[4],
             bytes[] = new byte[4],
             predDiffBytes[] = new byte[4],
             diffBytes[] = new byte[4];

        for(int i = 0; i < 4; ++i)
            lastBytes[i] = input.get();

        output.put(lastBytes);

        while(input.hasRemaining()) {
            for(int i = 0; i < 4; ++i)
                bytes[i] = input.get();
            
            for(int i = 0; i < 4; ++i)
                predDiffBytes[i] = predict(predDiffBytes, diffBytes, i);

            for(int i = 0; i < 4; ++i)
                diffBytes[i] = (byte) (bytes[i] - lastBytes[i] + predDiffBytes[i]);

            output.put(diffBytes);

            for(int i = 0; i < 4; ++i)
                lastBytes[i] = bytes[i];
        }

        return input.capacity();
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        byte diffBytes[] = new byte[4],
             predDiffBytes[] = new byte[4],
             decodedBytes[] = new byte[4];
       
        for(int i = 0; i < 4; ++i)
            decodedBytes[i] = input.get();

        output.put(decodedBytes);

        while(input.hasRemaining()) {
            for(int i = 0; i < 4; ++i)
                predDiffBytes[i] = predict(predDiffBytes, diffBytes, i);

            for(int i = 0; i < 4; ++i)
                diffBytes[i] = (byte) (input.get() & 0xFF);
           
            for(int i = 0; i < 4; ++i)
                decodedBytes[i] = (byte) (diffBytes[i] + decodedBytes[i] - predDiffBytes[i]);
            
            output.put(decodedBytes);
        }
    }

    @Override
    public int outputBufferMinLength() {
		return inputLength;
	}
}