package com.aegroto.cloudmonkey.common.encoding.stateless.downsampling;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.jme3.system.NativeLibraryLoader;
import com.jme3.system.Platform;

import lombok.Getter;

public class NativePixelDownsamplingEncoder implements Encoder {
    static {
        NativeLibraryLoader.registerNativeLibrary("NativeClasses", Platform.Linux64, "nativeclasses.so");        
        
        NativeLibraryLoader.loadNativeLibrary("NativeClasses", true);
    }

    protected final int width, height;

    @Getter protected final int pixelsPerDsRow;

    public NativePixelDownsamplingEncoder(int width, int height) {
        this.width = width;
        this.height = height;
        this.pixelsPerDsRow = (2 + (width-2)/2);
    }

    public native int encodeNative(ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int width, int height); 
    public native void decodeNative(ByteBuffer input, ByteBuffer output, int inputOffset, int outputOffset, int width, int height, int encodedInputLength);

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        int outLength = encodeNative(input, output, input.position(), output.position(), width, height);

        input.position(input.limit());
        output.position(output.position() + outLength);

        return outLength;
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        decodeNative(input, output, input.position(), output.position(), width, height, input.remaining());

        input.position(input.limit());
        output.position(output.position() + width * height * 4);
    }

    @Override
    public int outputBufferMinLength() {
		return (width*2 + height*pixelsPerDsRow + 1) * 4;
	}

}

