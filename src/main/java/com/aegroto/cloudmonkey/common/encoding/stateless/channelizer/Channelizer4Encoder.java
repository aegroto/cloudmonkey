package com.aegroto.cloudmonkey.common.encoding.stateless.channelizer;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.common.encoding.Encoder;

public class Channelizer4Encoder implements Encoder {
    protected final int inputLength;

    public Channelizer4Encoder(int inputLength) {
        this.inputLength = inputLength;
    }

    @Override
    public int encode(ByteBuffer input, ByteBuffer output) {
        for(int i = 0; i < 4; ++i) {
            input.position(i);

            output.put(input.get());

            do {
                input.position(input.position() + 3);
                output.put(input.get());
            } while (input.remaining() >= 4);
            input.rewind();
        }

        return input.capacity();
    }

    @Override
    public void decode(ByteBuffer input, ByteBuffer output) {
        int bytesPerChannel = input.remaining() / 4;

        for(int i = 0; i < 4; ++i) {
            output.position(i);

            // System.out.printf("Iteration %d, output remaining %d\n", i, output.remaining());
            // System.out.println("Input position: " + input.position() + ", output position: "+ output.position());
            
            // System.out.println("Input length: " + input.remaining() + " " + output.remaining());

            output.put(input.get());

            int writtenBytes = 1;
            while (writtenBytes < bytesPerChannel) {
                output.position(output.position() + 3);
                output.put(input.get());
                ++writtenBytes;

                // System.out.println("Input position: " + input.position() + ", output position: " + output.position());
            }

            /*System.out.print("Current output: ");
            while(output.hasRemaining()) {
                System.out.print(output.get() + " ");
            }
            System.out.print("\n");*/
        }
    }

    @Override
    public int outputBufferMinLength() {
		return inputLength;
	}

}