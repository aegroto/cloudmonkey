package com.aegroto.cloudmonkey.common.opencl;

import com.jme3.asset.AssetManager;
import com.jme3.opencl.CommandQueue;
import com.jme3.opencl.Context;
import com.jme3.opencl.Program;
import com.jme3.system.JmeContext;

import lombok.Getter;

public class OpenCLManager {
    @Getter protected static Context context; 
    @Getter protected static CommandQueue cmdQueue; 

    protected static AssetManager assetManager;

    public static void init(JmeContext jmeContext, AssetManager _assetManager) {
        assetManager = _assetManager;
        context = jmeContext.getOpenCLContext();
        cmdQueue = context.createQueue();
    }

    public static Program loadProgram(String path) {
        Program program = OpenCLManager.getContext().createProgramFromSourceFiles(assetManager, path); 
        return program;
    }
}