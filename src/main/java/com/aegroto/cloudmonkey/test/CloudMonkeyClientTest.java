package com.aegroto.cloudmonkey.test;

import java.io.IOException;

import com.aegroto.cloudmonkey.client.CloudMonkeyClientAppState;
import com.aegroto.cloudmonkey.client.CloudMonkeyStatusClient;
import com.aegroto.cloudmonkey.client.CloudMonkeyVideoClient;
import com.aegroto.cloudmonkey.common.encoding.stateful.composite.DeltaEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateful.composite.EpsilonEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateful.composite.GammaEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.dummy.NullEncoder;
import com.aegroto.cloudmonkey.common.opencl.OpenCLManager;
import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Quad;
import com.jme3.system.AppSettings;

public class CloudMonkeyClientTest extends SimpleApplication {
    protected CloudMonkeyClientAppState clientAppState;

    public static void main(String[] args) {
        CloudMonkeyClientTest app = new CloudMonkeyClientTest();
        AppSettings settings = new AppSettings(true);
        settings.setWidth(1280);
        settings.setHeight(720);
        settings.setOpenCLSupport(true);
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start();
    }

    @Override
    public void simpleInitApp() {
        // OpenCL init
        OpenCLManager.init(context, assetManager);

        // Scene init
        viewPort.setBackgroundColor(ColorRGBA.White);

        this.setDisplayFps(true);
        this.setDisplayStatView(false);

        inputManager.setCursorVisible(true);

        flyCam.setMoveSpeed(30f);

        flyCam.setMoveSpeed(0f);
        flyCam.setRotationSpeed(0f);

        // CloudMonkey client init
        clientAppState = new CloudMonkeyClientAppState(assetManager, "127.0.0.1");
        stateManager.attach(clientAppState);

        Geometry streamGeom = new Geometry("Canvas geom", new Quad(settings.getWidth(), settings.getHeight()));
        streamGeom.setMaterial(clientAppState.getStreamMaterial());
        guiNode.attachChild(streamGeom);
    }

    @Override
    public void simpleUpdate(float tpf) { }

    @Override
    public void simpleRender(RenderManager rm) { }
}