package com.aegroto.cloudmonkey.test;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;

import com.aegroto.cloudmonkey.common.Helpers;
import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.AvgRunLengthLZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4AvgRunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4LZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4NAvgRunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.MultithreadZstdEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.composite.RepPixelDSChRedEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.differential.ParallelPixelDifferentialEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.DownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.PixelDownsamplingEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.dummy.NullEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.NativeCanonicalHuffmanEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.NativeHuffmanEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.AvgRunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.NativeAvgRunLengthEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdDictEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder;
import com.aegroto.cloudmonkey.common.opencl.OpenCLManager;
import com.jme3.app.SimpleApplication;
import com.jme3.math.FastMath;
import com.jme3.opencl.Device;
import com.jme3.opencl.Platform;
import com.jme3.opencl.PlatformChooser;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext;
import com.jme3.util.BufferUtils;

import org.junit.Test;

import lombok.AllArgsConstructor;

public class StatelessEncodersTestAndBenchmark extends SimpleApplication {
    static class InputImageData {
        ByteBuffer data;
        int width;
        int height;

        public InputImageData(ByteBuffer data, int width, int height) {
            this.data = data;
            this.width = width;
            this.height = height;
        }
    }

    private static InputImageData convertBufferImageToByteBuffer(BufferedImage bimg) {
        DataBuffer dataBuffer = bimg.getRaster().getDataBuffer();
        ByteBuffer byteBuffer = null;

        if (dataBuffer instanceof DataBufferByte) {
            byte[] pixelData = ((DataBufferByte) dataBuffer).getData();
            byteBuffer = ByteBuffer.wrap(pixelData);
        } else if (dataBuffer instanceof DataBufferUShort) {
            short[] pixelData = ((DataBufferUShort) dataBuffer).getData();
            byteBuffer = ByteBuffer.allocate(pixelData.length * 2);
            byteBuffer.asShortBuffer().put(ShortBuffer.wrap(pixelData));
        } else if (dataBuffer instanceof DataBufferShort) {
            short[] pixelData = ((DataBufferShort) dataBuffer).getData();
            byteBuffer = ByteBuffer.allocate(pixelData.length * 2);
            byteBuffer.asShortBuffer().put(ShortBuffer.wrap(pixelData));
        } else if (dataBuffer instanceof DataBufferInt) {
            int[] pixelData = ((DataBufferInt) dataBuffer).getData();
            byteBuffer = ByteBuffer.allocate(pixelData.length * 4);
            byteBuffer.asIntBuffer().put(IntBuffer.wrap(pixelData));
        } else {
            throw new IllegalArgumentException("Not implemented for data buffer type: " + dataBuffer.getClass());
        }

        ByteBuffer directBuffer = BufferUtils.createByteBuffer(byteBuffer.capacity());
        directBuffer.put(byteBuffer);

        InputImageData imgData = new InputImageData(directBuffer, bimg.getWidth(), bimg.getHeight());

        return imgData;
    }

    public static class CustomPlatformChooser implements PlatformChooser {
        public CustomPlatformChooser() {
        }

        @Override
        public List<? extends Device> chooseDevices(List<? extends Platform> platforms) {
            return platforms.get(0).getDevices();
        }

    }

    public static void main(String[] args) {
        StatelessEncodersTestAndBenchmark app = new StatelessEncodersTestAndBenchmark();
        AppSettings settings = new AppSettings(true);
        settings.setWidth(1);
        settings.setHeight(1);
        settings.setOpenCLSupport(true);
        settings.setOpenCLPlatformChooser(CustomPlatformChooser.class);
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start(JmeContext.Type.Display);
        // app.start(JmeContext.Type.OffscreenSurface);
    }

    @Override
    public void simpleInitApp() {
        OpenCLManager.init(context, assetManager);

        String encoderClasses[] = {
                // "com.aegroto.cloudmonkey.common.encoding.stateless.wavelet.HaarWaveletEncoder",

                /* Differential */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.differential.DifferentialEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.differential.ParallelPixelDifferentialEncoder",

                /* Run-length */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.runlength.RunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.runlength.VariableRunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.runlength.LVRunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.runlength.LowRunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.runlength.AvgRunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.runlength.NativeAvgRunLengthEncoder",

                /* Channelizers */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.channelizer.Channelizer4Encoder",

                /* Huffman */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.huffman.NativeHuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.huffman.NativeCanonicalHuffmanEncoder",

                /* Composite */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.SelectiveRLVHEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.BlockD4Encoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.D4HuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4HuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4SelRLVHEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4DSelRLVHEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4DLessSelRLVHEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4HaarSelRLVHEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4DHuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4HaarHuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.BD4HuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.PD4HuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.ApproxHuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.D4ApproxHuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.PD4ApproxHuffmanEncoder",

                /* Composite run-length */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4RunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4VarRunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4LVRunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4AvgRunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4NAvgRunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4DNAvgRunLengthEncoder",

                /* LZ4 */
                "com.aegroto.cloudmonkey.common.encoding.stateless.lz4.LZ4Encoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4LZ4Encoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.D4LZ4Encoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.ApproxLZ4Encoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.AvgRunLengthLZ4Encoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.PixelDSLZ4Encoder",

                /* Zstandard */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.zstd.ZstdDictEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.MultithreadZstdEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.BlockZstdEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.ApproxZstdEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4ZstdEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.AvgRunLengthZstdEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.AvgRunLengthZstdEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4AvgrlZstdEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.DownsamplingZstdEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.PixelDSZstdEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.PixelDSApproxZstdEncoder",

                /* Approximation */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.approximation.ApproximationEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.approximation.NativeApproximationEncoder",

                /* Downsampling */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.DownsamplingEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.PixelDownsamplingEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.ParallelPixelDownsamplingEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.downsampling.NativePixelDownsamplingEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4DownsamplingEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.C4DownsampledRunLengthEncoder",

                /* Channel Redundancy */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.channelredundancy.ChannelRedundancyEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.RepPixelDSChRedEncoder",

                /* Snappy */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.snappy.SnappyEncoder",

                /* Parallel */
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.ParDHuffmanEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.ParDVarRunLengthEncoder",
                // "com.aegroto.cloudmonkey.common.encoding.stateless.composite.ParDAVarRunLengthEncoder",
        };

        // Sample testing
        // launchSingleSampleTest();

        // Random sources testing
        launchRandomInputTests(encoderClasses);

        // Image testing
        // launchImageTests(encoderClasses);

        System.exit(0);
    }

    protected static void launchSingleSampleTest() {
        ByteBuffer sampleInput = BufferUtils
                .createByteBuffer(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 })
                .order(ByteOrder.nativeOrder());

        singleEncodingTest(new LZ4Encoder(16), sampleInput);
    }

    protected static void launchRandomInputTests(String[] encoderClasses) {
        System.out.println("Testing on random inputs:");

        int inputCount = 1, inputWidth = 1920, inputHeight = 1080, inputBytesPerValue = 4;

        for(int i = 0; i < encoderClasses.length; ++i) {
            randomSourcesEncodingTest(encoderClasses[i], inputCount, inputWidth, inputHeight, inputBytesPerValue, (byte) 0, (byte) 16, 0.5f, false);
        }
    }

    protected static void launchImageTests(String[] encoderClasses) {
        File imgDbFolder = new File("src/main/resources/testimgdb");
        // File imgDbFolder = new File("./testimgdb");
        File[] files = imgDbFolder.listFiles();
        Arrays.sort(files);

        int maxFiles = files.length;
        
        // ByteBuffer[] imageBuffers = new ByteBuffer[maxFiles];
        BufferedImage[] bufferedImages = new BufferedImage[maxFiles];

        for(int i = 0; i < bufferedImages.length; ++i) {
            String fileName = files[i].getName();
            String fileNameParts[] = fileName.split("\\.");

            System.out.println("Loading image: " + Arrays.toString(fileNameParts));

            try {
                // bimg = ImageIO.read(files[i]);
                // imageBuffers[i] = convertBufferImageToByteBuffer(bimg);
                
                bufferedImages[i] = ImageIO.read(files[i]);
                // ImageIO.write(bimg, fileNameParts[1], new File(outImgDbFolder + "/" + fileNameParts[0] + "." + fileNameParts[1]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for(String encoder : encoderClasses) {
            imageSourcesEncodingTest(encoder, bufferedImages, true);
        }
    }

    protected static void imageSourcesEncodingTest(String encoderClassName, BufferedImage[] inputImages, boolean saveToFile) { 
        Encoder encoder;
        Class<?> encoderClass;
        Constructor<?> encoderConstructor;

        try {
            encoderClass = Class.forName(encoderClassName);

            try {
                encoderConstructor = encoderClass.getConstructor(Integer.TYPE);
            } catch(NoSuchMethodException e) {
                encoderConstructor = encoderClass.getConstructor(Integer.TYPE, Integer.TYPE);
            }
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException e) {
            System.err.println("Unable to initialize encoder class " + encoderClassName + "(" + e.getClass().getName() + ")");
            return;
        }

        InputImageData[] inputs = new InputImageData[inputImages.length];

        for(int i = 0; i < inputImages.length; ++i) {
            inputs[i] = convertBufferImageToByteBuffer(inputImages[i]);
        }

        double failRates[] = new double[inputs.length];
        double efficiency[] = new double[inputs.length];
        double psnr[] = new double[inputs.length];
        double mse[] = new double[inputs.length];
        double encodingTimes[] = new double[inputs.length];
        double decodingTimes[] = new double[inputs.length];

        long startTime, endTime;

        for(int i = 0; i < inputs.length; ++i) {
            InputImageData input = inputs[i];

            System.out.printf("Encoding image " + (i + 1) + "/" + inputs.length + " using " + encoderClassName + "...\n");

            try {
                try {
                    encoder = (Encoder) encoderConstructor.newInstance(input.data.capacity());
                } catch(IllegalArgumentException e) {
                    encoder = (Encoder) encoderConstructor.newInstance(input.width, input.height);
                }
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                System.err.println("Unable to initialize encoder " + encoderClassName + " with length " + input.data.capacity());
                continue;
			}

            int l = input.data.capacity();

            ByteBuffer encodedInput = BufferUtils.createByteBuffer(encoder.outputBufferMinLength()).order(encoder.getByteOrder());
            ByteBuffer output = BufferUtils.createByteBuffer(l); 

            input.data.rewind();
           
            /*int currentRowBytes = 0;
            System.out.print("Input : \n");
            while(input.data.hasRemaining()) {
                System.out.print(input.data.get() + ", ");

                ++currentRowBytes;
                if(currentRowBytes == input.width * 4) {
                    currentRowBytes = 0;
                    System.out.print("\n");
                }
            }
            System.out.print("\n");
            input.data.rewind();*/

            startTime = System.currentTimeMillis();
            int compressedSize = encoder.encode(input.data, encodedInput);
            endTime = System.currentTimeMillis();

            encodingTimes[i] = endTime - startTime;

            efficiency[i] = 1.0 - ((double) compressedSize) / l;

            encodedInput.flip();

            startTime = System.currentTimeMillis();
            encoder.decode(encodedInput, output);         
            endTime = System.currentTimeMillis();

            decodingTimes[i] = endTime - startTime;

            input.data.rewind();
            encodedInput.flip();
            output.rewind();

            long differentBytes = 0;

            /* boolean foundError = false;
            int counter = 0; */

            while(input.data.hasRemaining()) {
                byte bi = input.data.get();
                byte bo = output.get();

                if(bi != bo) {
                    ++differentBytes;
                    /*if(!foundError) {
                        System.err.println("Mismatch at " + counter + " : " + bi + " != " + bo);
                        foundError = true;
                    }*/
                }

                // ++counter;
            }

            failRates[i] = ((double) differentBytes) / l;

            input.data.flip();
            output.flip();

            mse[i] = calculateMSE(input.data, output);
            psnr[i] = calculatePSNR(mse[i]);

            if(saveToFile) {
                output.flip();

                try {
                    int[] imgRawArray = new int[output.remaining()];
                    for(int k = 0; k < imgRawArray.length; k += 4) { 
                        imgRawArray[k + 3] = output.get();
                        imgRawArray[k + 2] = output.get();
                        imgRawArray[k + 1] = output.get();
                        imgRawArray[k] = output.get();
                    }
                   
                    BufferedImage outputImage = new BufferedImage(inputImages[i].getWidth(), inputImages[i].getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
                    // BufferedImage outputImage = inputImages[i];
                    WritableRaster raster = (WritableRaster) outputImage.getData();

                    raster.setPixels(0, 0, raster.getWidth(), raster.getHeight(), imgRawArray);

                    outputImage.setData(raster);

                    // DataBufferByte dataBufferByte = new DataBufferByte(imgRawArray, imgRawArray.length);
                    // WritableRaster raster = WritableRaster.createBandedRaster(dataBufferByte,
                    //  inputImages[i].getWidth(), inputImages[i].getHeight(), arg3, arg4, arg5, arg6)
                    
                    String[] encoderClassNameSplit = encoderClassName.split("\\.");

                    File outFile = new File("src/main/resources/outimgdb/"
                        + encoderClassNameSplit[encoderClassNameSplit.length - 1] + "/" + i + ".png");
                    outFile.mkdirs();

                    ImageIO.write(outputImage, "png", outFile);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            output.rewind();
        }

        printAverageDouble(encoderClassName, "fail rate", failRates);
        printAverageDouble(encoderClassName, "efficiency", efficiency);
        printAverageDouble(encoderClassName, "encoding time", encodingTimes);
        printAverageDouble(encoderClassName, "decoding time", decodingTimes);
        printAverageDouble(encoderClassName, "MSE", mse);
        printAverageDouble(encoderClassName, "PSNR", psnr);
    }

    protected static void singleEncodingTest(Encoder encoder, ByteBuffer input) {
        long differentBytes = 0;
        double efficiency = 0;
 
        ByteBuffer encodedInput = BufferUtils.createByteBuffer(encoder.outputBufferMinLength()).order(encoder.getByteOrder());
        ByteBuffer output = BufferUtils.createByteBuffer(input.capacity()).order(encoder.getByteOrder());

        System.out.print("Input  : ");
        while(input.hasRemaining()) {
            System.out.print(input.get() + " ");
        }
        System.out.print("\n");
        input.rewind();

        int compressedSize = encoder.encode(input, encodedInput);
        efficiency = 1.0 - ((double) compressedSize) / input.capacity();

        encodedInput.flip();
        System.out.print("Encoded: ");
        while(encodedInput.hasRemaining()) {
            byte b = encodedInput.get();
            System.out.print(b + " ");
            // System.out.print(Helpers.byteToBinaryString(b) + " ");
        }
        System.out.print("\n");
        encodedInput.flip();
        encoder.decode(encodedInput, output);   

        input.rewind();
        output.rewind();

        int i = 0;

        while(input.hasRemaining()) {
            byte ib = input.get();
            byte ob = output.get();

            if(ib != ob) {
                ++differentBytes;
                // System.out.printf("ERROR at %d (%d, %d)\n", i, ib, ob);
            }

            ++i;
        }

        output.rewind();
        System.out.printf("Output (%d): ", differentBytes);
        while(output.hasRemaining()) {
            System.out.print(output.get() + " ");
        }
        System.out.print("\n");
    }

    protected static void randomSourcesEncodingTest(String encoderClassName, int n, int width, int height, int bytesPerValue, byte min, byte max, float variance, boolean printData) {
        double failRates[] = new double[n];
        double efficiency[] = new double[n]; 
        double encodingTimes[] = new double[n];
        double decodingTimes[] = new double[n];

        long startTime, endTime;
        
        Encoder encoder;
        Class<?> encoderClass;
        Constructor<?> encoderConstructor;

        int l = width * height * bytesPerValue;

        try {
            encoderClass = Class.forName(encoderClassName);
            try {
                encoderConstructor = encoderClass.getConstructor(Integer.TYPE);
                encoder = (Encoder) encoderConstructor.newInstance(l);
            } catch(NoSuchMethodException e) {
                encoderConstructor = encoderClass.getConstructor(Integer.TYPE, Integer.TYPE);
                encoder = (Encoder) encoderConstructor.newInstance(width, height);
            }
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException
                | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            System.err.println("Unable to initialize encoder class " + encoderClassName + "(" + e.getClass().getName() + ")");
            return;
        }

        ByteBuffer input = BufferUtils.createByteBuffer(l).order(encoder.getByteOrder());
        ByteBuffer encodedInput = BufferUtils.createByteBuffer(encoder.outputBufferMinLength()).order(encoder.getByteOrder());
        ByteBuffer output = BufferUtils.createByteBuffer(l).order(encoder.getByteOrder());

        for(int i = 0; i < n; ++i) {
            byte lastValue = (byte) FastMath.nextRandomInt(min, max); 
            for(int j = 0; j < l; ++j) {
                if(FastMath.nextRandomFloat() <= variance) { 
                    lastValue = (byte) FastMath.nextRandomInt(min, max); 
                }

                input.put(lastValue);
            }

            input.rewind();

            if(printData) {
                int currentRowBytes = 0;
                int currentValueBytes = 0;

                System.out.print("Input : \n");
                System.out.print("[ ");
                while(input.hasRemaining()) {
                    if(currentValueBytes == bytesPerValue) {
                        System.out.print("[ ");
                        currentValueBytes = 0;
                    }

                    System.out.print((input.get()) + " ");

                    ++currentValueBytes;
                    if(currentValueBytes == bytesPerValue) {
                        System.out.print("] ");
                    }

                    ++currentRowBytes;
                    if(currentRowBytes == width * 4) {
                        System.out.print("\n");
                        currentRowBytes = 0;
                    }
                }
                System.out.print("\n");
                input.rewind();
            }
            System.out.print("\n");

            startTime = System.currentTimeMillis();
            int compressedSize = encoder.encode(input, encodedInput);
            endTime = System.currentTimeMillis();

            encodingTimes[i] = endTime - startTime;
            efficiency[i] = 1.0 - ((double) compressedSize) / l;

            encodedInput.flip();

            if(printData) {
                int currentValueBytes = bytesPerValue;

                System.out.print("Encoded input: ");
                while(encodedInput.hasRemaining()) {
                    if(currentValueBytes == bytesPerValue) {
                        System.out.print("[ ");
                        currentValueBytes = 0;
                    }
                    byte b = encodedInput.get();
                    System.out.print(b + " "); 
                    
                    ++currentValueBytes;
                    if(currentValueBytes == bytesPerValue) {
                        System.out.print("] ");
                    }
                }
                System.out.print("\n");
                encodedInput.flip();
            }
            System.out.print("\n");

            startTime = System.currentTimeMillis();
            encoder.decode(encodedInput, output);         
            endTime = System.currentTimeMillis();

            decodingTimes[i] = endTime - startTime;

            /*for(int c = 0; c < input.length; ++c) {
                if(input[c] != output[c]) {
                    ++differentBytes;
                }
            }*/

            input.rewind();
            output.rewind();

            long differentBytes = 0;

            while(input.hasRemaining()) {
                if(input.get() != output.get()) {
                    ++differentBytes;
                }
            }

            failRates[i] = ((double) differentBytes) / l;

            if(printData) {
                output.rewind();
                int currentRowBytes = 0;
                int currentValueBytes = bytesPerValue;

                System.out.print("Output : \n");
                while(output.hasRemaining()) { 
                    if(currentValueBytes == bytesPerValue) {
                        System.out.print("[ ");
                        currentValueBytes = 0;
                    }

                    System.out.print(output.get() + " ");
                    ++currentRowBytes;
                    ++currentValueBytes;
                    if(currentValueBytes == bytesPerValue) {
                        System.out.print("] ");
                    }

                    if(currentRowBytes == width * 4) {
                        System.out.print("\n");
                        currentRowBytes = 0;
                    }
                }
                System.out.print("\n");
            }

            input.clear();
            encodedInput.clear();
            output.clear();

        }

        printAverageDouble(encoder.getClass().getSimpleName(), "fail rate", failRates);
        printAverageDouble(encoder.getClass().getSimpleName(), "efficiency", efficiency);
        printAverageDouble(encoder.getClass().getSimpleName(), "encoding time", encodingTimes);
        printAverageDouble(encoder.getClass().getSimpleName(), "decoding time", decodingTimes);
    }

    
    protected static void printAverageDouble(String encoderClassName, String indexName, double[] indices) {
        double sum = 0;

        for(int i = 0; i < indices.length; ++i) {
            sum += indices[i];
        }

        System.out.println(encoderClassName + " average " + indexName + ": " + sum / indices.length);
    } 

    protected static double calculateMSE(ByteBuffer input, ByteBuffer output) {
        // MSE
        long squareDiffSum = 0; 

        while(input.hasRemaining()) {
            long diff = input.get() - output.get();
            squareDiffSum += diff * diff;
        }

        return (double) (squareDiffSum) / input.capacity();
    }

    protected static double calculatePSNR(double MSE) {
        if(MSE > 0)
            return 20 * Math.log10(255/Math.sqrt(MSE));
        else
            return 99;
    }

}