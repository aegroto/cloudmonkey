package com.aegroto.cloudmonkey.test;

import java.io.IOException;

import com.aegroto.cloudmonkey.common.encoding.stateful.composite.EpsilonEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateful.composite.GammaEncoder;
import com.aegroto.cloudmonkey.common.opencl.OpenCLManager;
import com.aegroto.cloudmonkey.server.CloudMonkeyServerAppState;
import com.aegroto.cloudmonkey.server.CloudMonkeyStatusServer;
import com.aegroto.cloudmonkey.server.CloudMonkeyVideoServer;
import com.jme3.app.LostFocusBehavior;
import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext;

public class CloudMonkeyServerTest extends SimpleApplication {

    public static void main(String[] args) {
        CloudMonkeyServerTest app = new CloudMonkeyServerTest();
        AppSettings settings = new AppSettings(true);
        settings.setWidth(1280);
        settings.setHeight(720);
        settings.setFrameRate(120);
        settings.setOpenCLSupport(true);
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start(JmeContext.Type.Display);
    }

    private Geometry testGeom;
    private Material testMat;

    private final boolean ONLINE = true;

    protected CloudMonkeyServerAppState serverAppState;

    @Override
    public void simpleInitApp() {
        // OpenCL init
        OpenCLManager.init(context, assetManager);

        // Scene init

        this.setLostFocusBehavior(LostFocusBehavior.Disabled);

        viewPort.setBackgroundColor(ColorRGBA.BlackNoAlpha);

        testGeom = new Geometry("Test box", new Box(1.860f * 3f, 1.051f * 3f, 1.5f * 3f));
        testMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        testMat.setColor("Color", ColorRGBA.Red);
        // testMat.setTexture("ColorMap",
        // assetManager.loadTexture("Common/Textures/MissingTexture.png"));
        testMat.setTexture("ColorMap", assetManager.loadTexture("samplescreen.png"));
        testGeom.setMaterial(testMat);
        rootNode.attachChild(testGeom);

        flyCam.setRotationSpeed(1f);
        flyCam.setMoveSpeed(100f);

        if (ONLINE) {
            serverAppState = new CloudMonkeyServerAppState();
            stateManager.attach(serverAppState);
        }
    }

    private float angle = 0f;

    @Override 
    public void simpleUpdate(float tpf) {
        angle += 2f * tpf;
        angle %= FastMath.PI;

        testGeom.setLocalRotation(new Quaternion().fromAngles(0f, angle, 0f));

        if(angle % FastMath.PI < .1f) {
            testMat.setColor("Color", ColorRGBA.randomColor());
        }
    }

    @Override
    public void simpleRender(RenderManager rm) {
        // Raw way to test, please forget this code line
        inputManager.setCursorVisible(true);
    }
}