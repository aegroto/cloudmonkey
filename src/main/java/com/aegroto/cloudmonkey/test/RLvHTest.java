package com.aegroto.cloudmonkey.test;

import java.nio.ByteBuffer;
import java.util.Arrays;

import com.aegroto.cloudmonkey.common.encoding.stateless.huffman.HuffmanEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateless.runlength.VariableRunLengthEncoder;
import com.jme3.math.FastMath;
import com.jme3.util.BufferUtils;

import org.junit.Test;

public class RLvHTest {
    @Test
    public static void main(String[] args) {
        int nAlphabets = 16;
        int length = 16*16; 
        double varianceChange = 0.1;
        double minVariance = 0.1;
        double maxVariance = 0.9;

        randomVarianceEncodingTest(nAlphabets, length, (byte) -128, (byte) 127, minVariance, maxVariance, varianceChange, false);
    }

    private static void randomVarianceEncodingTest(int nAlphabets, int length, byte min, byte max, double minVariance, double maxVariance, double varianceChange, boolean printData) {
        int variancesTests = 0;

        // Most lame way to calculate expected variances tests
        for(double variance = minVariance; variance <= maxVariance; variance += varianceChange) {
            ++variancesTests;
        }

        long[] totalJumps = new long[variancesTests];
        long[] totalReEncLength = new long[variancesTests];
        long[] totalHuffTreeSize = new long[variancesTests];
        long[] totalHuffEncLength = new long[variancesTests];
        
        for(int t = 0; t < nAlphabets; ++t) {
            ByteBuffer input = BufferUtils.createByteBuffer(length);

            VariableRunLengthEncoder rEncoder = new VariableRunLengthEncoder(length);
            HuffmanEncoder huffEncoder = new HuffmanEncoder(length);

            ByteBuffer reEncodedInput = BufferUtils.createByteBuffer(rEncoder.outputBufferMinLength());
            ByteBuffer huffEncodedInput = BufferUtils.createByteBuffer(huffEncoder.outputBufferMinLength());

            int currentVarianceTest = 0;

            int remainingLength = length;
           
            // Alphabet generation
            int[] alphabetFrequencies = new int[256];

            while(remainingLength > 0) {
                int symbolLength = FastMath.nextRandomInt(length / 1000, length / 1000 + 50);
                if(symbolLength > remainingLength)
                    symbolLength = remainingLength;

                alphabetFrequencies[FastMath.nextRandomInt(min, max) & 0xFF] += symbolLength;
                remainingLength -= symbolLength;
            }

            /*for(int i = min; i <= max; ++i) {
                if(alphabetFrequencies[((byte) i) & 0xFF] > 0)
                    System.out.println(i + " = " + alphabetFrequencies[((byte) i) & 0xFF]);
            } 
            System.out.println("\n");*/
            
            // Variances tests
            for(double variance = minVariance; variance <= maxVariance; variance += varianceChange) {
                // System.out.println("Testing with variance: " + variance + " (" + currentVarianceTest + ")");
                
                long jumps = 0;
                long reEncLength = 0;
                long huffTreeSize = 0;
                long huffEncLength = 0;

                int[] currentAlphabetFrequencies = Arrays.copyOf(alphabetFrequencies, alphabetFrequencies.length); 

                byte currentValue = (byte) FastMath.nextRandomInt(min, max);

                // Input generation
                while(input.hasRemaining()) {
                    // System.out.println("\nCurrent value: " + currentValue + " (" + input.remaining() + ")");

                    int currentValueIndex = currentValue & 0xFF;

                    if(currentAlphabetFrequencies[currentValueIndex] > 0 && FastMath.nextRandomFloat() >= variance) {
                        input.put(currentValue);
                        --currentAlphabetFrequencies[currentValueIndex];
                    } else {
                        do {
                            currentValue = (byte) FastMath.nextRandomInt(min, max);
                        } while(currentAlphabetFrequencies[currentValue & 0xFF] <= 0); 
                    }
                
                    /*for(byte i = min; i <= max; ++i) {
                        System.out.println(i + " = " + currentAlphabetFrequencies[i & 0xFF]);
                    }

                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) { }*/
                }
                input.flip();

                // Counting jumps
                byte prevByte = input.get();
                while(input.hasRemaining()) {
                    byte currentByte = input.get();
                    if(currentByte != prevByte) {
                        ++jumps;
                        prevByte = currentByte;
                    }
                }
                input.flip();

                if(printData) {
                    System.out.print("Input (" + variance + ") : ");
                    while(input.hasRemaining()) {
                        System.out.print((input.get()) + ", ");
                    }
                    System.out.print("\n");
                    input.rewind();
                }

                rEncoder.encode(input, reEncodedInput);
                input.flip();
                huffEncoder.encode(input, huffEncodedInput);

                reEncodedInput.flip();
                huffEncodedInput.flip();

                reEncLength = reEncodedInput.getInt();
                huffTreeSize = huffEncodedInput.getInt() * 8;
                huffEncLength = huffEncodedInput.getInt();

                totalJumps[currentVarianceTest] += jumps;
                totalReEncLength[currentVarianceTest] += reEncLength;
                totalHuffTreeSize[currentVarianceTest] += huffTreeSize;
                totalHuffEncLength[currentVarianceTest] += huffEncLength;

                input.clear();
                reEncodedInput.clear();
                huffEncodedInput.clear();

                ++currentVarianceTest;

                /*String bestEncoder = reEncLength < huffEncLength ? "Run-length" : "Huffman";
                StringBuilder output = new StringBuilder();
                output.append("Input jumps: " + jumps + " / " + length + "\n");
                output.append("Jumps/length ratio: " + jumps / length + "\n");
                output.append("Run-length encoded length: " + reEncLength + "\n");
                output.append("Huffman encoded length: " + huffEncLength + "\n");
                output.append("Best Encoder: " + bestEncoder + " (" + (double) (reEncLength) / huffEncLength + ")\n\n");
                System.out.print(output.toString());*/
            }
        }

        for(int i = 0; i < variancesTests; ++i) {
            System.out.println("Stats for variance " + (minVariance + (varianceChange * i)) + " (" + i + ")");

            long meanJumps = totalJumps[i] / nAlphabets;
            long meanReEncLength = totalReEncLength[i] / nAlphabets;
            long meanHuffTreeSize = totalHuffTreeSize[i] / nAlphabets; 
            long meanHuffEncLength = totalHuffEncLength[i] / nAlphabets;

            String bestEncoder = meanReEncLength < meanHuffEncLength ? "Run-length" : "Huffman";
            StringBuilder output = new StringBuilder();

            output.append("Mean stats:\n");
            output.append("Input jumps: " + meanJumps + " / " + length + "\n");
            output.append("Jumps/length ratio: " + ((double) (meanJumps)) / length + "\n");
            output.append("Run-length mean encoded length: " + meanReEncLength + "\n");
            output.append("Huffman mean encoded length: " + meanHuffEncLength + "\n");
            output.append("Best Encoder: " + bestEncoder + " (" + (double) (meanReEncLength) / meanHuffEncLength + ")\n\n");
            System.out.print(output.toString());
        }

        /*System.out.println("Run-length encoded length:\t" + meanReEncLength + " bits");
        System.out.println("Huffman tree size:\t\t" + meanHuffTreeSize + " bits");
        System.out.println("Huffman encoded length:\t\t" + meanHuffEncLength + " bits");*/
    }
}