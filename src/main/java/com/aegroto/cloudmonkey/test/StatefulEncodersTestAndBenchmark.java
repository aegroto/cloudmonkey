package com.aegroto.cloudmonkey.test;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.common.encoding.stateful.composite.EpsilonEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateful.composite.GammaEncoder;
import com.aegroto.cloudmonkey.common.opencl.OpenCLManager;
import com.jme3.app.SimpleApplication;
import com.jme3.math.FastMath;
import com.jme3.opencl.Device;
import com.jme3.opencl.Platform;
import com.jme3.opencl.PlatformChooser;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext;
import com.jme3.util.BufferUtils;

public class StatefulEncodersTestAndBenchmark extends SimpleApplication {

    static class InputImageData {
        ByteBuffer data;
        int width;
        int height;

        public InputImageData(ByteBuffer data, int width, int height) {
            this.data = data;
            this.width = width;
            this.height = height;
        }
    }

    private static InputImageData convertBufferImageToByteBuffer(BufferedImage bimg) {
        DataBuffer dataBuffer = bimg.getRaster().getDataBuffer();
        ByteBuffer byteBuffer = null;

        if (dataBuffer instanceof DataBufferByte) {
            byte[] pixelData = ((DataBufferByte) dataBuffer).getData();
            byteBuffer = ByteBuffer.wrap(pixelData);
        } else if (dataBuffer instanceof DataBufferUShort) {
            short[] pixelData = ((DataBufferUShort) dataBuffer).getData();
            byteBuffer = ByteBuffer.allocate(pixelData.length * 2);
            byteBuffer.asShortBuffer().put(ShortBuffer.wrap(pixelData));
        } else if (dataBuffer instanceof DataBufferShort) {
            short[] pixelData = ((DataBufferShort) dataBuffer).getData();
            byteBuffer = ByteBuffer.allocate(pixelData.length * 2);
            byteBuffer.asShortBuffer().put(ShortBuffer.wrap(pixelData));
        } else if (dataBuffer instanceof DataBufferInt) {
            int[] pixelData = ((DataBufferInt) dataBuffer).getData();
            byteBuffer = ByteBuffer.allocate(pixelData.length * 4);
            byteBuffer.asIntBuffer().put(IntBuffer.wrap(pixelData));
        } else {
            throw new IllegalArgumentException("Not implemented for data buffer type: " + dataBuffer.getClass());
        }

        ByteBuffer directBuffer = BufferUtils.createByteBuffer(byteBuffer.capacity());

        // convert from ABGR to RGBA
        while(byteBuffer.hasRemaining()) {
            byte a = byteBuffer.get();
            byte b = byteBuffer.get();
            byte g = byteBuffer.get();
            byte r = byteBuffer.get();

            directBuffer.put(r);
            directBuffer.put(g);
            directBuffer.put(b);
            directBuffer.put(a);
        }

        InputImageData imgData = new InputImageData(directBuffer, bimg.getWidth(), bimg.getHeight());

        return imgData;
    }

    public static class CustomPlatformChooser implements PlatformChooser {
        public CustomPlatformChooser() { }

        @Override
        public List<? extends Device> chooseDevices(List<? extends Platform> platforms) {
            return platforms.get(0).getDevices();
        }
    }

    public static void main(String[] args) {
        StatefulEncodersTestAndBenchmark app = new StatefulEncodersTestAndBenchmark();
        AppSettings settings = new AppSettings(true);
        settings.setWidth(1);
        settings.setHeight(1);
        settings.setOpenCLSupport(true);
        settings.setOpenCLPlatformChooser(CustomPlatformChooser.class);
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start(JmeContext.Type.Display);
        // app.start(JmeContext.Type.OffscreenSurface);
    }

    @Override
    public void simpleInitApp() {
        OpenCLManager.init(context, assetManager);

        String encoderClasses[] = {
            // "com.aegroto.cloudmonkey.common.encoding.stateful.composite.AlphaEncoder",
            // "com.aegroto.cloudmonkey.common.encoding.stateful.composite.BetaEncoder",
            // "com.aegroto.cloudmonkey.common.encoding.stateful.composite.GammaEncoder",
            // "com.aegroto.cloudmonkey.common.encoding.stateful.composite.DeltaEncoder",
            "com.aegroto.cloudmonkey.common.encoding.stateful.composite.EpsilonEncoder",
        };

        // Sample testing
        // launchSingleSampleTest();

        // Random source testing
        // launchRandomInputTests(encoderClasses);

        // Video testing
        launchVideoTests(encoderClasses);

        System.exit(0);
    }

    protected static void launchVideoTests(String[] encoderClasses) {
        File videoDbFolder = new File("src/main/resources/testvideodb");
        File[] subfolders = videoDbFolder.listFiles();

        ArrayList<File[]> filesSequences = new ArrayList<>();

        for(int i = 0; i < subfolders.length; ++i) {
            File subfolder = subfolders[i];
            
            if(subfolder.isDirectory()) {
                System.out.println("Loading sequence: " + subfolder);

                File[] files = subfolder.listFiles();
                Arrays.sort(files);

                ArrayList<File> arrayBuffer = new ArrayList<>();

                for(int j = 0; j < files.length; j += 1) {
                    String fileName = files[j].getName();

                    // System.out.println("Loading image: " + Arrays.toString(fileNameParts));

                    arrayBuffer.add(files[j]);
                }

                filesSequences.add(arrayBuffer.toArray(new File[1]));
            }
        }

        for(String encoder : encoderClasses) {
            videoSourcesEncodingTest(encoder, filesSequences, true, false);
        }
    }

    protected static void videoSourcesEncodingTest(String encoderClassName, ArrayList<File[]> inputFileSequences, boolean saveToFile, boolean clearOutput) { 
        for(int s = 0; s < inputFileSequences.size(); ++s) {
            File[] inputFiles = inputFileSequences.get(s);

            Encoder encoder, decoder;
            Class<?> encoderClass;
            Constructor<?> encoderConstructor;

            try {
                encoderClass = Class.forName(encoderClassName);

                try {
                    encoderConstructor = encoderClass.getConstructor(Integer.TYPE);
                } catch(NoSuchMethodException e) {
                    encoderConstructor = encoderClass.getConstructor(Integer.TYPE, Integer.TYPE);
                }
            } catch (ClassNotFoundException | NoSuchMethodException | SecurityException e) {
                System.err.println("Unable to initialize encoder class " + encoderClassName + "(" + e.getClass().getName() + ")");
                return;
            }

            /*InputImageData[] inputs = new InputImageData[inputFiles.length];

            for(int i = 0; i < inputFiles.length; ++i) {
                try {
                    inputs[i] = convertBufferImageToByteBuffer(ImageIO.read(inputFiles[i]));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/

            double failRates[] = new double[inputFiles.length];
            double efficiency[] = new double[inputFiles.length];
            double psnr[] = new double[inputFiles.length];
            double mse[] = new double[inputFiles.length];
            double encodingTimes[] = new double[inputFiles.length];
            double decodingTimes[] = new double[inputFiles.length];

            long startTime, endTime;

            InputImageData input;

            try {
                input = convertBufferImageToByteBuffer(ImageIO.read(inputFiles[0]));

                try {
                    encoder = (Encoder) encoderConstructor.newInstance(input.data.capacity());
                    decoder = (Encoder) encoderConstructor.newInstance(input.data.capacity());
                } catch(IllegalArgumentException e) {
                    encoder = (Encoder) encoderConstructor.newInstance(input.width, input.height);
                    decoder = (Encoder) encoderConstructor.newInstance(input.width, input.height);
                }
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
                System.err.println("Unable to initialize encoder " + encoderClassName);
                continue;
            }

            for(int i = 0; i < inputFiles.length; ++i) {
                try {
                    input = convertBufferImageToByteBuffer(ImageIO.read(inputFiles[i]));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }


                if(clearOutput && i > 0) {
                    System.out.print(String.format("\033[2A")); // Move up
                    System.out.print("\033[2K"); // Erase line content
                }

                System.out.printf("Encoding image " + (i + 1) + "/" + inputFiles.length + " using " + encoderClassName + "...\n");

                int l = input.data.capacity();

                ByteBuffer encodedInput = BufferUtils.createByteBuffer(encoder.outputBufferMinLength()).order(encoder.getByteOrder());
                ByteBuffer output = BufferUtils.createByteBuffer(l); 

                input.data.rewind();
               
                /*
                int currentRowBytes = 0;
                System.out.print("Input : \n");
                while(input.data.hasRemaining()) {
                    System.out.print(input.data.get() + ", ");

                    ++currentRowBytes;
                    if(currentRowBytes == input.width * 4) {
                        currentRowBytes = 0;
                        System.out.print("\n");
                    }
                }
                System.out.print("\n");
                input.data.rewind();
                */

                startTime = System.currentTimeMillis();
                int compressedSize = encoder.encode(input.data, encodedInput);
                endTime = System.currentTimeMillis();

                encodingTimes[i] = endTime - startTime;

                efficiency[i] = 1.0 - ((double) compressedSize) / l;
                System.out.println("Efficiency: " + efficiency[i]);


                encodedInput.flip();

                /*
                System.out.print("Encoded input : \n");
                while(encodedInput.hasRemaining()) {
                    System.out.print(encodedInput.get() + ", ");
                }
                System.out.print("\n");
                encodedInput.flip();
                */

                startTime = System.currentTimeMillis();
                decoder.decode(encodedInput, output);         
                endTime = System.currentTimeMillis();

                decodingTimes[i] = endTime - startTime;

                input.data.rewind();
                encodedInput.flip();
                output.rewind();

                /*
                currentRowBytes = 0;
                System.out.print("Output : \n");
                while(output.hasRemaining()) {
                    System.out.print(output.get() + ", ");

                    ++currentRowBytes;
                    if(currentRowBytes == input.width * 4) {
                        currentRowBytes = 0;
                        System.out.print("\n");
                    }
                }
                System.out.print("\n");
                output.rewind();
                */

                long differentBytes = 0;

                while(input.data.hasRemaining()) {
                    byte bi = input.data.get();
                    byte bo = output.get();

                    if(bi != bo) {
                        ++differentBytes;
                    }
                }

                failRates[i] = ((double) differentBytes) / l;

                input.data.flip();
                output.flip();

                mse[i] = calculateMSE(input.data, output);
                psnr[i] = calculatePSNR(mse[i]);

                if(saveToFile) {
                    output.flip();

                    try {
                        int[] imgRawArray = new int[output.remaining()];
                        for(int k = 0; k < imgRawArray.length; k += 4) { 
                            imgRawArray[k] = output.get();
                            imgRawArray[k + 1] = output.get();
                            imgRawArray[k + 2] = output.get();
                            imgRawArray[k + 3] = output.get();
                        }
                    
                        BufferedImage outputImage = new BufferedImage(input.width, input.height, BufferedImage.TYPE_4BYTE_ABGR);
                        WritableRaster raster = (WritableRaster) outputImage.getData();

                        raster.setPixels(0, 0, raster.getWidth(), raster.getHeight(), imgRawArray);

                        outputImage.setData(raster);
                        
                        String[] encoderClassNameSplit = encoderClassName.split("\\.");

                        File outFile = new File("src/main/resources/outvideodb/"
                            + encoderClassNameSplit[encoderClassNameSplit.length - 1] + "/" + s + "/" + i + ".png");
                        outFile.mkdirs();

                        ImageIO.write(outputImage, "png", outFile);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                output.rewind();

                BufferUtils.destroyDirectBuffer(input.data);
                BufferUtils.destroyDirectBuffer(encodedInput);
                BufferUtils.destroyDirectBuffer(output);
            }

            printAverageDouble(encoderClassName, "fail rate", failRates);
            printAverageDouble(encoderClassName, "efficiency", efficiency);
            printAverageDouble(encoderClassName, "encoding time", encodingTimes);
            printAverageDouble(encoderClassName, "decoding time", decodingTimes);
            printAverageDouble(encoderClassName, "MSE", mse);
            printAverageDouble(encoderClassName, "PSNR", psnr);
        }
    }

    protected static void launchSingleSampleTest() {
        ByteBuffer sampleInput = BufferUtils.createByteBuffer(
                new byte[] {
                    1, 2, 3, 4,
                    5, 6, 7, 8,
                    9, 10, 11, 12,
                    13, 14, 15, 16
        }).order(ByteOrder.nativeOrder());

        singleEncodingTest(new EpsilonEncoder(4, 4), sampleInput);
    }

    protected static void launchRandomInputTests(String[] encoderClasses) {
        System.out.println("Testing on random inputs:");

        int inputCount = 1, inputLines = 4, inputWidth = 2, inputHeight = 2, inputBytesPerValue = 4;

        for(int i = 0; i < encoderClasses.length; ++i) {
            randomSourcesEncodingTest(
                encoderClasses[i], inputCount, inputLines, inputWidth, inputHeight, inputBytesPerValue, (byte) 0, (byte) 16, 0.5f, true
            );
        }
    }

    protected static void singleEncodingTest(Encoder encoder, ByteBuffer input) {
        long differentBytes = 0;
        double efficiency = 0;
 
        ByteBuffer encodedInput = BufferUtils.createByteBuffer(encoder.outputBufferMinLength()).order(encoder.getByteOrder());
        ByteBuffer output = BufferUtils.createByteBuffer(input.capacity()).order(encoder.getByteOrder());

        System.out.print("Input  : ");
        while(input.hasRemaining()) {
            System.out.print(input.get() + " ");
        }
        System.out.print("\n");
        input.rewind();

        int compressedSize = encoder.encode(input, encodedInput);
        efficiency = 1.0 - ((double) compressedSize) / input.capacity();

        encodedInput.flip();
        System.out.print("Encoded: ");
        while(encodedInput.hasRemaining()) {
            byte b = encodedInput.get();
            System.out.print(b + " ");
            // System.out.print(Helpers.byteToBinaryString(b) + " ");
        }
        System.out.print("\n");
        encodedInput.flip();
        encoder.decode(encodedInput, output);   

        input.rewind();
        output.rewind();

        int i = 0;

        while(input.hasRemaining()) {
            byte ib = input.get();
            byte ob = output.get();

            if(ib != ob) {
                ++differentBytes;
                // System.out.printf("ERROR at %d (%d, %d)\n", i, ib, ob);
            }

            ++i;
        }

        output.rewind();
        System.out.printf("Output (%d): ", differentBytes);
        while(output.hasRemaining()) {
            System.out.print(output.get() + " ");
        }
        System.out.print("\n");
    }

    protected static void randomSourcesEncodingTest(String encoderClassName, int n, int lines, int width, int height, int bytesPerValue, byte min, byte max, float variance, boolean printData) {
        double failRates[] = new double[n * lines];
        double efficiency[] = new double[n * lines]; 
        double encodingTimes[] = new double[n * lines];
        double decodingTimes[] = new double[n * lines];

        long startTime, endTime;
        
        Encoder encoder, decoder;
        Class<?> encoderClass;
        Constructor<?> encoderConstructor;

        int l = width * height * bytesPerValue;

        for(int i = 0; i < n; ++i) {
            try {
                encoderClass = Class.forName(encoderClassName);
                try {
                    encoderConstructor = encoderClass.getConstructor(Integer.TYPE);
                    encoder = (Encoder) encoderConstructor.newInstance(l);
                    decoder = (Encoder) encoderConstructor.newInstance(l);
                } catch(NoSuchMethodException e) {
                    encoderConstructor = encoderClass.getConstructor(Integer.TYPE, Integer.TYPE);
                    encoder = (Encoder) encoderConstructor.newInstance(width, height);
                    decoder = (Encoder) encoderConstructor.newInstance(width, height);
                }
            } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException
                    | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                System.err.println("Unable to initialize encoder class " + encoderClassName + "(" + e.getClass().getName() + ")");
                return;
            }

            ByteBuffer input = BufferUtils.createByteBuffer(l).order(encoder.getByteOrder());
            ByteBuffer encodedInput = BufferUtils.createByteBuffer(encoder.outputBufferMinLength()).order(encoder.getByteOrder());
            ByteBuffer output = BufferUtils.createByteBuffer(l).order(encoder.getByteOrder());

            for(int k = 0; k < lines; ++k) {
                byte lastValue = (byte) FastMath.nextRandomInt(min, max); 

                for(int j = 0; j < l; ++j) {
                    if(FastMath.nextRandomFloat() <= variance) { 
                        lastValue = (byte) FastMath.nextRandomInt(min, max); 
                    }

                    input.put(lastValue);
                }

                input.rewind();

                if(printData) {
                    int currentRowBytes = 0;
                    int currentValueBytes = 0;

                    System.out.print("Input : \n");
                    System.out.print("[ ");
                    while(input.hasRemaining()) {
                        if(currentValueBytes == bytesPerValue) {
                            System.out.print("[ ");
                            currentValueBytes = 0;
                        }

                        System.out.print((input.get()) + " ");

                        ++currentValueBytes;
                        if(currentValueBytes == bytesPerValue) {
                            System.out.print("] ");
                        }

                        ++currentRowBytes;
                        if(currentRowBytes == width * 4) {
                            System.out.print("\n");
                            currentRowBytes = 0;
                        }
                    }
                    System.out.print("\n");
                    input.rewind();
                }

                startTime = System.currentTimeMillis();
                int compressedSize = encoder.encode(input, encodedInput);
                endTime = System.currentTimeMillis();

                encodingTimes[i * lines + k] = endTime - startTime;
                efficiency[i * lines + k] = 1.0 - ((double) compressedSize) / l;

                encodedInput.flip();

                if(printData) {
                    int currentValueBytes = bytesPerValue;

                    System.out.print("Encoded input: ");
                    while(encodedInput.hasRemaining()) {
                        if(currentValueBytes == bytesPerValue) {
                            System.out.print("[ ");
                            currentValueBytes = 0;
                        }
                        byte b = encodedInput.get();
                        System.out.print(b + " "); 
                        
                        ++currentValueBytes;
                        if(currentValueBytes == bytesPerValue) {
                            System.out.print("] ");
                        }
                    }
                    System.out.print("\n");
                    encodedInput.flip();
                }

                startTime = System.currentTimeMillis();
                decoder.decode(encodedInput, output);         
                endTime = System.currentTimeMillis();

                decodingTimes[i * lines + k] = endTime - startTime;

                input.rewind();
                output.rewind();

                long differentBytes = 0;

                // System.out.println(input.remaining() + " " + output.remaining() + " " + l);

                while(input.hasRemaining()) {
                    if(input.get() != output.get()) {
                        ++differentBytes;
                    }
                }

                failRates[i * lines + k] = ((double) differentBytes) / l;

                if(printData) {
                    output.rewind();
                    int currentRowBytes = 0;
                    int currentValueBytes = bytesPerValue;

                    System.out.print("Output : \n");
                    while(output.hasRemaining()) { 
                        if(currentValueBytes == bytesPerValue) {
                            System.out.print("[ ");
                            currentValueBytes = 0;
                        }

                        System.out.print(output.get() + " ");
                        ++currentRowBytes;
                        ++currentValueBytes;
                        if(currentValueBytes == bytesPerValue) {
                            System.out.print("] ");
                        }

                        if(currentRowBytes == width * 4) {
                            System.out.print("\n");
                            currentRowBytes = 0;
                        }
                    }
                    System.out.print("\n");
                }

                input.clear();
                encodedInput.clear();
                output.clear();
            }
        }

        printAverageDouble(encoderClassName, "fail rate", failRates);
        printAverageDouble(encoderClassName, "efficiency", efficiency);
        printAverageDouble(encoderClassName, "encoding time", encodingTimes);
        printAverageDouble(encoderClassName, "decoding time", decodingTimes);
    }

    
    protected static void printAverageDouble(String encoderClassName, String indexName, double[] indices) {
        double sum = 0;

        for(int i = 0; i < indices.length; ++i) {
            sum += indices[i];
        }

        System.out.println(encoderClassName + " average " + indexName + ": " + sum / indices.length);
    } 

    protected static double calculateMSE(ByteBuffer input, ByteBuffer output) {
        // MSE
        long squareDiffSum = 0; 

        while(input.hasRemaining()) {
            long diff = input.get() - output.get();
            squareDiffSum += diff * diff;
        }

        return (double) (squareDiffSum) / input.capacity();
    }

    protected static double calculatePSNR(double MSE) {
        if(MSE > 0)
            return 20 * Math.log10(255/Math.sqrt(MSE));
        else
            return 99;
    }

}