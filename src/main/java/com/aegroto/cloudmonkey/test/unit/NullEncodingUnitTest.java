package com.aegroto.cloudmonkey.test.unit;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.aegroto.cloudmonkey.common.encoding.stateless.dummy.NullEncoder;
import com.jme3.math.FastMath;
import com.jme3.util.BufferUtils;

import org.junit.Test;

public class NullEncodingUnitTest {
    @Test
    public static void main(String[] args) {
        nullEncodingTest(100, 16, (byte) -128, (byte) 127);
    }

    protected static void nullEncodingTest(long n, int l,  byte min, byte max) {
        NullEncoder encoder = new NullEncoder(l);

        long differentBytes = 0;
        // int treeSize = ((max - min + 1) * 2 - 1) * 5;

        for(int i = 0; i < n; ++i) {
            /* byte[] input = new byte[l];
            byte[] encodedInput = new byte[offset + encoder.outputBufferMinLength()]; 
            byte[] output = new byte[l]; */
            ByteBuffer input = BufferUtils.createByteBuffer(l).order(encoder.getByteOrder());
            ByteBuffer encodedInput = BufferUtils.createByteBuffer(encoder.outputBufferMinLength()).order(encoder.getByteOrder());
            ByteBuffer output = BufferUtils.createByteBuffer(l).order(encoder.getByteOrder());

            for(int j = 0; j < l; ++j) {
                input.put((byte) FastMath.nextRandomInt(min, max));
            }

            input.rewind();

            System.out.print("Input : ");
            while(input.hasRemaining()) {
                System.out.print(input.get() + ", ");
            }
            System.out.print("\n");
            input.rewind();

            encoder.encode(input, encodedInput);
            encodedInput.flip();
            /*System.out.print("Encoded input: ");
            while(encodedInput.hasRemaining()) {
                System.out.print(Helpers.byteToBinaryString(encodedInput.get()) + " ");
            }
            System.out.print("\n");
            encodedInput.flip();*/
            encoder.decode(encodedInput, output);         
          
            /*for(int c = 0; c < input.length; ++c) {
                if(input[c] != output[c]) {
                    ++differentBytes;
                }
            }*/

            input.rewind();
            output.rewind();

            while(input.hasRemaining()) {
                if(input.get() != output.get()) {
                    ++differentBytes;
                    // System.err.println("ERROR");
                }
            }

            output.rewind();
            System.out.print("Output : ");
            while(output.hasRemaining()) {
                System.out.print(output.get() + ", ");
            }
            System.out.print("\n");

            // assertArrayEquals("Input: " + Arrays.toString(input), input, output);
        }

        System.out.println("Null encoding fail rate: " + (double) differentBytes / (n * l));
    }
}