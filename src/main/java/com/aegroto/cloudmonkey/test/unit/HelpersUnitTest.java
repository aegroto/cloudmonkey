package com.aegroto.cloudmonkey.test.unit;

import static org.junit.Assert.assertEquals;

import com.aegroto.cloudmonkey.common.Helpers;
import com.jme3.math.FastMath;

import org.junit.Test;

public class HelpersUnitTest {
    private static void intByteArrayTest(int n) {
        byte[] array = new byte[n * 4];

        for(int i = 0; i < n; ++i) {
            int a = FastMath.nextRandomInt();

            Helpers.putIntInByteArray(array, a, i * 4);
            assertEquals(a, Helpers.getIntFromByteArray(array, i * 4));
        }

        System.out.println("Int & byte array test: OK");
    }

    private static void shortByteArrayTest(int n) {
        byte[] array = new byte[n * 2];

        for(int i = 0; i < n; ++i) {
            short a = (short) FastMath.nextRandomInt();

            Helpers.putShortInByteArray(array, a, i * 2);
            assertEquals(a, Helpers.getShortFromByteArray(array, i * 2));
        }

        System.out.println("Short & byte array test: OK");
    }

    private static void bitsByteArrayTest(int n, int m) {
        for(int i = 0; i < n; ++i) {
            byte[] output = new byte[m];
            byte[] inputBytes = new byte[m];
            int[] offsets = new int[m];
            int[] lengths = new int[m];

            for(int j = 0; j < m; ++j) {
                inputBytes[j] = (byte) (FastMath.nextRandomInt() % 10 & 0xFF);
                lengths[j] = Helpers.byteDigitsNumber(inputBytes[j]);

                if(j > 0) { 
                    offsets[j] = offsets[j - 1] + lengths[j - 1];
                } else {
                    offsets[j] = 0;
                }

                Helpers.putBitsInByteArray(output, inputBytes[j], lengths[j], offsets[j]);
            }

            // System.out.println(Arrays.toString(inputBytes));

            for(int j = 0; j < m; ++j) {
                assertEquals(inputBytes[j], Helpers.getBitsFromByteArray(output, lengths[j], offsets[j]));
            }
        }
        System.out.println("Bits & byte array test: OK");
    }

    private static void simpleBitsByteArrayTest() {
        byte[] inputBytes = { 8, 12, 2, -4 };
        byte[] output = new byte[inputBytes.length + 1];
        int[] offsets = new int[inputBytes.length];
        int[] lengths = new int[inputBytes.length];

        for(int j = 0; j < inputBytes.length; ++j) {
            lengths[j] = Helpers.byteDigitsNumber(inputBytes[j]);

            if(j > 0) { 
                offsets[j] = offsets[j - 1] + lengths[j - 1];
            } else {
                offsets[j] = 0;
            }

            System.out.println(Helpers.byteToBinaryString(inputBytes[j]) + " " + Helpers.byteDigitsNumber(inputBytes[j]) + " " + offsets[j]);  

            Helpers.putBitsInByteArray(output, inputBytes[j], lengths[j], offsets[j]);
        }

        System.out.println("--");

        for(int j = 0; j < output.length; ++j) {
            System.out.println(Helpers.byteToBinaryString(output[j]));  
        }

        for(int j = 0; j < inputBytes.length; ++j) {
            assertEquals(inputBytes[j], Helpers.getBitsFromByteArray(output, lengths[j], offsets[j]));
        }
    }

    @Test
    public static void main(String[] args) {
        // int & byte array test
        intByteArrayTest(100);

        shortByteArrayTest(100);

        // simpleBitsByteArrayTest();
        // bits & byte array test
        bitsByteArrayTest(128, 64);
    }
}