package com.aegroto.cloudmonkey.server;

import java.io.IOException;
import java.util.List;

import com.aegroto.cloudmonkey.common.encoding.stateful.composite.EpsilonEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateful.composite.GammaEncoder;
import com.jme3.app.Application;
import com.jme3.app.state.BaseAppState;
import com.jme3.post.SceneProcessor;
import com.jme3.profile.AppProfiler;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.Renderer;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.texture.FrameBuffer;

public class CloudMonkeyServerAppState extends BaseAppState implements SceneProcessor {
    private CloudMonkeyVideoServer videoServer;
    private CloudMonkeyStatusServer statusServer;

    protected Renderer renderer;
    protected RenderManager renderManager;

    @Override
    protected void initialize(Application app) {
        this.renderer = app.getRenderer();
        this.renderManager = app.getRenderManager();

        List<ViewPort> postViewPorts = app.getRenderManager().getPostViews();
        ViewPort lastViewPort = postViewPorts.get(postViewPorts.size()-1);
        lastViewPort.addProcessor(this);

        videoServer = new CloudMonkeyVideoServer(postViewPorts);

        int width = lastViewPort.getCamera().getWidth();
        int height = lastViewPort.getCamera().getHeight();

        // videoServer.setEncoder(new NullEncoder(videoServer.getBufferSize()));
        videoServer.setEncoder(new EpsilonEncoder(width, height));

        statusServer = new CloudMonkeyStatusServer(app.getInputManager(), 13555);
    }

    @Override
    protected void cleanup(Application app) { }

    @Override
    protected void onEnable() {
        try {
            videoServer.waitForSocketConnection(12555);
        } catch (IOException e) {
            e.printStackTrace();
        }

        statusServer.openSockets();
    }

    @Override
    protected void onDisable() { }

    @Override
    public void initialize(RenderManager rm, ViewPort vp) { }

    @Override
    public void reshape(ViewPort vp, int w, int h) { }

    @Override
    public void preFrame(float tpf) {
        videoServer.update(tpf);
    }

    @Override
    public void postFrame(FrameBuffer out) {
        statusServer.update();
        videoServer.renderUpdate(renderManager, out);
    }

    @Override
    public void postQueue(RenderQueue rq) {

    }

    @Override
    public void setProfiler(AppProfiler profiler) {

    }

}