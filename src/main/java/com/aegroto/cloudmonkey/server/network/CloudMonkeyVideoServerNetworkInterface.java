package com.aegroto.cloudmonkey.server.network;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface CloudMonkeyVideoServerNetworkInterface {
    public void waitForConnection(ByteBuffer acceptData) throws IOException;
    public boolean waitForFrameRequest() throws IOException;
    public void sendFrameHeader(int frameLength) throws IOException;
    public void sendFrame(ByteBuffer frameData, int frameLength) throws IOException;
}