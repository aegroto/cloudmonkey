package com.aegroto.cloudmonkey.server.network;

import static com.aegroto.cloudmonkey.common.CloudMonkeyMessage.HELLO;
import static com.aegroto.cloudmonkey.common.CloudMonkeyMessage.NEXT_FRAME_CHUNK;
import static com.aegroto.cloudmonkey.common.CloudMonkeyMessage.NEXT_FRAME_HEADER;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aegroto.cloudmonkey.common.CloudMonkeyMessage;
import com.aegroto.cloudmonkey.common.Helpers;

import lombok.Setter;

public class CloudMonkeyVideoServerDatagramSocket extends DatagramSocket implements CloudMonkeyVideoServerNetworkInterface {
    protected static final Logger logger = Logger.getLogger(CloudMonkeyVideoServerDatagramSocket.class.getName());

    static {
        logger.setLevel(Level.WARNING);
    }

    @Setter protected int packetSize = 512;

    // Congestion control settings
    @Setter protected int rushSizeAdd = 128;
    @Setter protected int packetsRushSize = rushSizeAdd * 8;

    @Setter protected int maxPacketsRushSize = packetsRushSize * 4;
    @Setter protected int minPacketsRushSize = packetsRushSize;

    @Setter protected int packetsRushSleep = 10;

    @Setter protected int consecutiveFullFramesThreshold = 5;
    @Setter protected int maxConsecutivePacketLosses = 1;

    protected int currentConsecutiveFullFrames = 0;
    protected int currentConsecutivePacketLosses = 0;

    InetAddress clientAddress;
    int clientPort;

    byte[] packetBuffer = new byte[packetSize];
    DatagramPacket packet;

    public CloudMonkeyVideoServerDatagramSocket() throws SocketException {
        super();
    }

    public CloudMonkeyVideoServerDatagramSocket(int port) throws SocketException {
        super(port);
    }

    public void waitForConnection(ByteBuffer acceptData) throws IOException {
        logger.info("Waiting for connection...");

        byte[] helloData = new byte[HELLO.size];
        DatagramPacket helloPacket = new DatagramPacket(helloData, helloData.length);
        receive(helloPacket);

        logger.info("Connection opened! (data: " + helloData[0] + ")");

        clientAddress = helloPacket.getAddress();
        clientPort = helloPacket.getPort();

        packet = new DatagramPacket(packetBuffer, packetBuffer.length, clientAddress, clientPort);

        DatagramPacket acceptPacket = new DatagramPacket(acceptData.array(), acceptData.capacity(), clientAddress,
                clientPort);
        send(acceptPacket);
    }

    public boolean waitForFrameRequest() throws IOException {
        boolean slowdown = false;

        logger.info("Waiting for frame request...");

        byte[] frameRequestData = new byte[CloudMonkeyMessage.NEXT_FRAME.size];
        DatagramPacket requestPacket = new DatagramPacket(frameRequestData, frameRequestData.length);
        receive(requestPacket);

        if (frameRequestData[0] == CloudMonkeyMessage.NEXT_FRAME.id) {
            logger.info("Received frame request");

            // Packet loss during the last round
            if(frameRequestData[1] == 1) {
                currentConsecutiveFullFrames = 0;
                ++currentConsecutivePacketLosses;

                if(currentConsecutivePacketLosses >= maxConsecutivePacketLosses) {
                    maxPacketsRushSize = maxPacketsRushSize / 2;
                    packetsRushSize = packetsRushSize / 2;

                    if(packetsRushSize < minPacketsRushSize) {
                        packetsRushSize = minPacketsRushSize;
                        maxPacketsRushSize = minPacketsRushSize;
                    }

                    currentConsecutivePacketLosses = 0;

                    logger.info("Received a slowdown, new packetsRushSize: " + packetsRushSize + ", maxPacketsRushSize: " + maxPacketsRushSize);
                    slowdown = true;
                }
            } else {
                currentConsecutivePacketLosses = 0;

                if(packetsRushSize < maxPacketsRushSize) {
                    packetsRushSize += rushSizeAdd;
                } else {
                    ++currentConsecutiveFullFrames;

                    if(currentConsecutiveFullFrames >= consecutiveFullFramesThreshold) {
                        currentConsecutiveFullFrames = 0;
                        maxPacketsRushSize *= 2;

                        logger.info("Max packets rush size: " + maxPacketsRushSize);
                    }
                }
            }
        } else {
            logger.warning("Wrong packet received while waiting for frame request");
        }

        return slowdown;
    }

    public void sendFrameHeader(int frameLength) throws IOException {
        byte[] sendBuffer = new byte[NEXT_FRAME_HEADER.size];

        sendBuffer[0] = NEXT_FRAME_HEADER.id;
        Helpers.putIntInByteArray(sendBuffer, frameLength, 1);

        DatagramPacket frameHeaderPacket = new DatagramPacket(sendBuffer, NEXT_FRAME_HEADER.size, clientAddress,
                clientPort);
        send(frameHeaderPacket);

        logger.info("Sent frame header");
    }

    public void sendFrame(ByteBuffer frameData, int frameLength) throws IOException {
        int sentBytes = 0;

        int rushedPackets = 0;

        sendFrameHeader(frameLength);
        packetBuffer[0] = NEXT_FRAME_CHUNK.id;

        while (sentBytes < frameLength) {
            int bytesToSend = Math.min(packetBuffer.length - 5, frameData.remaining());
            sentBytes += bytesToSend;

            Helpers.putIntInByteArray(packetBuffer, sentBytes, 1);
            frameData.get(packetBuffer, 5, bytesToSend);

            packet.setData(packetBuffer);

            // if(System.currentTimeMillis() % 10 != 0) {
            send(packet);
            /*
             * } else { logger.warning("Simulated packet loss (" + sentBytes + "/" +
             * frameLength + ")"); }
             */

            logger.info("Sent frame chunk (" + sentBytes + "/" + frameLength + ")");

            // sentBytes = waitAndParseFrameChunkRequest();
            // }

            if(rushedPackets > packetsRushSize) {
                try {
                    Thread.sleep(packetsRushSleep);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                rushedPackets = 0;
            } else {
                ++rushedPackets;
            }
        }
    }

    /*public int waitAndParseFrameChunkRequest() throws IOException {
        logger.info("Waiting for frame chunk request...");

        byte[] chunkRequestData = new byte[NEXT_FRAME_CHUNK.size];
        DatagramPacket requestPacket = new DatagramPacket(chunkRequestData, chunkRequestData.length);
        receive(requestPacket);

        logger.info("Received frame chunk request");

        return Helpers.getIntFromByteArray(chunkRequestData, 1);
    }*/
}