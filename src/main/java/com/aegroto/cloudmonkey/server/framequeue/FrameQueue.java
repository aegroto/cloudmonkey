package com.aegroto.cloudmonkey.server.framequeue;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;

import com.jme3.util.BufferUtils;

import lombok.Getter;
import lombok.Setter;

public class FrameQueue {
    protected ArrayDeque<FrameSlot> freeSlots, shippedSlots;

    @Getter protected final int size, bufferSize;
    @Getter @Setter long framesTimeoutThreshold;

    protected long lastFrameId;

    public FrameQueue(int size, int bufferSize, ByteOrder byteOrder) {
        this.size = size;
        this.bufferSize = bufferSize;

        this.framesTimeoutThreshold = 50;
        this.lastFrameId = 1;

        this.freeSlots = new ArrayDeque<FrameSlot>(size);
        this.shippedSlots = new ArrayDeque<FrameSlot>(size);

        while(freeSlots.size() < size) {
            freeSlots.offer(new FrameSlot(
                0,
                BufferUtils.createByteBuffer(bufferSize).order(byteOrder),
                System.currentTimeMillis()
            ));
        }
    }

    public ByteBuffer getNextFreeBuffer() {
        synchronized(freeSlots) {
            if (freeSlots.isEmpty()) {
                try {
                    freeSlots.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return freeSlots.poll().getBuffer();
        }
    }

    public ByteBuffer getNextShippedBuffer() {
        synchronized(shippedSlots) {
            FrameSlot nextShippedSlot = null;

            while(nextShippedSlot == null) {
                if(shippedSlots.isEmpty()) {
                    try {
                        shippedSlots.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                do {
                    nextShippedSlot = shippedSlots.poll();

                    if(nextShippedSlot != null && System.currentTimeMillis() - nextShippedSlot.getTimestamp() > framesTimeoutThreshold) {
                        nextShippedSlot.getBuffer().clear();                            
                        paybackBuffer(nextShippedSlot.getBuffer());

                        nextShippedSlot = null;
                    }
                } while(!shippedSlots.isEmpty() && nextShippedSlot == null);
            }

            return nextShippedSlot.getBuffer();
        }
    }

    public void shipBuffer(ByteBuffer buffer) {
        synchronized(shippedSlots) {
            shippedSlots.offer(new FrameSlot(lastFrameId, buffer, System.currentTimeMillis()));

            ++lastFrameId;

            shippedSlots.notifyAll();
        }
    }

    public void paybackBuffer(ByteBuffer buffer) {
        synchronized(freeSlots) {
            freeSlots.offer(new FrameSlot(0, buffer, System.currentTimeMillis()));

            freeSlots.notifyAll();
        }
    }
}