package com.aegroto.cloudmonkey.server.framequeue;

import java.nio.ByteBuffer;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class FrameSlot {
    @Getter protected long id;
    @Getter protected ByteBuffer buffer;
    @Getter protected final long timestamp;

    @Override
    public String toString() {
        return String.format("[%d, %s, %d]", id, buffer, timestamp);
    }
}