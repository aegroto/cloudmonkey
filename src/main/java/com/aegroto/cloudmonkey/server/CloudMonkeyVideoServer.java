package com.aegroto.cloudmonkey.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.server.dispatcher.FrameDispatcher;
import com.aegroto.cloudmonkey.server.framequeue.FrameQueue;
import com.aegroto.cloudmonkey.server.network.CloudMonkeyVideoServerDatagramSocket;
import com.aegroto.cloudmonkey.server.network.CloudMonkeyVideoServerNetworkInterface;
import com.aegroto.cloudmonkey.server.retriever.FrameRetriever;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.Renderer;
import com.jme3.renderer.ViewPort;
import com.jme3.texture.FrameBuffer;
import com.jme3.texture.Image.Format;

import org.lwjgl.BufferUtils;

import lombok.Getter;

public class CloudMonkeyVideoServer {
    protected static final Logger logger = Logger.getLogger(CloudMonkeyVideoServer.class.getName());

    static {
        logger.setLevel(Level.WARNING);
    }

    protected final int bytesPerPacket = 512;

    protected final int bytesPerPixel = 4;
    protected CloudMonkeyVideoServerNetworkInterface networkInterface;

    protected final int width, height;

    protected float timeCounter = 0;

    protected FrameQueue frameQueue;
    protected FrameRetriever retriever;
    protected FrameDispatcher dispatcher;

    public CloudMonkeyVideoServer(List<ViewPort> viewPorts) {
        ViewPort lastViewPort = viewPorts.get(viewPorts.size() - 1);

        final int camWidth = lastViewPort.getCamera().getWidth();
        final int camHeight = lastViewPort.getCamera().getHeight();

        width = camWidth;
        height = camHeight;
    }

    public void setEncoder(Encoder encoder) {
        this.frameQueue = new FrameQueue(5, getBufferSize(), encoder.getByteOrder());
        this.retriever = new FrameRetriever(frameQueue);
        this.dispatcher = new FrameDispatcher(frameQueue, encoder);
    }

    public void update(float tpf) {
        timeCounter += tpf;

        if (timeCounter >= 1f) {
            if (dispatcher.getFramesCounter() > 0) {
                double mbits = (double) (dispatcher.getBandwidthCounter() * 8) / (1024 * 1024);
                logger.warning(String.format("server FPS: %d, BW: %.2f Mbit/s (%.2f Mbit/frame)",
                        dispatcher.getFramesCounter(), mbits, mbits / dispatcher.getFramesCounter()));
            } else {
                logger.warning("server FPS: no frames trasmitted");
            }

            dispatcher.setBandwidthCounter(0);
            dispatcher.setFramesCounter(0);
            timeCounter = 0;
        }
    }

    protected Thread dispatchThread = new Thread(new Runnable() {
        @Override
        public void run() {
            while(true) {
                dispatcher.dispatchUpdate(networkInterface);
            }
        }
    });

    public void renderUpdate(RenderManager renderManager, FrameBuffer frameBuffer) {
        retriever.retrieveUpdate(renderManager, frameBuffer);
    }

    public void waitForSocketConnection(int listenPort) throws IOException {
        CloudMonkeyVideoServerDatagramSocket socket = new CloudMonkeyVideoServerDatagramSocket(listenPort);
        networkInterface = socket;

        socket.setPacketSize(Math.min(frameQueue.getBufferSize(), bytesPerPacket));
        socket.setSendBufferSize(width * height * bytesPerPixel);

        /*CloudMonkeyServerDatagramChannel channel = new CloudMonkeyServerDatagramChannel(listenPort);
        networkInterface = channel;*/

        ByteBuffer acceptData = ByteBuffer.allocate(8);
        acceptData.putInt(width);
        acceptData.putInt(height);

        networkInterface.waitForConnection(acceptData);

        dispatchThread.start();
    }

    public int getBufferSize() {
        return width * height * bytesPerPixel;
    }
}