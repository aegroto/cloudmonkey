package com.aegroto.cloudmonkey.server.dispatcher;

import com.aegroto.cloudmonkey.server.network.CloudMonkeyVideoServerNetworkInterface;
import com.jme3.util.BufferUtils;

import lombok.Getter;
import lombok.Setter;

import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.aegroto.cloudmonkey.server.framequeue.FrameQueue;

public class FrameDispatcher {
    protected static final Logger logger = Logger.getLogger(FrameDispatcher.class.getName());

    static {
        logger.setLevel(Level.WARNING);
    }

    protected FrameQueue frameQueue;
    protected ByteBuffer encodedFrameBuffer;

    protected Encoder encoder;

    @Getter @Setter protected int framesCounter = 0, bandwidthCounter = 0;

    public FrameDispatcher(FrameQueue frameQueue, Encoder encoder) {
        this.frameQueue = frameQueue;
        this.encoder = encoder;
        
        this.encodedFrameBuffer = BufferUtils.createByteBuffer(encoder.outputBufferMinLength()).order(encoder.getByteOrder());
    }

    public void dispatchUpdate(CloudMonkeyVideoServerNetworkInterface networkInterface) {
        try {
            boolean slowdown = networkInterface.waitForFrameRequest();

            if(slowdown) {
                encoder.reset();
            }

            logger.info("Sending current frame...");

            // Frame encoding
            ByteBuffer currentFrameBuffer = frameQueue.getNextShippedBuffer();

            int encodedDataLength = encoder.encode(currentFrameBuffer, encodedFrameBuffer);
            encodedFrameBuffer.flip();

            // Frame sending
            networkInterface.sendFrame(encodedFrameBuffer, encodedFrameBuffer.limit());

            currentFrameBuffer.clear();
            encodedFrameBuffer.clear();

            frameQueue.paybackBuffer(currentFrameBuffer);

            bandwidthCounter += encodedDataLength;

            ++framesCounter;

            logger.info("Sent last frame");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}