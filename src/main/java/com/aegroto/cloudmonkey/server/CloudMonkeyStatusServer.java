package com.aegroto.cloudmonkey.server;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aegroto.cloudmonkey.common.InputEntry;
import com.jme3.input.InputManager;
import com.jme3.input.RawInputListener;
import com.jme3.input.event.InputEvent;
import com.jme3.input.event.JoyButtonEvent;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.input.event.MouseMotionEvent;
import com.jme3.input.event.TouchEvent;
import com.jme3.input.event.TouchEvent.Type;

public class CloudMonkeyStatusServer {
    protected static final Logger logger = Logger.getLogger(CloudMonkeyStatusServer.class.getName());

    static {
        logger.setLevel(Level.WARNING);
    }

    protected final int port;

    protected ServerSocketChannel serverSocketChannel;
    protected SocketChannel socketChannel;

    protected ByteBuffer packetBuffer;

    protected InputManager inputManager;

    protected ArrayList<InputEvent> imQueue = new ArrayList<InputEvent>();

    public CloudMonkeyStatusServer(InputManager inputManager, int port) {
        this.port = port;
        this.inputManager = inputManager;
    }

    public void openSockets() {
        try {
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(true);
            serverSocketChannel.socket().bind(new InetSocketAddress(port));

            socketChannel = serverSocketChannel.accept();
            socketChannel.configureBlocking(false);

            packetBuffer = ByteBuffer.allocate(1024);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Field f = inputManager.getClass().getDeclaredField("inputQueue");

            f.setAccessible(true);
            imQueue = (ArrayList<InputEvent>) f.get(inputManager);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        logger.info("Sockets initialized");
    }

    protected void addTouchEventToQueue(ArrayList<InputEvent> queue, ByteBuffer packetBuffer) {
        Type type = Type.values()[packetBuffer.getInt()]; // TODO: Risky, to be reviewed.
        float x = packetBuffer.getFloat();
        float y = packetBuffer.getFloat();
        float dx = packetBuffer.getFloat();
        float dy = packetBuffer.getFloat();

        queue.add(new TouchEvent(type, x, y, dx, dy));
    }
    
    protected void addMouseMotionEventToQueue(ArrayList<InputEvent> queue, ByteBuffer packetBuffer) {
        int x = packetBuffer.getInt();
        int y = packetBuffer.getInt();
        int dx = packetBuffer.getInt();
        int dy = packetBuffer.getInt();
        int wheel = packetBuffer.getInt();
        int deltaWheel = packetBuffer.getInt();

        queue.add(new MouseMotionEvent(x, y, dx, dy, wheel, deltaWheel));
    }

    protected void addMouseButtonEventToQueue(ArrayList<InputEvent> queue, ByteBuffer packetBuffer) {
        int buttonIndex = packetBuffer.getInt();
        boolean pressed = packetBuffer.get() == 1;
        int x = packetBuffer.getInt();
        int y = packetBuffer.getInt();

        queue.add(new MouseButtonEvent(buttonIndex, pressed, x, y));
    }

    protected void addKeyEventToQueue(ArrayList<InputEvent> queue, ByteBuffer packetBuffer) {
        char keyChar = packetBuffer.getChar();
        int keyCode = packetBuffer.getInt();
        boolean pressed = packetBuffer.get() == 1;
        boolean released = packetBuffer.get() == 1;

        queue.add(new KeyInputEvent(keyCode, keyChar, pressed, released));
    }

    // TODO: To be implemented
    protected void addJoyButtonEventToQueue(ArrayList<InputEvent> queue, ByteBuffer packetBuffer) {
    }

    // TODO: To be implemented
    protected void addJoyAxisEventToQueue(ArrayList<InputEvent> queue, ByteBuffer packetBuffer) {
    }

    public void update() {
        try {
            packetBuffer.clear();
            socketChannel.read(packetBuffer);

            // System.out.println("Received from client: " + Arrays.toString(packetBuffer.array()));
            
            packetBuffer.flip();
            boolean endBlock = false;

            while(packetBuffer.hasRemaining() && !endBlock) {
                byte inputType = packetBuffer.get();

                switch(inputType) {
                    case InputEntry.TYPE_TOUCH:
                        addTouchEventToQueue(imQueue, packetBuffer);
                        break;
                    case InputEntry.TYPE_MOUSE_MOTION:
                        addMouseMotionEventToQueue(imQueue, packetBuffer);
                        break;
                    case InputEntry.TYPE_MOUSE_BUTTON:
                        break;
                    case InputEntry.TYPE_KEY: 
                        addKeyEventToQueue(imQueue, packetBuffer);
                        break;
                    case InputEntry.TYPE_JOY_BUTTON: 
                        addJoyButtonEventToQueue(imQueue, packetBuffer);
                        break;
                    case InputEntry.TYPE_JOY_AXIS: 
                        addJoyAxisEventToQueue(imQueue, packetBuffer);
                        break;
                    case InputEntry.TYPE_ENDBLOCK:
                        endBlock = true;
                        break;
                }
            }

            packetBuffer.clear();
            packetBuffer.put((byte) 16);

            packetBuffer.flip();
            socketChannel.write(packetBuffer);
        } catch (IOException e) {
            System.out.println("Error while updating status server: " + e);
        }
    }
}