package com.aegroto.cloudmonkey.server.retriever;

import java.nio.ByteBuffer;

import com.aegroto.cloudmonkey.server.framequeue.FrameQueue;
import com.jme3.renderer.RenderManager;
import com.jme3.texture.FrameBuffer;

public class FrameRetriever {
    protected FrameQueue frameQueue;

    public FrameRetriever(FrameQueue frameQueue) {
        this.frameQueue = frameQueue;
    }

    public void retrieveUpdate(RenderManager renderManager, FrameBuffer frameBuffer) {
        ByteBuffer currentFrameBuffer = frameQueue.getNextFreeBuffer(); 
        renderManager.getRenderer().readFrameBuffer(frameBuffer, currentFrameBuffer);
        frameQueue.shipBuffer(currentFrameBuffer);
    }
}