package com.aegroto.cloudmonkey.client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aegroto.cloudmonkey.common.InputEntry;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.RawInputListener;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.event.InputEvent;
import com.jme3.input.event.JoyAxisEvent;
import com.jme3.input.event.JoyButtonEvent;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.input.event.MouseMotionEvent;
import com.jme3.input.event.TouchEvent;

import lombok.AllArgsConstructor;

public class CloudMonkeyStatusClient {
    protected static final Logger logger = Logger.getLogger(CloudMonkeyStatusClient.class.getName());

    static {
        logger.setLevel(Level.WARNING);
    }

    protected String ip;
    protected int port;

    protected SocketChannel socketChannel;

    protected ByteBuffer packetBuffer;

    protected InputManager inputManager;

    protected ArrayDeque<InputEvent> inputEvents;
    
    protected HashMap<String, Integer> keyList = new HashMap<>();

    public CloudMonkeyStatusClient(InputManager inputManager, String ip, int port) {
        this.ip = ip;
        this.port = port;
        this.inputManager = inputManager;

        inputManager.addRawInputListener(networkRawListener);
    }

    public void openConnection() {
        try {
            logger.info("Enstablishing connection...");

            socketChannel = SocketChannel.open();
            socketChannel.connect(new InetSocketAddress(ip, port));
            socketChannel.configureBlocking(false);

            packetBuffer = ByteBuffer.allocate(1024);

            inputEvents = new ArrayDeque<>(8);

            logger.info("Sockets and buffer initialized.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected RawInputListener networkRawListener = new RawInputListener(){
        @Override public void onTouchEvent(TouchEvent evt) { inputEvents.add(evt); }
        @Override public void onMouseMotionEvent(MouseMotionEvent evt) { inputEvents.add(evt); }
        @Override public void onMouseButtonEvent(MouseButtonEvent evt) { inputEvents.add(evt); }
        @Override public void onKeyEvent(KeyInputEvent evt) { inputEvents.add(evt); }
        @Override public void onJoyButtonEvent(JoyButtonEvent evt) { inputEvents.add(evt); }
        @Override public void onJoyAxisEvent(JoyAxisEvent evt) { inputEvents.add(evt); }
        @Override public void endInput() { }
        @Override public void beginInput() { }
    };

    protected void putTouchEventInBuffer(ByteBuffer packetBuffer, TouchEvent touchEvent) {
        packetBuffer.put(InputEntry.TYPE_TOUCH);

        packetBuffer.putInt(touchEvent.getType().ordinal());
        packetBuffer.putFloat(touchEvent.getX());
        packetBuffer.putFloat(touchEvent.getY());
        packetBuffer.putFloat(touchEvent.getDeltaX());
        packetBuffer.putFloat(touchEvent.getDeltaY());
    }

    protected void putMouseMotionEventInBuffer(ByteBuffer packetBuffer, MouseMotionEvent mouseMotionEvent) {
        packetBuffer.put(InputEntry.TYPE_MOUSE_MOTION);
        
        packetBuffer.putInt(mouseMotionEvent.getX());
        packetBuffer.putInt(mouseMotionEvent.getY());
        packetBuffer.putInt(mouseMotionEvent.getDX());
        packetBuffer.putInt(mouseMotionEvent.getDY());
        packetBuffer.putInt(mouseMotionEvent.getWheel());
        packetBuffer.putInt(mouseMotionEvent.getDeltaWheel());
    }

    protected void putMouseButtonEventInBuffer(ByteBuffer packetBuffer, MouseButtonEvent mouseButtonEvent) {
        packetBuffer.put(InputEntry.TYPE_MOUSE_BUTTON);

        packetBuffer.putInt(mouseButtonEvent.getButtonIndex());
        packetBuffer.put((byte) (mouseButtonEvent.isPressed() ? 1 : 0));
        packetBuffer.putInt(mouseButtonEvent.getX());
        packetBuffer.putInt(mouseButtonEvent.getY());
    }

    protected void putKeyEventInBuffer(ByteBuffer packetBuffer, KeyInputEvent keyEvent) {
        packetBuffer.put(InputEntry.TYPE_KEY);

        packetBuffer.putChar(keyEvent.getKeyChar());
        packetBuffer.putInt(keyEvent.getKeyCode());
        packetBuffer.put((byte) (keyEvent.isPressed() ? 1 : 0));
        packetBuffer.put((byte) (keyEvent.isRepeating() ? 1 : 0));
    }

    // TODO: To be implemented
    protected void putJoyButtonEventInBuffer(ByteBuffer packetBuffer, JoyButtonEvent joyButtonEvent) {
    }

    // TODO: To be implemented
    protected void putJoyAxisEventInBuffer(ByteBuffer packetBuffer, JoyAxisEvent joyButtonEvent) {
    }

    public void update() {
        try {
            packetBuffer.clear();
            Arrays.fill(packetBuffer.array(), (byte) 0);

            while(!inputEvents.isEmpty() && packetBuffer.remaining() > 128) {
                InputEvent event = inputEvents.pop();

                if(event instanceof TouchEvent) {
                    putTouchEventInBuffer(packetBuffer, (TouchEvent) event);
                } else if(event instanceof MouseMotionEvent) {
                    putMouseMotionEventInBuffer(packetBuffer, (MouseMotionEvent) event);
                } else if(event instanceof MouseButtonEvent) {
                    putMouseButtonEventInBuffer(packetBuffer, (MouseButtonEvent) event);
                } else if(event instanceof KeyInputEvent) {
                    putKeyEventInBuffer(packetBuffer, (KeyInputEvent) event);
                } else if(event instanceof JoyButtonEvent) {
                    putJoyButtonEventInBuffer(packetBuffer, (JoyButtonEvent) event);
                } else if(event instanceof JoyAxisEvent) {
                    putJoyAxisEventInBuffer(packetBuffer, (JoyAxisEvent) event);
                }
            }

            packetBuffer.put(InputEntry.TYPE_ENDBLOCK);

            packetBuffer.flip();

            // System.out.println("Sending: " + Arrays.toString(packetBuffer.array()));

            socketChannel.write(packetBuffer);

            packetBuffer.clear();
            socketChannel.read(packetBuffer);

            // System.out.println("Response from server: " + Arrays.toString(packetBuffer.array()));
        } catch (IOException e) {
            System.out.println("Error while updating status client: " + e);
        }
    }
}