package com.aegroto.cloudmonkey.client.network;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface CloudMonkeyClientNetworkInterface {
    public void openConnection(ByteBuffer connectionData) throws IOException; 
    public void waitNextFrameHeader(byte[] frameHeaderBuffer) throws IOException; 
    public boolean waitAndLoadNextFrame(ByteBuffer frameBuffer) throws IOException; 
    public void sendNextFrameRequest(boolean slowDown) throws IOException;
    public void sendNextFrameChunkRequest(int receivedBytes) throws IOException; 
}