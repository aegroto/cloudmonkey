package com.aegroto.cloudmonkey.client.network;

import static com.aegroto.cloudmonkey.common.CloudMonkeyMessage.HELLO;
import static com.aegroto.cloudmonkey.common.CloudMonkeyMessage.NEXT_FRAME_CHUNK;
import static com.aegroto.cloudmonkey.common.CloudMonkeyMessage.NEXT_FRAME_HEADER;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aegroto.cloudmonkey.common.CloudMonkeyMessage;
import com.aegroto.cloudmonkey.common.Helpers;

import lombok.Setter;

public class CloudMonkeyClientDatagramSocket extends DatagramSocket implements CloudMonkeyClientNetworkInterface {
    protected static final Logger logger = Logger.getLogger(CloudMonkeyClientDatagramSocket.class.getName());

    static {
        logger.setLevel(Level.WARNING);
    }

    @Setter protected InetAddress serverAddress;
    @Setter protected int serverPort;

    @Setter protected int framePacketSize = -1;

    public CloudMonkeyClientDatagramSocket() throws SocketException {
        super();
    }
    
    public CloudMonkeyClientDatagramSocket(int port) throws SocketException {
        super(port);
    }

    public void openConnection(ByteBuffer connectionData) throws IOException {
        logger.info("Opening connection...");

        byte[] netBuffer = new byte[connectionData.capacity()];
        netBuffer[0] = HELLO.id;
        DatagramPacket helloPacket = new DatagramPacket(netBuffer, HELLO.size, serverAddress, serverPort);
        send(helloPacket);

        logger.info("Sent hello packet, waiting for response...");

        DatagramPacket acceptPacket = new DatagramPacket(netBuffer, netBuffer.length);
        receive(acceptPacket);

        connectionData.put(netBuffer);

        logger.info("Received response from the server");

        sendNextFrameRequest(false);
    }

    public void waitNextFrameHeader(byte[] frameHeaderBuffer) throws IOException {
        DatagramPacket frameHeaderPacket = new DatagramPacket(frameHeaderBuffer, NEXT_FRAME_HEADER.size);

        byte messageId = -1;
        while(messageId != NEXT_FRAME_HEADER.id) {
            receive(frameHeaderPacket);
            messageId = frameHeaderBuffer[0];
        }
    }

    public boolean waitAndLoadNextFrame(ByteBuffer frameBuffer) throws IOException {
        logger.fine("Waiting for next frame...");

        byte[] receiveBuffer = new byte[framePacketSize];

        // receive frame header
        waitNextFrameHeader(receiveBuffer);
        int frameLength = Helpers.getIntFromByteArray(receiveBuffer, 1);
        logger.info("Received frame header (" + frameLength + "," + frameBuffer.capacity() + ")");

        // receive frame chunks
        int receivedBytes = 0, lastReceivedBytes = 0, expectedReceivedBytes = 0;
        DatagramPacket frameChunkPacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);

        boolean fullFrame = true;

        while(receivedBytes < frameLength) {
            receive(frameChunkPacket);

            if(receiveBuffer[0] == NEXT_FRAME_CHUNK.id) {
                expectedReceivedBytes = receivedBytes + framePacketSize - 5;
                lastReceivedBytes = Helpers.getIntFromByteArray(receiveBuffer, 1);

                int bytesToPut = Math.min(receiveBuffer.length - 5, frameLength - receivedBytes);

                if(lastReceivedBytes > expectedReceivedBytes) {
                    logger.warning("Packet loss (" + lastReceivedBytes + "/" + expectedReceivedBytes + ") , (" + frameLength + ")");

                    receivedBytes = lastReceivedBytes;

                    fullFrame = false;
                    break;
                } else {
                    receivedBytes = lastReceivedBytes;

                    frameBuffer.put(receiveBuffer, 5, bytesToPut);

                    logger.info("Received frame chunk... (" + receivedBytes + "/" + frameLength + ")");
                }
                
                // receivedBytes += receiveBuffer.length;
            } else {
                logger.warning("Wrong packet ID while waiting for frame chunk");

                fullFrame = false;
                break;
            }
        }

        return fullFrame;
    }

    public void sendNextFrameRequest(boolean slowDown) throws IOException {
        logger.info("Sending frame request...");

        byte[] requestData = { CloudMonkeyMessage.NEXT_FRAME.id , (byte) (slowDown ? 1 : 0) };

        DatagramPacket requestPacket = new DatagramPacket(requestData, requestData.length, serverAddress, serverPort);
        send(requestPacket);
    }

    public void sendNextFrameChunkRequest(int receivedBytes) throws IOException {
        logger.info("Sending frame chunk request...");
        byte[] chunkRequestData = new byte[NEXT_FRAME_CHUNK.size];
        chunkRequestData[0] = NEXT_FRAME_CHUNK.id;
        Helpers.putIntInByteArray(chunkRequestData, receivedBytes, 1);
        DatagramPacket requestPacket = new DatagramPacket(chunkRequestData, chunkRequestData.length, serverAddress, serverPort);
        send(requestPacket);
    }
}