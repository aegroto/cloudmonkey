package com.aegroto.cloudmonkey.client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aegroto.cloudmonkey.client.network.CloudMonkeyClientDatagramSocket;
import com.aegroto.cloudmonkey.client.network.CloudMonkeyClientNetworkInterface;
import com.aegroto.cloudmonkey.common.Helpers;
import com.aegroto.cloudmonkey.common.encoding.Encoder;
import com.jme3.texture.Image.Format;
import com.jme3.texture.Texture2D;

import org.lwjgl.BufferUtils;

import lombok.Getter;

public class CloudMonkeyVideoClient {
    protected static final Logger logger = Logger.getLogger(CloudMonkeyVideoClient.class.getName());

    static {
        logger.setLevel(Level.WARNING);
    }

    @Getter
    protected Texture2D texture;

    protected final int bytesPerPacket = 512;

    protected final int bytesPerPixel = 4;
    protected CloudMonkeyClientNetworkInterface networkInterface;

    @Getter
    protected ByteBuffer lastFrameBuffer;
    @Getter
    protected ByteBuffer encodedLastFrameBuffer;

    protected Encoder encoder = null;

    protected int framesCounter = 0;
    protected float timeCounter = 0;
    protected long networkThreadSleep = 10;

    @Getter protected int width, height;

    /*public CloudMonkeyClient(int width, int height) {
        this.width = width;
        this.height = height;

        initializeDataAndBuffers(width, height);
    }*/

    public CloudMonkeyVideoClient() { }

    protected void initializeDataAndBuffers(int width, int height) {
        texture = new Texture2D(width, height, Format.RGBA8);

        lastFrameBuffer = BufferUtils.createByteBuffer(getBufferSize()).order(ByteOrder.BIG_ENDIAN);
    }

    public void setEncoder(Encoder encoder) {
        this.encoder = encoder;
        this.encodedLastFrameBuffer = BufferUtils.createByteBuffer(encoder.outputBufferMinLength())
                .order(encoder.getByteOrder());
        encodedLastFrameBuffer.clear();
    }

    public void update(float tpf) {
        timeCounter += tpf;
        if (timeCounter >= 1f) {
            logger.severe("client FPS: " + framesCounter);
            framesCounter = 0;
            timeCounter = 0;
        }
    }

    public void renderUpdate() {
        boolean receivedFrame = networkUpdate();

        try {
            networkInterface.sendNextFrameRequest(!receivedFrame);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (lastFrameBuffer != null && receivedFrame) {
            if (encoder != null) {
                /*System.out.print("Encoded frame: ");
                while(encodedLastFrameBuffer.hasRemaining()) {
                    System.out.print(Helpers.byteToBinaryString(encodedLastFrameBuffer.get()) + ", ");
                }
                System.out.print("\n");
                encodedLastFrameBuffer.rewind();*/

                encoder.decode(encodedLastFrameBuffer, lastFrameBuffer);

                encodedLastFrameBuffer.clear();
            }

            lastFrameBuffer.rewind();

            /*System.out.print("Frame: ");
            while(lastFrameBuffer.hasRemaining()) {
                System.out.print(lastFrameBuffer.get() + ", ");
            }
            System.out.print("\n");
            lastFrameBuffer.rewind();*/

            setFrameData(lastFrameBuffer);

            lastFrameBuffer.clear();
        } else {
            encodedLastFrameBuffer.clear();
        }
    }

    protected boolean networkUpdate() {
        if (networkInterface != null) {
            try {
                if (encodedLastFrameBuffer != null) {
                    if (encodedLastFrameBuffer.position() == 0) {
                        boolean fullFrame = networkInterface.waitAndLoadNextFrame(encodedLastFrameBuffer);

                        if (fullFrame) {
                            encodedLastFrameBuffer.flip();

                            ++framesCounter;
                            logger.info("Received encoded frame, updating data");
                            return true;
                        } else {
                            encodedLastFrameBuffer.clear();
                            encoder.reset();

                            return false;
                        }
                    }
                }

                return false;
            } catch (SocketTimeoutException e) {
                logger.warning("Socket timeout, retrying...");
                encodedLastFrameBuffer.clear();
                encoder.reset();

                return false;
            } catch (Exception e) {
                e.printStackTrace();
                encodedLastFrameBuffer.clear();
                encoder.reset();

                return false;
            }
        }

        return false;
    }

    public void setFrameData(ByteBuffer frameBuffer) {
        /*while(frameBuffer.hasRemaining()) {
            System.out.print(frameBuffer.get() + " ");
        }
        System.out.print("\n");
        frameBuffer.flip();*/

        texture.getImage().setData(frameBuffer);
    }

    public void openSocketConnection(String serverAddress, int serverPort, int listenPort) throws IOException {
        logger.info("Opening connection...");
        
        CloudMonkeyClientDatagramSocket socket = new CloudMonkeyClientDatagramSocket(listenPort);
        networkInterface = socket;

        socket.setServerAddress(InetAddress.getByName(serverAddress));
        socket.setServerPort(serverPort);
        socket.setSoTimeout(2000);

        ByteBuffer connectionData = ByteBuffer.allocate(8);
        networkInterface.openConnection(connectionData);

        connectionData.flip();

        width = connectionData.getInt();
        height = connectionData.getInt();

        initializeDataAndBuffers(width, height);

        socket.setFramePacketSize(Math.min(lastFrameBuffer.capacity(), bytesPerPacket));
        socket.setReceiveBufferSize(width * height * bytesPerPixel);

        socket.setSoTimeout(100);
    }

    public int getBufferSize() {
        return width * height * bytesPerPixel;
    }
}