package com.aegroto.cloudmonkey.client;

import java.io.IOException;

import com.aegroto.cloudmonkey.common.encoding.stateful.composite.EpsilonEncoder;
import com.aegroto.cloudmonkey.common.encoding.stateful.composite.GammaEncoder;
import com.jme3.app.Application;
import com.jme3.app.state.BaseAppState;
import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Texture;

public class CloudMonkeyClientAppState extends BaseAppState {
    protected CloudMonkeyVideoClient videoClient;
    protected CloudMonkeyStatusClient statusClient;

    protected String serverIp;

    protected Material streamMat;

    public CloudMonkeyClientAppState(AssetManager assetManager, String serverIp) {
        this.serverIp = serverIp;

        streamMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        streamMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
    }

    @Override
    protected void initialize(Application app) {
        videoClient = new CloudMonkeyVideoClient();

        statusClient = new CloudMonkeyStatusClient(app.getInputManager(), serverIp, 13555);
    }

    public Material getStreamMaterial() {
        return streamMat;
    }

    @Override
    protected void cleanup(Application app) { }

    @Override
    protected void onEnable() {
        try {
            videoClient.openSocketConnection(serverIp, 12555, 12556);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // videoClient.setEncoder(new NullEncoder(videoClient.getBufferSize()));
        videoClient.setEncoder(new EpsilonEncoder(videoClient.getWidth(), videoClient.getHeight()));

        statusClient.openConnection();

        streamMat.setTexture("ColorMap", videoClient.getTexture());
    }

    @Override
    protected void onDisable() { }

    @Override
    public void update(float tpf) {
        videoClient.update(tpf);
    }

    @Override
    public void render(RenderManager rm) {
        statusClient.update(); 
        videoClient.renderUpdate();
    }
}