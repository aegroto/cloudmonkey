JDK_ROOT=/usr/lib/jvm/java-1.8.0-openjdk-amd64/

CFLAGS="-fPIC -O3 -march=native -mtune=native"

OUTPUT_FOLDER=./src/main/resources/

rm -r tmp

mkdir -p tmp/objs
mkdir -p $OUTPUT_FOLDER 

# for CLASS in "${NATIVE_SOURCES[@]}"
for CLASS in $(ls src/main/c | grep \\.c)
do
        CLASSNAME="${CLASS//.c/}"

        echo "Building $CLASSNAME..."

        gcc -c $CFLAGS \
                -I$JDK_ROOT/include/ \
                -I$JDK_ROOT/include/linux/ \
                src/main/c/$CLASSNAME.c -o tmp/objs/$CLASSNAME.o
done

gcc -shared -fPIC $CFLAGS -o $OUTPUT_FOLDER/nativeclasses.so tmp/objs/*.o  -lm -pthread -Wl,--whole-archive libzstd.a liblz4.a -Wl,--no-whole-archive
